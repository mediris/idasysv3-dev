<?php
session_start();

include_once("../conectar.php");

$atrnum = !empty($atrnum)?$atrnum:trim($_REQUEST["atrnum"]);
$aalcod = !empty($aalcod)?$aalcod:trim($_REQUEST["aalcod"]);
$atrcod = !empty($atrcod)?$atrcod:trim($_REQUEST["atrcod"]);

$sql2=" SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATSCOD, T1.ADSNRO,       
			T1.ATRDES, T1.ATROBS, T1.ATRFEC, T1.ATRHOR, T1.ATRMOR, T1.AISCOD,   
			T1.AUSCOD, T1.AAZCOD, T1.ADSSTS, T3.ATRSEC, T3.AARCOD, T3.ALTCOD,   
			T3.ASRCOD, T3.ATRCAN, T3.ATRUMB, T3.ATRUMH, T3.ATRFAC, T3.ATRCUT,   
			T3.ATRCUS, T3.ATREAC, T3.ATREAA, T3.ATREAL, T4.AARCOD, T4.AARNIV,   
			T4.AARDES, T5.AALDES, T6.AUSNO1, T6.AUSNO2, T6.AUSAP1, T6.AUSAP2,   
			T7.AEMMAI, T8.AISDES , T9.ATSDES, T8.AISOBS,
			(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2002') as DESEMP,
			(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2012') as CITCRE,
			(SELECT (TRIM(NOMBIN)||' '|| TRIM(PRAPIN) ||' '|| TRIM(SEAPIN)) AS NOMB FROM INTRAMED.INEMP WHERE CEDUIN=(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2012')) AS NOMBIN,
			(SELECT TRABIN FROM INTRAMED.INEMP WHERE CEDUIN=(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2012')) AS  TRABIN,
			(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2110') as FECCAL,
			(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2111') as FECCAD,
			(SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATSCOD=T1.ATSCOD and T10.ADSNRO=T1.ADSNRO AND T10.APACOD='2122') as MEDTRA
	FROM IV35FP T1
			left join (iv36fp t3 left join iv05fp t4 on(T3.ACICOD=T4.ACICOD and T3.AARCOD=T4.AARCOD)) on (T1.ACICOD=T3.ACICOD and T1.ADSNRO= T3.ADSNRO and T1.AALCOD=T3.AALCOD and T1.ADPCOD= T3.ADPCOD )
			INNER JOIN IV07FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD=T5.AALCOD)
			INNER JOIN (MB20FP T6 left join mb21fp T7 on (T6.auscod=t7.auscod and T7.aemprd='S') ) ON  (T1.AUSCOD=T6.AUSCOD)
			INNER JOIN iv42fp t8 ON (T1.AISCOD= T8.AISCOD and T1.ACICOD=T8.ACICOD)
			INNER JOIN is15fp t9 ON (t1.acicod=T9.ACICOD AND T1.ATSCOD= T9.ATSCOD )
	WHERE T1.ACICOD='$Compania' and T1.AALCOD='$aalcod' and T1.ADSNRO=$atrnum AND T1.ATSCOD='$atrcod'"; 

$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
$atrcod=trim(odbc_result($result2,'ATSCOD'));//cod tipo transaccion
$atsdes=trim(odbc_result($result2,'ATSDES'));//desc tipo transaccion
$adpcod=trim(odbc_result($result2,'ADPCOD'));//departamento
if(!empty($adpcod)){
	$aunsup = unisolicitante($adpcod, $Compania);
}else{
	$aunsup ='-';
}
$atrnum=trim(odbc_result($result2,'ADSNRO'));//transaccion
$atrfec=trim(odbc_result($result2,'ATRFEC'));//fecha de transaccion
$atrhor=trim(odbc_result($result2,'ATRHOR'));//hora de transaccion
$desc82=trim(odbc_result($result2,'AALDES'));//descripcion de almacen
$atrobs=trim(odbc_result($result2,'ATROBS'));//obser
$auscod=trim(odbc_result($result2,'AUSCOD'));//usuario quien realizo la ord
$ausap1=trim(odbc_result($result2,'AUSAP1'));//ususario apellido 1
$ausap2=trim(odbc_result($result2,'AUSAP2'));//ususario apellido 2
$ausno1=trim(odbc_result($result2,'AUSNO1'));//usuario nombre 1
$ausno2=trim(odbc_result($result2,'AUSNO2'));//usuario nombre 2
$aemmai=trim(odbc_result($result2,'AEMMAI'));//email.. desti 
$atrsts=trim(odbc_result($result2,'ADSSTS'));//status transaccion
$atrdes=trim(odbc_result($result2,'ATRDES'));//status transaccion
$aiscod=trim(odbc_result($result2,'AISCOD'));// cod iden servicio
$aisdes=trim(odbc_result($result2,'AISDES'));// descripcion iden servicio
$aisobs=trim(odbc_result($result2,'AISOBS'));// OBSERVACION iden servicio
/*parametros adicionales*/
$desemp=trim(odbc_result($result2,'DESEMP'));// DESCRIPCION empaque (Param Adi)
$citcre=trim(odbc_result($result2,'CITCRE'));// CEDULA DE RESPONSABLE (Param Adi)
$nombin=trim(odbc_result($result2,'NOMBIN'));// NOMBRE DE RESPONSABLE (Param Adi)
$trabin=trim(odbc_result($result2,'TRABIN'));// COD TRAB DE RESPONSABLE (Param Adi)


$feccal=trim(odbc_result($result2,'FECCAL'));// COD TRAB DE RESPONSABLE (Param Adi)
$feccad=trim(odbc_result($result2,'FECCAD'));// COD TRAB DE RESPONSABLE (Param Adi)

$medtra=trim(odbc_result($result2,'MEDTRA'));// COD TRAB DE RESPONSABLE (Param Adi)
			
		
$sqla="SELECT  (sum(T1.ATRCAN)) AS ATRCAN, T1.ACICOD, T1.ATRUMB, T2.AARDES, T1.AARCOD, T1.ATDSTS, T2.ATICOD , T3.AMCCOD, T4.AMCDES, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('0102') ) AS DESLAR, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1202') ) AS CNTNV, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1203') ) AS CN1NV, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1204') ) AS ITEM, 
		(SELECT T8.AITDES FROM is22fp T8 WHERE T8.acicod=T1.ACICOD and T8.AITCOD=(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1204') )) as DESITEM, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1205') ) AS CNTBI, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1206') ) AS CN1BI, 
		(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('1207') ) AS MODELO, 
		(SELECT T7.ATRCUT FROM IV16FP T7 WHERE T7.ACICOD=T1.ACICOD AND T7.ATRCOD='0101' and T7.ATRART=T1.AARCOD ORDER BY T7.ATRCUT DESC FETCH FIRST 1 ROWS ONLY) as ATRCUT 
	FROM IV36FP T1 
		LEFT JOIN IV05FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AARCOD=T2.AARCOD) 
		LEFT JOIN IV06FP T3 ON (T2.ACICOD=T3.ACICOD AND T2.AARCOD=T3.AARCOD) 
		LEFT JOIN IV04FP T4 ON (T1.ACICOD=T4.ACICOD AND T3.AMCCOD=T4.AMCCOD) 
	WHERE T1.ACICOD='$Compania' AND 
		T1.ADSNRO=$atrnum AND 
		T1.ATSCOD='$atrcod' AND 
		T1.AALCOD='$aalcod'
	group by T1.ACICOD, T1.ATRUMB, T2.AARDES, T1.AARCOD, T1.ATDSTS, T2.ATICOD , T3.AMCCOD, T4.AMCDES";

	 
$resulta3=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec. SQL: *".$sqla."*"));

/* Select para consultar la informacion del articulo asociado a la dotacion */
$sqlDot = "SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATSCOD, T1.ADSNRO, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATROBS, T1.ATRFEC, T1.ATRHOR, T1.AUSCOD, T1.ADSSTS,  T2.AARCOD, T3.AARDES, T4.ALTCOD, T5.AISDES, T5.AISOBS, T5.AISCOD FROM IV35FP T1 INNER JOIN IV36FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATSCOD = T2.ATSCOD AND T1.ADSNRO = T2.ADSNRO) INNER JOIN IV05FP T3 ON (T1.ACICOD = T3.ACICOD AND T2.AARCOD = T3.AARCOD) LEFT JOIN IV48FP T4 ON (T1.ACICOD = T4.ACICOD  AND T2.AARCOD = T4.AARCOD) LEFT JOIN IV42FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AISCOD = T5.AISCOD) WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$aalcod."' AND T1.ADSNRO = '".$atrnum."' AND T1.ATSCOD = '".$atrcod."'";

$resultDot=odbc_exec($cid,$sqlDot)or die(exit("Error en odbc_exec 11111"));

$paciente = odbc_result($resultDot, "ATRDES");
$cliente = odbc_result($resultDot, "AISDES");
$direccionServicio = odbc_result($resultDot, "AISOBS");


/*Consulta para obtener cantida de mCi*/
$sqlb = "SELECT  T1.AARCOD, T2.AAPVLA FROM IV36FP T1 LEFT JOIN IV17FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.AARCOD = T2.AARCOD AND T2.APACOD = '2112') WHERE T1.ACICOD='$Compania' AND T1.ADSNRO='$atrnum' AND T1.ATSCOD='$atrcod' AND T1.AALCOD='$aalcod'";

$resultsqlb=odbc_exec($cid,$sqlb)or die(exit("Error en odbc_exec. SQL: *".$sqlb."*"));

while(odbc_fetch_row($resultsqlb)){

	//$cantMci=trim(odbc_result($resultsqlb,'AAPVLA'));

	/*nuevo*/
	$articulo = odbc_result($resultsqlb,'AARCOD');

	$sqln ="SELECT ALTCOD FROM iv48fp t1 WHERE T1.ACICOD='$Compania' AND T1.AARCOD='$articulo'";

	$resultn=odbc_exec($cid,$sqln)or die(exit("Error en odbc_exec. sql:*".$sql3."*"));
	while(odbc_fetch_row($resultn)){
		$lote = trim(odbc_result($resultn,'ALTCOD')).", ";

	}
	$lote = substr($lote,0, (strripos($lote,",")));
	
	if(!empty($lote)){
		$cantMci = trim(odbc_result($resultsqlb,'AAPVLA'));
	}
	/*nuevo*/

	if(strpos(trim($articulo), 'GTC2') !== false){
		if(stripos($articulo,"-i") > 0){
			$observacion = "La empresa MEDITRON, C.A., no es productora del Tc-99m, solo importa y comercializa el producto, por lo que la disposici&oacute;n final del generador y su embalaje es RESPONSABILIDAD EXCLUSIVA DEL CLIENTE, una vez finalizada su vida &uacute;til.";
		}else{
			$observacion = "El manejo y disposici&oacute;n final del generador lo realizar&aacute; MEDITRON, C.A una vez finalizada su vida &uacute;til. Es responsabilidad del cliente informar con anticipaci&oacute;n la disponibilidad del servicio para efectuar el retiro del generador.";
		}
	}
}
/*Consulta para obtener cantida de mCi*/


if($Compania =='43'){
	$img = 'MEDITRON_logo_rif_2.png';
}else{
	$img = 'logomeditronnuevo2.png';
}

$direccion = !empty($Direccionip)?$Direccionip:'srvdesarrolloweb';
$Companiarif = $_SESSION['Companiarif'];


/* Nota de entrega */
$html.='
	<style>
		.codart{
			text-align: left;
			margin-left: 10px;
			margin-top: 0px;
		}
		.cantidadc{
			text-align: right;
			margin-right: 18px;
			margin-top: -30px;
		}
		.descripcionc1{
			margin-left: 200px;
			margin-top:-25px;
			text-align:justify;
		}
		.descripcionc2{
			margin-left:200px;
			margin-right:160px;
			margin-bottom:0px;
			margin-top:0px;
		}
		.contenido{
			text-align: right;
			margin-right:10px;
			margin-top: -28px;
		}
	</style>
	<page backtop="70mm" backbottom="80mm" >
		<page_header>
    		<table style="width:100%;align:center;" border="0" >
            	<tr>
                	<td>
                    	<img src="http://'.$direccion.'/idasysv3/images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5>
                	</td>
                	<td>
                    	<table style="width: 50%;margin-left:350px;font-size:10;">
                        	<tr align="left" style="width: 30%;"><td>P&Aacute;G. NRO: [[page_cu]] DE: [[page_nb]]</td></tr>
                        	<tr align="left" style="width: 30%;"><td>FECHA: '.date('d/m/Y').'</td></tr>
                    	</table>
                	</td>
            	</tr>
            	<tr>
             		<td colspan="2" style="text-align: center"> 
						<h1>NOTA DE ENTREGA</h1>
					</td>
            	</tr>
				<tr>
                	<td colspan="3" style="width: 100%;" > 
						<table style="width: 100%;">
							<tr >
								<td align="left" style="width: 20%;"><strong>N&deg; DE CONTROL:</strong> </td>
								<td align="left" style="width: 81%;"><strong>'.add_ceros($atrnum,6).'</strong>&nbsp;</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>INSTITUCIÓN:</strong> </td>
								<td align="left" style="width: 81%;">'.utf8_encode($aisdes).'</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>DIRECCI&Oacute;N:</strong> </td>
								<td align="left" style="width: 81%;"><span style="font-size:10">'.wordwrap(utf8_encode($aisobs),150,"<br>").'&nbsp;</span></td>
							</tr>	
						</table>
					</td>
            	</tr>
				<tr>
					<td colspan="3" style="width: 100%;" style="vertical-align:top;height:502px;border-bottom:none;"  >
						<table style="vertical-align:top;width: 100%;" cellpadding="0"  cellspacing="0"> 
							<tr>
								<td style="text-align:center;vertical-align:middle;border:solid;width:23%;font-size:11;border-bottom:none;" >
									<strong>C&Oacute;DIGO</strong>
								</td>
								<td style="text-align:center;border:solid;width:70%;vertical-align:middle;font-size:11;border-bottom:none;border-left:none;" >
									<strong>DESCRIPCI&Oacute;N</strong>
								</td>
								<td style="text-align:center;border:solid;width:7%;vertical-align:middle;font-size:11;border-bottom:none;border-left:none;" >
									<strong>CANT.</strong>
								</td>
							</tr>
							<tr>
								<td style="height:513px;text-align:center;border:solid;border-bottom:none;" >
									&nbsp;
								</td>
								<td style="text-align:center;border:solid;border-bottom:none;border-left:none;" >
									&nbsp;
								</td>
								<td style="text-align:center;border:solid;border-bottom:none;border-left:none;" >
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</page_header>
		<page_footer>
        	<table style="width: 100%;border: solid; border-bottom:none;border-right:none; border-left:none;" cellpadding="2px" cellspacing="0">
				<tr>
					<td style="border-right:solid; border-left:solid; text-align:left;" colspan="3">
						<strong>OBSERVACIONES:</strong>
					</td>
				</tr>
				<tr>
					<td style="height:43px;border-right:solid; border-left:solid; text-align:left; border-bottom:solid;" colspan="3">
						<div style="text-align:justify;">'.utf8_encode($observacion).' '.utf8_encode($atrobs).'</div>
					</td>
				</tr>
				<tr>
					<td style=" border-bottom:solid; border-right:solid; border-left:solid; text-align:center;">
						<strong>MEDITRON:</strong>
					</td>
					<td style=" border-bottom:solid; text-align:center;border-right:solid;">
						<strong>Institución: <span style="font-size:11">'.trim(utf8_encode($aisdes)).'</span></strong>
					</td>
				</tr>
        		<tr>
            		<td style="border-right:solid; border-left:solid; width: 50%;">
						<table style="font-size:10px; width: 100%;" cellspacing="10px">
							<tr>
								<td><strong>NOMBRE <br>Y APELLIDO:</strong></td>
								<td>______________________________________________________</td>
							</tr>
							<tr>
								<td><strong>C.I.:</strong></td>
								<td>______________________________________________________</td>
							</tr>
							<!--
							<tr>
								<td><strong>CARGO:</strong></td>
								<td>______________________________________________________</td>
							</tr>
							<tr>
								<td><strong>UNIDAD:</strong></td>
								<td>______________________________________________________</td>
							</tr>
							-->
							<tr>
								<td><strong>FIRMA:</strong></td>
								<td >______________________________________________________</td>
							</tr>
							<tr>
								<td><strong>FECHA:</strong></td>
								<td>_____/_____/_______</td>
							</tr>
							<tr>
								<td><strong>HORA:</strong></td>
								<td>____:____  ____</td>
							</tr>
						</table>
					</td>
            		<td style="width: 48%;border-right:solid;">
						<table style="font-size:10px; width: 100%;" cellspacing="10px">
							<tr>
								<td><strong>NOMBRE <br>Y APELLIDO:</strong></td>
								<td>_________________________________________________</td>
							</tr>
							<tr>
								<td><strong>C.I.:</strong></td>
								<td>_________________________________________________</td>
							</tr>
							<tr>
								<td><strong>CARGO:</strong></td>
								<td>_________________________________________________</td>
							</tr>
							<!--
							<tr>
								<td><strong>UNIDAD:</strong></td>
								<td>_________________________________________________</td>
							</tr>
							-->
							<tr>
								<td><strong>FIRMA:</strong></td>
								<td >_________________________________________________</td>
							</tr>
							<tr>
								<td><strong>FECHA:</strong></td>
								<td>_____/_____/_______</td>
							</tr>
							<tr>
								<td><strong>HORA:</strong></td>
								<td>____:____  ____</td>
							</tr>
						</table>
					</td>
        		</tr>';
	           
				$html.= '<tr style="font-size:10px;">
							<td style="border-top:solid;text-align:left;" colspan="3">
								F-LAB-040-01-23
							</td>
						</tr>';
				
        	$html.= '</table>
		</page_footer>';

		$cantidadTotal = 0;
		$cantidadTotalCN = 0;
		while(odbc_fetch_row($resulta3)){
			$aarcod =odbc_result($resulta3,'AARCOD');

			$sql3 ="SELECT ALTCOD FROM iv48fp t1 WHERE T1.ACICOD='$Compania' AND T1.AARCOD='$aarcod'";

			$result5=odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec. sql:*".$sql3."*"));
			while(odbc_fetch_row($result5)){
				$altcod =trim(odbc_result($result5,'ALTCOD')).", ";

			}
			$altcod = substr($altcod,0, (strripos($altcod,",")));
			
			if(empty($altcod)){
				$altcod = "(S/L)";
			}
			
			$modelo=odbc_result($resulta3,'MODELO');
			$descripcion=odbc_result($resulta3,'AARDES');
			$stsdet=odbc_result($resulta3,'ATDSTS');
			$cantidad=number_format(odbc_result($resulta3,'ATRCAN'),0,",",".");
			$cantidad222=odbc_result($resulta3,'ATRCAN');
			$unidad=odbc_result($resulta3,'ATRUMB');
			$aticod=odbc_result($resulta3,'ATICOD');
			$marca=odbc_result($resulta3,'AMCDES');

			$atrcut=odbc_result($resulta3,'ATRCUT'); //costo unitario

			$deslar=odbc_result($resulta3,'DESLAR');//DESCRIPCION LARGA
			$cntnv=odbc_result($resulta3,'CNTNV');//cantidad total nv
			$cn1nv=odbc_result($resulta3,'CN1NV');//cantidad 1 semestre nv
			$coditem=odbc_result($resulta3,'ITEM');//cod item
			$desitem=odbc_result($resulta3,'DESITEM');//descripcion item
			$cntbi=odbc_result($resulta3,'CNTBI');//cantidad total bi
			$cn1bi=odbc_result($resulta3,'CN1BI');//cantidad 1 semestre bi
			$cantidadTotal +=$cantidad222;
			$costoTotal +=$atrcut;
			$cantidadTotalCN +=($cantidad222 * $atrcut);
			//$nombreArt =empty($desitem)?utf8_encode($descripcion):utf8_encode($desitem.$descripcion);
			$nombreArt =utf8_encode($descripcion);
			$html.='<br />';
			
			$descripcionART = trim("");
			if(file_exists("../tiposdeinventarios/articulos/descripcion/".$Compania.'_'.trim($aarcod).".txt")){
				$file = fopen("../tiposdeinventarios/articulos/descripcion/".$Compania.'_'.trim($aarcod).".txt", "r");
				while(!feof($file)) {
					$descripcionART.= str_replace( "\n","<br />",( fgets($file) )  );
				}
				fclose($file);
			}else{
				$descripcionART .= trim("");
			}
			if(empty($descripcionART)){
				$descripcionART = ($deslar)!=''?( str_replace("\n","<br>",utf8_encode($deslar)) ):"";
			}

			$uni = "";

			if($unidad == 0005){
				$uni = 'µCi';
			}else{
				$uni = 'mCi';
			}
		
			$descripcionART= !empty(trim($descripcionART))?'<br />'.$descripcionART:'';
			$html.='<p class="codart">'.wordwrap(trim($aarcod),20,'<br/>', true).'</p>';
			$html.='<p class="cantidadc">'.trim($cantidad).'</p>';


			/*if($altcod != '(S/L)'){
				$html.='<p class="contenido">'.trim($cantMci).' '.$uni.'</p>';
			}else{
				$html.='';
			}*/

			$html.='<p class="descripcionc1">'.wordwrap('<strong>'.trim($nombreArt),75,'<br/>', true).'<br/> ';

			if($altcod != '(S/L)'){
				$html.='N° de Lote: '.$altcod.'</strong>'.wordwrap(trim($descripcionART),68,'<br/>', true).'</p>';
			}else{
				$html.='</strong></p>';
			}

			$class= 'descripcionc2';
		}

	$html.='</page>';

	/* Verificación de las fuentes */
	$html.='
	<style>
		.principal{
			width:100%; 
			margin-left:4px; 
			border-bottom: 1px solid;
			padding: 9px 0px 0px 0px;
		}
		.item{
			text-align: left;
			margin-left: 14px;
			margin-top: 0px;
			font-size: 11;
		}
		.conforme{
			margin-left: 655px;
			margin-top: -20px;
			text-align:justify;
		}
		.no-conforme{
			text-align: right;
			margin-right: 48px;
			margin-top: -32px;
		}
		.cuadro{
			border:1px solid; 
			width:15px;
			height:15px;
		}
	</style>
	<page backtop="70mm" backbottom="80mm" >
		<page_header>
			<table style="width:100%;align:center;" border="0" >
            	<tr>
                	<td>
                    	<img src="http://'.$direccion.'/idasysv3/images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5>
                	</td>
                	<td>
                    	<table style="width: 50%;margin-left:350px;font-size:10;">
                        	<tr align="left" style="width: 30%;"><td>P&Aacute;G. N&deg; [[page_cu]] DE [[page_nb]]</td></tr>
                        	<tr align="left" style="width: 30%;"><td>FECHA: '.date('d/m/Y').'</td></tr>
                        	<tr align="left" style="width: 30%;"><td>N° de control: '.add_ceros($atrnum,6).'</td></tr>
                    	</table>
                	</td>
            	</tr>
            	<tr>
             		<td colspan="2" style="text-align: center"> 
						<h3>VERIFICACIÓN DE LAS FUENTES</h3>
					</td>
            	</tr>
            	<tr>
                	<td colspan="3" style="width: 100%;">
						<table style="width: 100%;">
							<tr>
								<td align="left" style="width: 20%;"><strong>Institución:</strong> </td>
								<td align="left" style="width: 81%;">'.trim(utf8_encode($cliente)).'</td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Dirección:</strong> </td>
								<td align="left" style="width: 81%;"><span style="font-size:11">'.trim(utf8_encode($direccionServicio)).'</span></td>
							</tr>
							<tr>
								<td align="left" style="width: 20%;"><strong>Tipo de fuente:</strong> </td>
								<td align="left" style="width: 81%;"><strong>SELLADA</strong></td>
							</tr>
						</table>
					</td>
            	</tr>
            	<tr>
					<td colspan="3" style="width: 100%;" style="vertical-align:top;height: 502px;">
						<table style="vertical-align:top;width: 100%; margin-top:25px;" cellpadding="0" cellspacing="0"> 
							<tr>
								<td style="vertical-align:middle; width:80%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
									<strong>VERIFICACIÓN DEL CORRECTO ETIQUETADO</strong>
								</td>
								<td style="text-align:center; width:10%; vertical-align:middle; font-size:11; border-top:1px solid; border-bottom:1px solid; border-left:1px solid; padding:5px 0px 5px 0px;">
									<strong>SI</strong>
								</td>
								<td style="text-align:center; width:10%; vertical-align:middle; font-size:11; border-top:1px solid; border-bottom:1px solid; border-left:1px solid; border-right:1px solid; padding:5px 0px 5px 0px;">
									<strong>NO</strong>
								</td>
							</tr>
							<tr>
								<td style="height:149px; text-align:center; border-left:1px solid;">
									&nbsp;
								</td>
								<td style="text-align:center; border-left:1px solid;">
									&nbsp;
								</td>
								<td style="text-align:center; border-left:1px solid; border-right:1px solid;">
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
            </table>
		</page_header>
		<page_footer>
			<table style="width: 90%; border: 1px solid; border-bottom:none;border-right:none; border-left:none; margin-left: 1px;" cellpadding="2px" cellspacing="0">
				<tr>
					<td style="border-right:1px solid; border-left:1px solid; text-align:left; border-bottom:1px solid;" colspan="3">
						<strong>OBSERVACIONES:</strong>
					</td>
				</tr>
				<tr>
					<td style="height:100px;border-right:1px solid; border-left:1px solid; text-align:left; border-bottom:1px solid;" colspan="3"></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid; border-right:1px solid; border-left:1px solid; text-align:center; padding: 5px 0px 5px 0px;">
						<strong>Elaborado por:</strong>
					</td>
					<td style=" border-bottom:1px solid; text-align:center;border-right:1px solid; padding: 5px 0px 5px 0px;">
						<strong>Verificado por:</strong>
					</td>
					<td style=" border-bottom:1px solid; border-right:1px solid;text-align:center; padding: 5px 0px 5px 0px;">
						<strong>Recibido por:</strong>
					</td>
				</tr>
        		<tr>
            		<td style="border-right:1px solid; border-left:1px solid; width: 33%;">
						<table style="font-size:10px; width: 100%;" cellspacing="3px">
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Nombre y Apellido:</strong></td>
								<td>Jesus Alarcón</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Cargo:</strong></td>
								<td>Supervisor de Seguridad Radiológica</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Unidad:</strong></td>
								<td >Departamento de Laboratorio</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Firma:</strong></td>
								<td ><img style="width:50px; height: 50px;" src="http://'.$direccion.'/idasysv3/images/firmaJA.jpg"/></td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Fecha:</strong></td>
								<td><strong>Hora:</strong></td>
							</tr>
						</table>
					</td>
            		<td style="width: 33%;border-right:1px solid;">
						<table style="font-size:10px; width: 100%; padding: 5px 0px 5px 0px;" cellspacing="3px">
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Nombre y Apellido:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Cargo:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Unidad:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Firma:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Fecha:</strong></td>
								<td><strong>Hora:</strong></td>
							</tr>
						</table>
					</td>
					<td style="width: 33%; border-right:1px solid;">
						<table style="font-size:10px; width: 100%;" cellspacing="3px">
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Nombre y Apellido:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Cargo:</strong></td>
								<td>___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Unidad:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Firma:</strong></td>
								<td >___________________________</td>
							</tr>
							<tr>
								<td style="padding: 3px 0px 3px 0px;"><strong>Fecha:</strong></td>
								<td><strong>Hora:</strong></td>
							</tr>
						</table>
					</td>
        		</tr>
        		<tr style="font-size:10px;">
					<td style="border-top: 1px solid;text-align:left;" colspan="3">
						F-LAB-041-01-23
					</td>
				</tr>
			</table>
		</page_footer>
		<div class="principal">
			<div class="item">CONTENIDO: <span style="margin-left: 180px;"><strong>Contenedor del Generador de Tc-99m</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">CLASIFICACIÓN Y CATEGORÍA: <span style="margin-left: 140px;"><strong>CLASE 7</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">NÚMERO DE LAS NACIONES UNIDAS: <span style="margin-left: 107px;"><strong>UN-2915</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">ACTIVIDAD: </div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">ÍNDICE DE TRANSPORTE (IT): </div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<div class="principal">
			<div class="item">ETIQUETADO DE BULTO: <span style="margin-left: 165px;"><strong>AMARILLO III</strong></span></div>
			<div class="conforme">
				<div class="cuadro"></div>
			</div>
			<div class="no-conforme">
				<div class="cuadro"></div>
			</div>
		</div>
		<table style="vertical-align:top; width: 100%; margin-top:5px; margin-left: 3px;" cellpadding="0" cellspacing="0"> 
			<tr>
				<td style="vertical-align:middle; width:100%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-right:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
					<strong>VERIFICACIÓN EN EL DESPACHO DE LABORATORIO</strong>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					1. La Tasa de Dosis (TD) a un (1) metro de distancia del bulto, es igual o menor que: TD &lt;= IT/100 &nbsp;&nbsp;&nbsp;____ mSv/h&nbsp;&nbsp;&nbsp; ____ µSv/h
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis __________ µSv/h</p>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					2. La Tasa de Dosis (TD) en la superficie de las cuatro (4) caras del bulto es:  Mayor qué 5 µSv/h y Menor qué 500 µSv/h
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis &lt;__________ µSv/h</p>

				</td>
			</tr>
		</table>
		<table style="vertical-align:top; width: 100%; margin-top:5px; margin-left: 3px;" cellpadding="0" cellspacing="0"> 
			<tr>
				<td style="vertical-align:middle; width:100%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-right:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
					<strong>VERIFICACIÓN EN EL VEHÍCULO</strong>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					1. La Tasa de Dosis (TD) en la cabina del vehículo no es superior a: 20 µSv/h
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis __________ µSv/h</p>
				</td>
			</tr>
		</table>
		<table style="vertical-align:top; width: 100%; margin-top:5px; margin-left: 3px;" cellpadding="0" cellspacing="0"> 
			<tr>
				<td style="vertical-align:middle; width:100%; font-size:11; border-top:1px solid; border-bottom:1px solid; border-right:1px solid; border-left:1px solid; padding:5px 0px 5px 5px;">
					<strong>VERIFICACIÓN EN LA ENTREGA DE LA FUENTE EN EL HOSPITAL O INSTITUTO</strong>
				</td>
			</tr>
			<tr>
				<td style="font-size:11; border-left:1px solid; border-right:1px solid; border-bottom:1px solid; padding: 5px 0px 5px 5px;">
					1. La Tasa de Dosis (TD) en la superficie de las cuatro (4) caras del bulto es: ______________________________________
					<p style="text-align: center">SI __________ &nbsp;&nbsp;&nbsp; NO __________ &nbsp;&nbsp;&nbsp; Tasa de Dosis &lt;__________ µSv/h</p>
				</td>
			</tr>
		</table>
	</page>';

	/* Reacción adversa */
	$html.='
	<page>
		<table style="width:100%;" border="0">
        	<tr>
            	<td>
                	<img src="http://'.$direccion.'/idasysv3/images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5>
            	</td>
            	<td>
                	<table style="width: 50%;margin-left:350px;font-size:10;">
                    	<tr align="left" style="width: 30%;"><td>P&Aacute;G. N&deg; [[page_cu]] DE [[page_nb]]</td></tr>
                    	<tr align="left" style="width: 30%;"><td>FECHA: '.date('d/m/Y').'</td></tr>
                    	<tr align="left" style="width: 30%;"><td>N° de control: '.add_ceros($atrnum,6).'</td></tr>
                	</table>
            	</td>
        	</tr>
        	<tr>
         		<td colspan="2" style="text-align: center"> 
					<h3>REPORTE DE REACCIÓN ADVERSA</h3>
				</td>
        	</tr>
        </table>
        <table style="width:100%;" border="0" cellspacing="0">
        	<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>PASO 1</strong></td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>DATOS DE LA PLANILLA</strong></td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 33%; font-size: 11; border-bottom: none;">Planilla N°: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Fecha de creación: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Fecha de recepción:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>DATOS DEL PACIENTE</strong></td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Cédula de identidad: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Nombre y Apellido:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Historia N°: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Fecha de nacimiento:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 33%; font-size: 11; border-bottom: none;">Genero: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; F __ &nbsp;&nbsp;&nbsp;&nbsp; M __&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Edad: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Peso:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Estatura: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Raza:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">Historia Medica relevante (personal y Familiar), reacciones alergicas conocidas (fechas)</td>
			</tr>
			<tr>
				<td style="height: 100px; border-left: 1px solid; border-right: 1px solid;"></td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">Diagnóstico Principal: </td>
			</tr>
			<tr>
				<td style="border-left: 1px solid; border-right: 1px solid;">Otros: </td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>PASO 2</strong></td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>DATOS DE LOS MEDICAMENTOS SOSPECHOSOS</strong></td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">MEDICAMENTO 1</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Nombre del Medicamento:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Dosis*</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Vía de Administración:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Nombre del Medicamento:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Fecha del Inicio Tratamiento:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Fecha Fin del Tratamiento:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Fecha de Elaboración:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Fecha  Vencimiento del medicamento (si Aplica):</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Número de Lote:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Motivo de Uso*:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Código ICD-10:</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>PASO 3</strong></td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 33%; font-size: 11; border-bottom: none;">¿QUÉ DESEA NOTIFICAR DEL MEDICAMENTO?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							
							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REACCIÓN ADVERSA __</td>
							
							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FALTA DE EFICACIA __</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 20%; font-size: 11; border-bottom: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Reacción&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Fecha de Inicio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Fecha Fin&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Severidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Continua con la reacción</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 20%; font-size: 11; border-bottom: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __&nbsp;&nbsp;NO__ </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 20%; font-size: 11; border-bottom: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __&nbsp;&nbsp;NO__ </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 20%; font-size: 11; border-bottom: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __&nbsp;&nbsp;NO__ </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 33%; font-size: 11; border-bottom: none;">¿Continua con la Reacción?*</td>
							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __</td>
							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;NO__</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Tipo de Reporte: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Indicado por:</td>
						</tr>
					</table>
				</td>
			</tr>
        </table>
        <page_footer>
			<table style="width: 101%; border: 1px solid; border-bottom:none;border-right:none; border-left:none; margin-left: 3px;" cellpadding="2px" cellspacing="0">
        		<tr style="font-size:10px;">
					<td style="border-top: 1px solid;text-align:left;" colspan="3">
						F-RF-001-10-16
					</td>
				</tr>
			</table>
		</page_footer>
	</page>';

	$html.='
	<page>
		<table style="width:100%;" border="0">
        	<tr>
            	<td>
                	<img src="http://'.$direccion.'/idasysv3/images/'.$img.'"/><h5>R.I.F.: '.$Companiarif.'</h5>
            	</td>
            	<td>
                	<table style="width: 50%;margin-left:350px;font-size:10;">
                    	<tr align="left" style="width: 30%;"><td>P&Aacute;G. N&deg; [[page_cu]] DE [[page_nb]]</td></tr>
                    	<tr align="left" style="width: 30%;"><td>FECHA: '.date('d/m/Y').'</td></tr>
                    	<tr align="left" style="width: 30%;"><td>N° de control: '.add_ceros($atrnum,6).'</td></tr>
                	</table>
            	</td>
        	</tr>
        	<tr>
         		<td colspan="2" style="text-align: center"> 
					<h3>REPORTE DE REACCIÓN ADVERSA</h3>
				</td>
        	</tr>
        </table>
        <table style="width:100%;" border="0" cellspacing="0">  	
        	<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>PASO 4</strong></td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 20%; font-size: 11; border-bottom: none;">CONDUCTA* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Continuó la Terapia __</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Disminuyó la Dosis __</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suspendió la Terapia __</td>

							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desconocido __</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 20%; font-size: 11; border-bottom: none;" rowspan="3">EVOLUCIÓN* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;¿Al reducir la dosis o suspender el fármaco, desapareció la Reacción?</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO __</td>
						</tr>
						<tr>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;¿Se administró el Fármaco?</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO __</td>
						</tr>
						<tr>
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;¿Reapareció la Reacción?</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SI __</td>
							
							<td style = "text-align: center; border: solid; width: 20%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NO __</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 25%; font-size: 11; border-bottom: none;" rowspan="3">DESENLACE*</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Desconocido __</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;Resolución Espontánea __</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ameritó Tratamiento __</td>
						</tr>
						<tr>
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tratamiento concomitante __</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;Ameritó Tratamiento __</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Prolongó Hospitalización __</td>
						</tr>
						<tr>
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Incapacidad __</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;Muerte __</td>
							
							<td style = "text-align: center; border: solid; width: 25%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Malformación congénita __</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">OBSERVACIONES:</td>
			</tr>
			<tr>
				<td style="height: 100px; border-left: 1px solid; border-right: 1px solid;"></td>
			</tr>
			<tr>
				<td style="border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">RESULTADOS DE LA CAUSALIDAD:(SOLO PARA PROFESIONALES DE LA SALUD)</td>
			</tr>
			<tr>
				<td style="height: 100px; border-left: 1px solid; border-right: 1px solid;"></td>
			</tr>
			<tr>
				<td style="border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">COMENTARIOS DE LA UNIDAD DE FARMACOVIGILANCIA (SOLO PARA PROFESIONALES DE LA SALUD)</td>
			</tr>
			<tr>
				<td style="height: 100px; border-left: 1px solid; border-right: 1px solid;"></td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>PASO 5</strong></td>
			</tr>
			<tr>
				<td style="background-color: #ECECEC; border-left: 1px solid; border-top: 1px solid; border-right: 1px solid;"><strong>DATOS DEL NOTIFICADOR</strong></td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Notificador:* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Empresa o Institución:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">Apellidos y Nombres:</td>
			</tr>
			<tr>
				<td style="border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">Dirección:</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: none; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 33%; font-size: 11; border-bottom: none;">Estado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							
							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Ciudad:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							
							<td style = "text-align: center; border: solid; width: 33%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Teléfonos:</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="width: 100%; vertical-align: top; border-bottom: 1px solid; border-left: 1px solid; border-right: 1px solid; border-top: 1px solid;">
					<table style="vertical-align: top;" cellpadding = "0" cellspacing = "0" border="0"> 
						<tr>
							<td style = "text-align: center; vertical-align: middle; border: solid; width: 50%; font-size: 11; border-bottom: none;">Fax: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							
							<td style = "text-align: center; border: solid; width: 50%; vertical-align: middle; font-size: 11; border-bottom: none; border-left: none;">Correo Electrónico:</td>
						</tr>
					</table>
				</td>
			</tr>
        </table>
        <div style="margin-top: 25px; font-size: 11px; font-weight: bold">NOTA: EN CASO DE NO RECIBIR EL PRESENTE REPORTE, MEDITRON SE EXIME DE CUALQUIER TIPO DE RESPONSABILIDAD</div>
        <page_footer>
			<table style="width: 101%; border: 1px solid; border-bottom:none;border-right:none; border-left:none; margin-left: 3px;" cellpadding="2px" cellspacing="0">
        		<tr style="font-size:10px;">
					<td style="border-top: 1px solid;text-align:left;" colspan="3">
						F-RF-001-10-16
					</td>
				</tr>
			</table>
		</page_footer>
	</page>';
	/* Reacción adversa */
?>