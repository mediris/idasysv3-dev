<?php session_start();
	
	include_once("../conectar.php");
	require_once('html2pdf/html2pdf.class.php');

	$nrodot = trim($_GET['nrodot']);
	$alma = trim($_GET['alma']);
	$tipdot = trim($_GET['tipdot']);

	/* Select para consultar la informacion del articulo asociado a la dotacion */
	$sql = "SELECT T1.ACICOD, T1.AALCOD, T1.ADPCOD, T1.ATSCOD, T1.ADSNRO, T1.ATRCOD, T1.ATRNUM, T1.ATRDES, T1.ATROBS, T1.ATRFEC, T1.ATRHOR, T1.AUSCOD, T1.ADSSTS,  T2.AARCOD, T3.AARDES, T4.ALTCOD, T5.AISDES, T5.AISOBS, T5.AISCOD FROM IV35FP T1 INNER JOIN IV36FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATSCOD = T2.ATSCOD AND T1.ADSNRO = T2.ADSNRO) INNER JOIN IV05FP T3 ON (T1.ACICOD = T3.ACICOD AND T2.AARCOD = T3.AARCOD) LEFT JOIN IV48FP T4 ON (T1.ACICOD = T4.ACICOD  AND T2.AARCOD = T4.AARCOD) LEFT JOIN IV42FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AISCOD = T5.AISCOD) WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$alma."' AND T1.ADSNRO = '".$nrodot."' AND T1.ATSCOD = '".$tipdot."'";

	$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));

	/* Select para consultar los paramatros adicionales de la dotacion */
	$sqla= "SELECT T1.ACICOD, T1.ATSCOD, T1.ADSNRO, T1.ATRCOD, T4.APDCOD, T5.AAPVLA FROM IV35FP T1  INNER JOIN IV37FP T4 ON (T1.ACICOD = T4.ACICOD AND T4.ATRCOD = T1.ATRCOD AND T4.AMDCOD = 'IDAS') LEFT JOIN IV46FP T5 ON ( T1.ACICOD = T5.ACICOD AND T5.ADSNRO = T1.ADSNRO AND T4.APDCOD = T5.APACOD and T5.ATSCOD = T1.ATSCOD) WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$alma."' AND T1.ADSNRO = '".$nrodot."' AND T1.ATSCOD = '".$tipdot."'";

	$resulta= odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));

	ob_start();
	$paramAdicional = [];
	while(odbc_fetch_row($resulta)){
		array_push($paramAdicional, odbc_result($resulta, "AAPVLA"));
	}

	$paciente = odbc_result($result, "ATRDES");
	$cliente = odbc_result($result, "AISDES");
	$direccionServicio = odbc_result($result, "AISOBS");
	
	include('exportarpdfhtml_validacion_fuentes.php');
	$content = ob_get_clean();
	$html2pdf = new HTML2PDF('Portrait', 'letter', 'es');
	$html2pdf->WriteHTML($html);
	$html2pdf->Output('Validacion_fuentes'.$nrodot.'.pdf');
?>