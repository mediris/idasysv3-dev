<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Editar Dotaciones</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo trim($atrcod);?>','<?php echo trim($pedid);?>','<?php echo trim($almacen);?>'); cargarparametros(document.getElementById('atscod'),document.getElementById('atrnum').value )">
<div id="agregardiv">

<div>
<div id="ocultarform">
	<form id="agregarform" name="agregarform" method="post" class="form" action="">
    <input name="atrcod" id="atrcod" type="hidden" value="0052" />
	<table class="tabla1">
      <tr>
        <td width="29%"  scope="col"><label>C&oacute;digo y N&uacute;mero de Transacci&oacute;n</label></td>
        <td width="32%" scope="col">
        	<input name="atrnum" id="atrnum" type="hidden" value="" />
			<input name="aalcod" type="hidden" id="aalcod" value="<?php echo $almacen;?>" /> 
			<div align="left" id="nroreq"></div>
        </td>
        <td colspan="2" id="errarqtip"  scope="col">&nbsp;</td>
      </tr>
       <tr>
        <td  scope="col">Tipo de Dotaci&oacute;n: </td>
        <td width="32%" scope="col"><div align="left" id="watscod"></div>
        <input name="atscod" id="atscod" type="hidden" value="" /></td>
        <td colspan="2" id="erratscod"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col">Almac�n: </td>
        <td width="32%" scope="col"> 
        	<input name="aalcod" type="hidden" id="aalcod" value="<?php echo $almacen;?>" />
        	<div align="left" id="waalcod"></div>
        </td>
        <td colspan="2" id="erraalcod"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col"><?php echo $Compania=='42'?'Hospital: ':'Servicio:'; ?></td>
        <td width="32%" scope="col"><div align="left">
         	<select name="aiscod" id="aiscod">
			  <?php $sql="SELECT AISCOD, AISDES FROM IV42FP t1 WHERE t1.acicod='$Compania'";
                        $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));//si no es supervisor debe aparecer un textbox en modo lectura del usuario logueado o conectado, de lo contrario si aparece esta lista
                        
                        while(odbc_fetch_row($result1)){			
                                                $aiscod=trim(odbc_result($result1,1));
                                                $aisnom=trim(odbc_result($result1,2));?>
              <option value= "<?php echo $aiscod; ?>" ><?php echo $aisnom ?></option>
              <?php } ?>
            </select>&nbsp;&nbsp;
            
        </div></td>
        <td colspan="2" id="errarqtip"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td  scope="col">Nombre: </td>
        <td width="32%" scope="col">
        	<div align="left">
            	<input name="atrdes" type="text" id="atrdes" size="50" maxlength="50" />
        	</div>
        </td>
        <td colspan="2" id="erratrdes"  scope="col">&nbsp;</td>
      </tr>
      <tr>
        <td scope="col"><label>Observaci�n</label></td>
        <td scope="col"><div align="left">
          <textarea name="atrobs"  id="atrobs" cols="50" rows="5"/></textarea>
         
        </div></td>
        <td colspan="2" id="erratrobs" scope="col">&nbsp;</td>
      </tr>
      <tr>
            <td colspan="4">
                <table width="100%"  border="0" >
                  <tr>
                    <td width="100%" colspan="3" scope="col"><h3>Par�metros Adicionales: </h3>
                      <span class="header">
                      </span></td>
                    </tr>
                    <tr>
                        <td>
                        <div id="parametros">
                        </div>
                        </td>
                    </tr>
                  </table>
            </td>
      </tr>
      <tr>
        <th scope="col">&nbsp;</th>
        <th scope="col"></th>
        <th width="18%" scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar"></th>
        <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
        
      </tr>
    </table>

  </form></div>
  
  <div id="reqst06fp" align="center" style="display:none">
	<form id="solicitudes" name="solicitudes" class="form">
  <legend></legend>
  <table width="100%" class="tabla1">
  	<tr>
  	<td>
  			<table width="90%" height="94" border="0" cellpadding="0" cellspacing="4" align="center">
  	<tr>
    <td><strong>N�mero de Transacci&oacute;n</strong></td>
    <td>
    	<input name="atrnum" type="text"  ReadOnly/>
	    <input name="aalcod" type="hidden" id="aalcod" value="<?php echo $almacen;?>" />
    </td>
  	</tr>
    <tr>
    <td width="36%"><strong>Tipo de Dotaci�n</strong></td>
    <td width="64%"><div id="ord5"></div><input name="atscod" id="atscod" type="hidden" value="" /></td>
 	 </tr>
     <!--<tr>
    <td width="36%"><strong>Almac�n</strong></td>
    <td width="64%"><div id="ord6"></div></td>
 	 </tr>-->
    <tr>
    <tr>
    <td width="36%"><strong>Servicio</strong></td>
    <td width="64%"><div id="ord4"></div><div id="ord1"></div></td>
 	 </tr>
 	 
  	<tr>
    <td><strong>Observaci�n:</strong></td>
    <td><div id="ord3"></div></td>
  	</tr>
  
				</table>
 </td>
 </tr>
 <tr>
 <td>
  <table  id="grilla" width="100%"  border="0" >
    <thead>
      <tr>
        <th colspan="4" align="center" scope="col"><h3>Detalle de la salida por Dotaci&oacute;n: </h3>
          </th>
        </tr>
      <tr>	
        <th width="27%">Art�culo</th>
        <th width="19%">Cant. entregada</th>
        <th width="20%">Unid/Med </th>
        <th width="13%">Opciones</th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td>
          <div ><div align="left" id="ocultararticulo">
            <select id="aarcod" name="aarcod"  onchange="choiceart('parent',this,0,event)">
            </select>
          </div></div>
        </td>
        <td><div ><input type="text" name="aslcan" id="aslcan" size="15" maxlength="15" onchange="cambiarpuntoxcoma(this);activaimagengrabar(1);"  onkeypress="cambiarpuntoxcoma(this);"/></div></td>
        <td>
          <!--<div >
            <input name="aarumb" type="text" id="aarumb" size="6" maxlength="4"  ReadOnly>
            </div>-->
            <div id="div_aarnum">
            </div>
          </td>
        
        <td >
          <table width="100%">
            <tr>
              <td>
                <div id="imagengrabar" style="display:none"><a href="javascript:grabardetallereq();"><img src="../images/aprobar.png" width="16" height="16" border="0" /></a> </div>
              </td>
              <td>&nbsp;</td>
              <td><div id="imageneliminar" style="display:none"><a href="javascript:detallereqeliminar();"><img src="../images/rechazar.png" width="16" height="16" /></a> </div></td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
  </table>
  
       </td>
       </tr>
                <tr>
                    	
        	
        	<td width="21%" scope="col"><strong><p id='finalizarboton' onclick="finalizarreq('<?php echo trim($coddpto);?>')" class="subir" align="right">Finalizar</p></strong></td>
        	
   		  </tr>
        
          </table>
		</form> 
    </div>
     
</div>

</div>
<div align="center" id="agregaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
  <div align="center" id="espera" style="display:none;"><strong>Cargando Art&iacute;culos Asociados!</strong></div>
</body>
</html>