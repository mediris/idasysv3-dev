<?php 

 //$atscod = trim($_REQUEST["atscod"]);
 //$adsnro = trim($_REQUEST["adsnro"]);
 $acicod = trim($_REQUEST["acicod"]);
 $aalcod = trim($_REQUEST["aalcod"]);

$sql="SELECT T2.ADSNRO,T3.AISDES, T3.AISOBS, T2.ATRFEC,
(SELECT T9.AAPVLA FROM IV46FP T9 WHERE T9.ACICOD=T2.ACICOD and T9.ADSNRO=T2.ADSNRO AND T9.APACOD IN ('1509') ) AS ITEM, 
(SELECT T9.AAPVLA FROM IV46FP T9 WHERE T9.ACICOD=T2.ACICOD and T9.ADSNRO=T2.ADSNRO AND T9.APACOD IN ('1510') ) AS NRITEM 
FROM iv35fp t2 INNER JOIN IV42FP T3 ON (T2.ACICOD=T3.ACICOD AND T2.AISCOD=T3.AISCOD) 
     WHERE T2.ACICOD='".$Compania."' AND T2.ADSNRO =".$adsnro."  AND T2.ATSCOD='".$atscod."' AND T2.AALCOD='".$aalcod."'";

     $result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
while(odbc_fetch_row($result))
{
?>

<page backtop="35mm" backbottom="10mm" > 

<page_header>
    <table border="0" style="text-align:center;" align="center">
        <tr>
            <td align="center" style="width:210px;">
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_mpps.png" style="height:90px"/><br />
<!--                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_c_admirable.png" style="height:40px"/>-->
            </td>
            <td align="center" style="width:210px;">
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/convenio_medix.png"/>
            </td>
            <td align="center" style="width:210px;">
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_medix.png" style="height:40px"/>
                <p style="margin-left:25px;font-size:10;">P&Aacute;G. NRO: [[page_cu]] DE: [[page_nb]]</p>
            </td>
        </tr>
    </table>
</page_header>
<page_footer>
</page_footer>

<p class=MsoNormal style="margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm;font-size:13px;;letter-spacing:1.0pt;text-align:center;">
    <b>
        ACTA DE INSTALACIÓN DE BIENES
    </b>
</p>
<p class=MsoBodyText style="text-align:justify;line-height:125%;font-size:13px;border-bottom:thin;">
    Hoy, ___ del mes de __________________del año _______, en el <b><u>HOSPITAL <?php echo utf8_encode(odbc_result($result,'AISDES')); ?></u></b> 
        del Ministerio del Poder Popular para la Salud, ubicado en <b><u><?php echo utf8_encode(trim(odbc_result($result,'AISOBS'))); ?>,</u></b> se procedió a la Instalación del o los equipo (s), que fueron entregados según 
         <?php  
                /*
				if(odbc_result($result,'ADSNRO')=='712' || odbc_result($result,'ADSNRO')=='1129'){//712 y 1129
                    echo 'NOTAS DE ENTREGAS No. '.add_ceros('712',6)."/".date('Y')." y No. ".add_ceros('1129',6)."/".date('Y');
                    
                }else if(odbc_result($result,'ADSNRO')=='620' || odbc_result($result,'ADSNRO')=='1130'){//620 y 1130
                    echo 'NOTAS DE ENTREGAS No. '.add_ceros('620',6)."/".date('Y')." y No. ".add_ceros('1130',6)."/".date('Y');
                    
                }else
				*/
				{
                    echo '<b>NOTA DE ENTREGA No. <u>'.add_ceros( odbc_result($result,'ADSNRO'),6);
                }
        ?> de fecha <?php echo formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd-mm-aaaa' ); ?>,</u></b> los cuales se detallan a continuación:
    
<br /><br />
       <table style="border-collapse:collapse; border-bottom:-5px" border="1" cellspacing="0" align="center">
            <tr >
                <th style="width:135mm;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 11px;">NOMBRE Y DESCRIPCIÓN DEL BIEN</th>
                <th style="width:20mm;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 11px;">SERIAL</th>
                <th style="width:20mm;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 11px;">N° BIEN NACIONAL</th>
            </tr>
<?php 
			$sql="SELECT T2.AARDES, T1.AARCOD, T1.ATRCAN, T1.ALTCOD 
					FROM IV36FP T1 
						INNER JOIN IV05FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AARCOD=T2.AARCOD) 
					WHERE T1.ACICOD='".$Compania."' AND T1.ADSNRO =".$adsnro."  AND T1.ATSCOD='".$atscod."' AND T1.ALTCOD<>''";
			
			$resultDETA=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec"));
			while(odbc_fetch_row($resultDETA))
			{
				//echo $can>0? "<br />":"";
				//echo odbc_result($resultDETA, 'AARDES')." (".odbc_result($resultDETA, 'AARCOD').") Cantidad-".number_format(odbc_result($resultDETA, 'ATRCAN'),0)." ";		
?>
                <tr>
                    <td style="height:5mm; text-align:left;"><?php echo wordwrap( utf8_encode(odbc_result($resultDETA, 'AARDES')),65,'<br />',false );?></td>
                    <td style="height:5mm; text-align:center;"><?php echo utf8_encode( odbc_result($resultDETA, 'ALTCOD')) ;?></td>
                    <td></td>
                </tr>
<?php 
			}
?>
            
       </table>
<?php 
//echo $sql;
?>
<br /><br /><br /><br /><br /><br /><br /><br />
El (Los) equipo(s) instalado(s) fueron adquirido(s) mediante el Convenio Integral de Cooperación Internacional suscrito entre la República Bolivariana de Venezuela y la República de Argentina según Contrato No. <b>039/2015 y Addendum</b> 001 suscrito en fecha <b>14/10/2015,</b> para la "<b>Actualización Tecnológica de los Servicios Obstétricos y Neonatales</b>".
<br /><br />
En testimonio de lo expuesto, se deja constancia que el o los equipo(s) se encuentra en perfecto estado y funcionamiento. Se verifica con la firma del presente documento, en señal de aceptación y conformidad. Se hacen Cinco (5) ejemplares de un mismo tenor y a un sólo efecto.
</p>  
<br /><br /><br /><br /><br />
<table  style="width:100%; text-align:center">
    <tr>
        <td style="width:50%">
            <p class=MsoBodyText2 align=left style="text-align:left;line-height:125%"><b><span
            lang=ES-TRAD style="">&nbsp;&nbsp;&nbsp;JEFE DEL SERVICIO:</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">NOMBRE Y APELLIDO:

            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">C.I. N°:
            _____________________________________</span></p>

             <p class=MsoNormal align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">CARGO:____________________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">FIRMA Y SELLO:
            _____________________________</span></p>
        </td>
        <td style="width:50%">
            <p class=MsoBodyText2 align=left style="text-align:left;line-height:125%"><b><span
            lang=ES-TRAD style="">&nbsp;&nbsp;&nbsp;DIRECTOR DEL HOSPITAL:</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">NOMBRE Y APELLIDO:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">C.I. N°:
            _____________________________________</span></p>

             <p class=MsoNormal align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">CARGO:____________________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">FIRMA Y SELLO:
            _____________________________</span></p>
        </td>
    </tr>
    <tr>
        <td aling="center" colspan="2">
            <p class=MsoBodyText2 align=center style="text-align:left;line-height:125%"><b><span
            lang=ES-TRAD style="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            MEDITRON, C.A.:</span></b></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">NOMBRE Y APELLIDO:
            _______________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">C.I. N°:
            _____________________________________</span></p>

             <p class=MsoNormal align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">CARGO:____________________________________</span></p>
            
            <p class=MsoBodyText2 align=center style="text-align:center;line-height:125%"><span
            lang=ES-TRAD style="">FIRMA Y SELLO:
            _____________________________</span></p>
        </td>
    </tr>
</table>
</page>

<?php 
}
?>

<page backtop="35mm"> 

<page_header>
    <table border="0" style="text-align:center;" align="center">
        <tr>
            <td align="center" style="width:210px;">
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_mpps.png" style="height:90px"/><br />
<!--                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_c_admirable.png" style="height:40px"/>-->
            </td>
            <td align="center" style="width:210px;">
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/convenio_medix.png"/>
            </td>
            <td align="center" style="width:210px;">
                <img src="http://<?php echo $Direccionip;?>/idasysv3/images/logo_medix.png" style="height:40px"/>
                <p style="margin-left:25px;font-size:10;">P&Aacute;G. NRO: [[page_cu]] DE: [[page_nb]]</p>
            </td>
        </tr>
    </table>
    <p class=MsoNormal style="margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;margin-left:0cm;text-align:right">
        <b>
            <span lang=ES-TRAD style="font-size:13px;;letter-spacing:1.0pt;text-align:center">
                Documento de Entrega No. <?php echo odbc_result($result,'ADSNRO'); ?> de fecha <?php echo formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd-mm-aaaa' ); ?>
            </span>
        </b>
    </p>
</page_header>


<div class=WordSection1>
<p class=MsoBodyText style="text-align:center;line-height:125%">
    <span lang=ES-TRAD style="font-size:13px;line-height:125%;"> <b>PERSONAL MÉDICO, DE ENFERMERÍA Y ELECTROMEDICINA CAPACITADO EN EL MANEJO Y CONSERVACIÓN DE LA "ACTUALIZACIÓN TECNÓLOGICA PREVISTA EN EL CONTRATO N°039-2015 Y EL ADDENDUM 001-2016"</b>
    </span>
</p>
       <table style="width:50%;">
            <tr style="width:10%;">
                <td style="width:1%;text-align:left;vertical-align:right;height:7mm;"><b>HOSPITAL:</b> <?php echo utf8_encode(odbc_result($result,'AISDES')); ?></td>
                <td style="width:1%;text-align:center;vertical-align:right;height:7mm;"><b>SERVICIO:</b> __________________</td>
            </tr>
            <tr style="width:100%;">
                <td style="width:100%;text-align:left;vertical-align:right;height:7mm;"><b>UBICACIÓN:</b> <?php echo utf8_encode(trim(odbc_result($result,'AISOBS'))); ?></td>
            </tr>
            <tr style="width:100%;">
                <td style="width:100%;text-align:left;vertical-align:middle;"><b>FECHA:</b> __/__/____</td>
            </tr>
       </table>
<br /><br />
       <table style="width:100%;border-collapse:collapse;" border="1" cellspacing="0">
            <tr style="width:100%;">
                <th style="width:5%;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 11px;">No.</th>
                <th style="width:30%;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 10px;">NOMBRE Y APELLIDO</th>
                <th style="width:15%;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 10px;">NÚMERO DE CÉDULA</th>
                <th style="width:30%;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 10px;">CARGO</th>
                <th style="width:15%;text-align:center;vertical-align:middle;height:6mm;background-color:#F2F2F2;font-size: 10px;">FIRMA</th>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>1</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>2</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>3</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>4</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>5</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>6</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>7</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>8</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>9</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>10</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>11</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>12</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>13</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>14</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>
            <tr>
                <td style="text-align:center;height:7mm;vertical-align:middle;"><b>15</b></td>
                <td> </td>
                <td> </td>
                <td> </td>
                <td> </td>
            </tr>    
       </table>
<br /><br />    

</div>

</page>