<?php 
session_start();
include("../../conectar.php");

$adsnro = $_GET['pedid'];
$aalcod = $_GET['almacen'];
$atscod = $_GET['tipo'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cargar Actas</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/jquery.js"></script>
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script>

	

	function deleteActaEscan(carpeta,nombre,input){
		if(input=='pdfI'){
			/*MANEJO DE ACTA DE INSTALACION*/
			var inp = 'inputI';
			var cargados_info = 'cargados_infoI';
			var loadX = 'loadI';
			var cargados = 'cargadosI';
			//var imgpdf = 'pdf_I.jpg';
			var title = 'Acta de Instalación de Bienes';
		}else if(input=='pdfR'){
			/*MANEJO DE ACTA DE RECEPCION*/
			var inp = 'inputR';
			var cargados_info = 'cargados_infoR';
			var loadX = 'loadR';
			var cargados = 'cargadosR';
			//var imgpdf = 'pdf_R.jpg';
			var title = 'Acta de Recepción Individual';
		}else if(input=='pdfN'){
			/*MANEJO NOTA DE ENTREGA*/
			var inp = 'inputN';
			var cargados_info = 'cargados_infoN';
			var loadX = 'loadN';
			var cargados = 'cargadosN';
			//var imgpdf = 'pdf_N.jpg';
			var title = 'Nota de Entrga';
		}
		$('#'+inp).hide(1);
		$('#'+cargados_info).hide(1);
		$('#'+loadX).show(1);
		$('#'+cargados).hide(1);

		$.ajax({
			type: "POST",
			url: "eliminarpdf.php",
			data: "acicod="+$('input#compania').val()+
						   "&carpeta="+carpeta+
						   "&nombre="+nombre+
						   "&doctip=pdfI",
			beforeSend : function (){
				$('#'+loadX).show(1);
			},
			success: function(data){
				data= parseJSONx(data);
				if(data['val']==true){
					$('#'+inp).show(1);
					//alert(inp);
					
					//$('#'+inp).val(' ');
					//$('#'+inp).reset();
					//reset($('#'+inp));
					//document.getElementById(''+inp).value='';
					//$('#'+inp).html(' ');
					$('#'+cargados_info).hide(1);
					$('#'+cargados_info).html('');
					$('#'+loadX).hide(1);
					$('#'+cargados).html('');
					$('#'+cargados).hide(1);
				}else{
					$('#'+inp).hide(1);
					$('#'+cargados_info).show(1);
					$('#'+cargados_info).append(data['msj']);
					$('#'+loadX).hide(1);
					$('#'+cargados).hide(1);	
				}
			}
		});
	}

	function CargarArchivo(adsnro,aalcod,atscod,input){
			//alert(input.value);
			if(input.id=='pdfI'){
				/*MANEJO DE ACTA DE INSTALACION*/
				var inp = 'inputI';
				var cargados_info = 'cargados_infoI';
				var loadX = 'loadI';
				var cargados = 'cargadosI';
				var nombre = "acta_de_instalacion_bienes_"+add_ceros($.trim(adsnro),6)+".pdf";
				var imgpdf = 'pdf_I.jpg';
				var title = 'Acta de Instalación de Bienes';
			}else if(input.id=='pdfR'){
				/*MANEJO DE ACTA DE RECEPCION*/
				var inp = 'inputR';
				var cargados_info = 'cargados_infoR';
				var loadX = 'loadR';
				var cargados = 'cargadosR';
				var nombre = "acta_de_recepcion_individual_"+add_ceros($.trim(adsnro),6)+".pdf";
				var imgpdf = 'pdf_R.jpg';
				var title = 'Acta de Recepción Individual';
			}else if(input.id=='pdfN'){
				/*MANEJO NOTA DE ENTREGA*/
				var inp = 'inputN';
				var cargados_info = 'cargados_infoN';
				var loadX = 'loadN';
				var cargados = 'cargadosN';
				var nombre = "nota_de_entrega_"+add_ceros($.trim(adsnro),6)+".pdf";
				var imgpdf = 'pdf_N.jpg';
				var title = 'Nota de Entrga';
			}
			$('#'+inp).hide(1);
			$('#'+cargados_info).hide(1);
			$('#'+loadX).show(1);
			$('#'+cargados).hide(1);
			
			var archivos = input;//Damos el valor del input tipo file
			var archivo = archivos.files; //Obtenemos el valor del input (los arcchivos) en modo de arreglo
			var i=0;
		
			/* Creamos el objeto que hara la petición AJAX al servidor, debemos de validar 
			si existe el objeto “ XMLHttpRequest” ya que en internet explorer viejito no esta,
			y si no esta usamos “ActiveXObject” */ 
			if(window.XMLHttpRequest) { 
				var Req = new XMLHttpRequest(); 
			}else if(window.ActiveXObject) { 
				var Req = new ActiveXObject("Microsoft.XMLHTTP"); 
			}
			
			//El objeto FormData nos permite crear un formulario pasandole clave/valor para poder enviarlo, 
			//este tipo de objeto ya tiene la propiedad multipart/form-data para poder subir archivos
			var data = new FormData();
			
			/* validar tipo de archivo */
			//if( (archivo[i].type == 'image/jpeg' || archivo[i].type == 'image/png') && (archivo[i].size<=buscar_upload_max_filesize()) )
			if( (archivo[i].type == 'application/pdf') && (archivo[i].size<=buscar_upload_max_filesize()) )
			{	
				//Como no sabemos cuantos archivos subira el usuario, iteramos la variable y al
				//objeto de FormData con el metodo "append" le pasamos calve/valor, usamos el indice "i" para
				//que no se repita, si no lo usamos solo tendra el valor de la ultima iteración
				for(i=0; i<archivo.length; i++){
				   data.append('archivo'+i,archivo[i]);
				   //$('#ARCHICUP').val(archivo[i].name);
				}
				
				Req.open("POST", "cargar_archivo.php?adsnro="+adsnro+"&aalcod="+aalcod+"&atscod="+atscod+"&input="+input.id+"", true);
				
				/* Le damos un evento al request, esto quiere decir que cuando termine de hacer la petición,
				se ejecutara este fragmento de código */ 
				
				Req.onload = function(Event) {
					if (Req.status == 200) { 
					  //Recibimos la respuesta de php
					  var msg = Req.responseText;
					  $('#'+inp).hide(1);
					  $('#'+cargados_info).show(1);
					  $('#'+loadX).hide(1);
					  $('#'+cargados).show(1);
					  
					  $('#'+cargados_info).html('');
					  $('#'+cargados_info).append(msg);
					  
					  
					  var carpeta = ""+$('#compania').val()+"_"+aalcod+"_"+atscod+"_"+add_ceros(adsnro,6)+"/";
					  
					  $('#'+cargados).show(1);
					  $('#'+cargados).append('<a href="javascript:openActaEscan(\''+carpeta+'\',\''+nombre+'\')"><img src="../../images/'+imgpdf+'" title="'+title+'" alt="'+title+'" width="22" border="0"></a>');
					  $('#'+cargados).append('&nbsp;&nbsp;<a href="javascript:deleteActaEscan(\''+carpeta+'\',\''+nombre+'\',\''+input.id+'\');"><img src="../../images/eliminar.gif" title="Eliminar '+title+'" alt="Eliminar '+title+'" width="22" border="0"></a>');
					  
					} else { 
					  console.log(Req.status); //Vemos que paso. 
					} 
				};
				Req.send(data); 
			}else if(archivo[i].size>buscar_upload_max_filesize()){
				$('#'+loadX).hide(1);
				$('#'+cargados_info).html('');
				$('#'+cargados_info).append('<h12><strong>El archivo no puede pasar de '+(buscar_upload_max_filesize())+' Megas</strong></h2><br /><hr>');
				data = '';
			}else if(archivo[i].type != 'application/pdf'){
				$('#'+loadX).hide(1);
				$('#'+cargados_info).html('');
				//$("#cargadosCUPON").append('<h12><strong>Solo se aceptan archivos "TXT"</strong></h2><br /><hr>');
				$('#'+cargados_info).append('<h12><strong>Solo se aceptan archivos "PDF"</strong></h2><br /><hr>');
				data = '';
			}
	}
	
	//encontrar valores upload_max_filesize de servidor
	function buscar_upload_max_filesize(){
		var valor = '0<?php echo ini_get('upload_max_filesize');?>';
		valor = parseInt( valor.replace('M','') ) * 1048576 ; //valor en bytes
		return valor; 
	}
	
	$(document).ready(function(){
			/*MANEJO DE ACTA DE INSTALACION*/
			//$('#input').show(1);
			$("#subir input").each(function(index, element) {
                
				var inp = '';
				var cargados_info = '';
				var loadX = '';
				var cargados = '';
				var nombre = "";
				var imgpdf = '';
				var title = '';
				
				var doctip = this.id;
				
				if(doctip=='pdfI'){
					/*MANEJO DE ACTA DE INSTALACION*/
					inp = 'inputI';
					cargados_info = 'cargados_infoI';
					loadX = 'loadI';
					cargados = 'cargadosI';
					nombre = "acta_de_instalacion_bienes_"+add_ceros((adsnro),6)+".pdf";
					imgpdf = 'pdf_I.jpg';
					title = 'Acta de Instalación de Bienes';
				}else if(doctip=='pdfR'){
					/*MANEJO DE ACTA DE RECEPCION*/
					inp = 'inputR';
					cargados_info = 'cargados_infoR';
					loadX = 'loadR';
					cargados = 'cargadosR';
					nombre = "acta_de_recepcion_individual_"+add_ceros((adsnro),6)+".pdf";
					imgpdf = 'pdf_R.jpg';
					title = 'Acta de Recepción Individual';
				}else if(doctip=='pdfN'){
					/*MANEJO NOTA DE ENTREGA*/
					inp = 'inputN';
					cargados_info = 'cargados_infoN';
					loadX = 'loadN';
					cargados = 'cargadosN';
					nombre = "nota_de_entrega_"+add_ceros((adsnro),6)+".pdf";
					imgpdf = 'pdf_N.jpg';
					title = 'Nota de Entrga';
				}else{
					inp = '';
					cargados_info = '';
					loadX = '';
					cargados = '';
					nombre = "";
					imgpdf = '';
					title = '';
				}
				if(inp != ''){
					$.ajax({
						type: "POST",
						url: "validarPDFExis.php",
						data: "acicod="+$('input#compania').val()+
									   "&adsnro="+$('input#adsnro').val()+
									   "&aalcod="+$('input#aalcod').val()+
									   "&atscod="+$('input#atscod').val()+
									   "&doctip="+this.id,
						beforeSend : function (){
							$('#'+loadX).show(1);	
						},
						success: function(data){
							data= parseJSONx(data);
							if(data['val']==true){
								//alert(data['modi']);
								$('#'+loadX).hide(1);
								$('#'+inp).hide(1);
								
								$('#'+cargados_info).show(1);
								$('#'+cargados_info).append("<strong>Nombre: </strong>"+data['nomb']+"<br />");
								$('#'+cargados_info).append("<strong>Modificado: </strong>"+data['modi']+"<br />");
								$('#'+cargados_info).append("<strong>Tamaño: </strong>"+data['tama']+" Mb<br />");
								$('#'+cargados_info).append("<strong>Tipo: </strong>"+data['tipo']+"<br />");
							
								$('#'+cargados).show(1);
								$('#'+cargados).append('<a href="javascript:openActaEscan(\''+data['carp']+'\',\''+data['nomb']+'\')"><img src="../../images/'+imgpdf+'" title="'+title+'" alt="'+title+'" width="22" border="0"></a>');
								$('#'+cargados).append('&nbsp;&nbsp;<a href="javascript:deleteActaEscan(\''+data['carp']+'\',\''+data['nomb']+'\',\''+doctip+'\');"><img src="../../images/eliminar.gif" title="'+title+'" alt="Eliminar '+title+'" width="22" border="0"></a>');
							}else{
								$('#'+loadX).hide(1);
								$('#'+inp).show(1);
								
							}
						}
					});	
				}
				
            });
			
			
	});
</script>


</head>

<body>
	<form id="subir" >
    	<input type="hidden" name="compania" id="compania" value="<?php echo $Compania;?>"  />
        <input type="hidden" name="adsnro" id="adsnro" value="<?php echo $adsnro;?>"  />
        <input type="hidden" name="aalcod" id="aalcod" value="<?php echo $aalcod;?>"  />
        <input type="hidden" name="atscod" id="atscod" value="<?php echo $atscod;?>"  />
        
        <table style="" align="center">
            <tr>
                <td>Seleccione Acta de Instalación:</td>
                <td>
                	<div id="inputI" style="display:">
						<input type="file" name="pdfI" id="pdfI" accept="application/pdf"  onchange="CargarArchivo('<?php echo $adsnro?>', '<?php echo $aalcod?>', '<?php echo $atscod?>', this)"/>
                    </div>
                   	<div id="cargados_infoI" style="float: inherit;width: 100%;">
                        <!-- Aqui van los archivos cargados -->
                    </div>
                </td>
                <td>
                    <div id="loadI" style="display:none"><img src="../../images/loader.gif"/></div>
                    <div id="cargadosI" style="float: inherit;width: 100%;">
                        <!-- Aqui van los archivos cargados -->
                    </div>
              </td>
            </tr>
            <?php if($Compania!='42'){ ?>
            <tr>
                <td>Seleccione Acta de Recepción:</td>
                <td>
                	<div id="inputR" style="display:">
						<input type="file" name="pdfR" id="pdfR" accept="application/pdf"  onchange="CargarArchivo('<?php echo $adsnro?>', '<?php echo $aalcod?>', '<?php echo $atscod?>', this)"/>
                    </div>
                   	<div id="cargados_infoR" style="float: inherit;width: 100%;">
                        <!-- Aqui van los archivos cargados -->
                    </div>
                </td>
                <td>
                    <div id="loadR" style="display:none"><img src="../../images/loader.gif"/></div>
                    <div id="cargadosR" style="float: inherit;width: 100%;">
                        <!-- Aqui van los archivos cargados -->
                    </div>
              </td>
            </tr>
            <?php } ?>
            <tr>
                <td>Seleccione Nota de Entrega:</td>
                <td>
                	<div id="inputN" style="display:">
						<input type="file" name="pdfN" id="pdfN" accept="application/pdf"  onchange="CargarArchivo('<?php echo $adsnro?>', '<?php echo $aalcod?>', '<?php echo $atscod?>', this)"/>
                    </div>
                   	<div id="cargados_infoN" style="float: inherit;width: 100%;">
                        <!-- Aqui van los archivos cargados -->
                    </div>
                </td>
                <td>
                    <div id="loadN" style="display:none"><img src="../../images/loader.gif"/></div>
                    <div id="cargadosN" style="float: inherit;width: 100%;">
                        <!-- Aqui van los archivos cargados -->
                    </div>
              </td>
            </tr>
            <tr>
            	<td colspan="3" style="text-align:center">
                	<input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar">
                </td>
            </tr>
        </table>
	</form>
</body>
</html>