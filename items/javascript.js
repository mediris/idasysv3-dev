/*
* jdavila 
* 08/09/2015
*/
function agregar()
{
	//$("#compcod").val('');
	// $("#aprcod ").html('');
	$('input[name="aitcod"]').val('');
	$("#wsaitcod").html('');
	$("#aitdes").val('');
	$("#aitobs").val('');
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "itemsagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
26/03/12
*/
function editar(comp,cod) {

	var param = [];
	param['compcod']=comp;
	param['aitcod']=cod;
	ejecutasqlp("itemsinformacionphp.php",param);

	for(i in gdata)
	{
		$("#compcod").val(gdata[i].ACICOD);
//		$("#aprcod ").html(gdata[i].APRCOD);
		$('input[name="aitcod"]').val(gdata[i].AITCOD);
		$("#wsaitcod").html(gdata[i].AITCOD);
		$("#aitdes").val(gdata[i].AITDES);
		$("#aitobs").val(gdata[i].AITOBS);
	};
	$("#aftsav").hide(1);
}

/*
jDavila
26/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "itemseditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
26/03/12
*/
function eliminar(comp,cod) {

	
	if (confirm('Seguro que desea borrar el Item ' + cod + '?'))
	{
		var param = [];
		param['compcod']=comp;
		param['aitcod']=cod;
		ejecutasqld("itemseliminar.php",param);
		location.reload();
	}
}


