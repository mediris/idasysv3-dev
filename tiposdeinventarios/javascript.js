/*
jDavila
07/03/12
*/
function agregar()
{
	document.form.aticod.value="";
	document.form.atides.value="";
	document.form.atieca1.value="";
	document.form.atieca2.value="";
	document.form.atieca3.value="";
	document.form.atieca4.value="";
	document.form.atiniv1.value="";
	document.form.atiniv2.value="";
	document.form.atiniv3.value="";
	document.form.atiniv4.value="";
	document.form.atid1n.value="";
	document.form.atid2n.value="";
	document.form.atid3n.value="";
	document.form.atid4n.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
07/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tiposinventariosagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
07/03/12
*/
function editar(tipo) {

	var param = [];
	param['aticod']=tipo;
	ejecutasqlp("tiposinventariosinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.haticod.value=gdata[i].ATICOD;
		document.getElementById("wsaticod").innerHTML=gdata[i].ATICOD;
		document.form.atides.value=gdata[i].ATIDES;
		document.form.atiniv1.value=gdata[i].ATINIV1;
		document.form.atiniv2.value=gdata[i].ATINIV2;
		document.form.atiniv3.value=gdata[i].ATINIV3;
		document.form.atiniv4.value=gdata[i].ATINIV4;
		document.form.atieca1.value=gdata[i].ATIECA1;
		document.form.atieca2.value=gdata[i].ATIECA2;
		document.form.atieca3.value=gdata[i].ATIECA3;
		document.form.atieca4.value=gdata[i].ATIECA4;
		document.form.atid1n.value=gdata[i].ATID1N;
		document.form.atid2n.value=gdata[i].ATID2N;
		document.form.atid3n.value=gdata[i].ATID3N;
		document.form.atid4n.value=gdata[i].ATID4N;
		if(gdata[i].ATIAUT=='N') {document.form.atiaut[1].checked=true;}
		if(gdata[i].ATIAUT=='S') {document.form.atiaut[0].checked=true;}
		var tipocosto=gdata[i].ATITCO;
		for(i=0;i<document.form.atitco.length;i++)
		{
			var tipocostos=document.form.atitco.options[i].value;
			if(tipocostos==tipocosto)
			{
				document.form.atitco.options[i].selected=true;
			}
		} 

		validaetotalcaracteresan();
		validaetotalcaracteresa();
	};
	$("#aftsav").hide(1);
}

/*
jDavila
07/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "tiposinventarioseditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
07/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar el tipo de inventario' + tipo + '?'))
	{
		var param = [];
		param['aticod']=tipo;
		ejecutasqld("tiposinventarioeliminar.php",param);
		location.reload();
	}
}


/**/
function validatotalcaracteresan(nivel) {

		if (document.form.atiniv1.value=="") {
			niv1=0;
		}
		else{
			niv1=parseInt(document.form.atiniv1.value);
		}
		if (document.form.atiniv2.value=="") {
			niv2=0;
		}
		else{
			niv2=parseInt(document.form.atiniv2.value);
		}
		if (document.form.atiniv3.value=="") {
			niv3=0;
		}
		else{
			niv3=parseInt(document.form.atiniv3.value);
		}
		if (document.form.atiniv4.value=="") {
			niv4=0;
		}
		else{
			niv4=parseInt(document.form.atiniv4.value);
		}

		nivs=15-(niv1+niv2+niv3+niv4);
		document.form.atinivs.value=nivs;
}
function validatotalcaracteresa(nivel) {
	
		if (document.form.atieca1.value=="") {
			niv1=0;document.getElementById('nivel1').style.display='none';
		}
		else{
			niv1=parseInt(document.form.atieca1.value);
			//document.getElementById('nivel1').style.display='block';
			document.getElementById('nivel1').style.display='';
		}
		if (document.form.atieca2.value=="") {
			niv2=0;document.getElementById('nivel2').style.display='none';
		}
		else{
			niv2=parseInt(document.form.atieca2.value);
			//document.getElementById('nivel2').style.display='block';
			document.getElementById('nivel2').style.display='';
		}
		if (document.form.atieca3.value=="") {
			niv3=0;document.getElementById('nivel3').style.display='none';
		}
		else{
			niv3=parseInt(document.form.atieca3.value);
			//document.getElementById('nivel3').style.display='block';
			document.getElementById('nivel3').style.display='';
		}
		if (document.form.atieca4.value=="") {
			niv4=0;document.getElementById('nivel4').style.display='none';
		}
		else{
			niv4=parseInt(document.form.atieca4.value);
			//document.getElementById('nivel4').style.display='block';
			document.getElementById('nivel4').style.display='';
		}
		
		nivs=30-(niv1+niv2+niv3+niv4);
		document.form.atiecas.value=nivs;
	
}
function automatizar()
{
	document.form.atieca1.value=9;
	document.form.atieca2.value="";
	document.form.atieca3.value="";
	document.form.atieca4.value="";
	
	document.form.atieca1.readOnly=true;
	document.form.atieca2.readOnly=true;
	document.form.atieca3.readOnly=true; 
	document.form.atieca4.readOnly=true; 
	validatotalcaracteresa(this);
}
function desautomatizar()
{
	document.form.atieca1.value="";
	document.form.atieca2.value="";
	document.form.atieca3.value="";
	document.form.atieca4.value="";
	
	document.form.atieca1.readOnly=false;
	document.form.atieca2.readOnly=false;
	document.form.atieca3.readOnly=false;
	document.form.atieca4.readOnly=false;
	validatotalcaracteresa(this);
}
function buscarparametrosa(tinventario){
		var x;
 		x=$("#pa");
  		x.empty();
		ejecutasql("buscarparametrosaphp.php",tinventario);
		for(i in gdata)
		{
			var ul = $("#pa");
			var check= (gdata[i].APDTIP!="*") ? " checked": " ";
			ul.append("<li id='pr"+gdata[i].APDCOD+"'><input type='checkbox' name='parama["+i+"]' value='"+gdata[i].APDCOD+"'"+check+"/>"+gdata[i].APDDES+"<ul id='ul"+gdata[i].APDCOD+"'></ul></li>");
		}
}


function validaetotalcaracteresan(nivel) {

	if (document.form.atiniv1.value=="") {niv1=0;}
	else									   {niv1=parseInt(document.form.atiniv1.value);}
	if (document.form.atiniv2.value=="") {niv2=0;}
	else									   {niv2=parseInt(document.form.atiniv2.value);}
	if (document.form.atiniv3.value=="") {niv3=0;}
	else									   {niv3=parseInt(document.form.atiniv3.value);}
	if (document.form.atiniv4.value=="") {niv4=0;}
	else									   {niv4=parseInt(document.form.atiniv4.value);}

	nivs=15-(niv1+niv2+niv3+niv4);
	document.form.atinivs.value=nivs;
}
function validaetotalcaracteresa(nivel) {

	if (document.form.atieca1.value=="") {niv1=0;document.getElementById('nivel1').style.display='none';}
	else{
		niv1=parseInt(document.form.atieca1.value);
		//document.getElementById('nivel1').style.display='block';
		document.getElementById('nivel1').style.display='';
	}
	if (document.form.atieca2.value=="") {niv2=0;document.getElementById('nivel2').style.display='none';}
	else{
		niv2=parseInt(document.form.atieca2.value);
		//document.getElementById('nivel2').style.display='block';
		document.getElementById('nivel2').style.display='';
	}
	if (document.form.atieca3.value=="") {niv3=0;document.getElementById('nivel3').style.display='none';}
	else{
		niv3=parseInt(document.form.atieca3.value);
		//document.getElementById('nivel3').style.display='block';
		document.getElementById('nivel3').style.display='';
	}
	if (document.form.atieca4.value=="") {niv4=0;document.getElementById('nivel4').style.display='none';}
	else{
		niv4=parseInt(document.form.atieca4.value);
		//document.getElementById('nivel4').style.display='block';
		document.getElementById('nivel4').style.display='';
	}

	nivs=30-(niv1+niv2+niv3+niv4);
	document.form.atiecas.value=nivs;
}