<?php
/*
 * jDavila
 * 14/03/2012
 */
	session_start();
	header("Content-type: text/javascript; charset=iso-8859-1"); 
	include("../../conectar.php");
	include("../../JSON.php");
	if ($Usuario) { 
		$aarcod=trim($cadena["aarcod"]);//iv05fp
		$aardes=$cadena["aardes"];//iv05fp
		$tinventario=$_POST['tinventario'];
		$stinventario=$_POST['stinventariob'];
		$amccod=$cadena["amccod"];//iv06fp
		$aarumb=$cadena["aarumb"];//iv06fp
		$aarlot=$cadena["aarlot"];//iv06fp
		$aarser=$cadena["aarser"];//iv06fp
		$paradi=$_POST["paradi"];
		$almacen=$_POST["almacen"];
		$stok=$_POST["stok"];
		$aardesex = trim($_POST["aardesex"]);
		

		$i=0;
		$json = new Services_JSON();
		/* Validar Error */				
		if (strlen($aarcod)==0) 
		{
			$i++;$i++;$mensaje[$i]["campo"]="erraarcod";
			$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"art_10001")));
		}
		else 
		{ 	
			//$sql="Select * from iv05fp where aarcod='$aarcod'";
			$sql="Select * from iv05fp where acicod='".$Compania."' and aarcod='".$aarcod."'";
			$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
			if (odbc_result($result,1)) 
			{
				$i++;$mensaje[$i]["campo"]="erraarcod";
				$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"art_10002"))); 
			}
		}
		
		if (strlen($aardes)==0) 
		{
			$i++;$i++;$mensaje[$i]["campo"]="erraardes";
			$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"art_20001")));
		}
		if (strlen($aardes)>100) 
		{
			$i++;$i++;$mensaje[$i]["campo"]="erraardes";
			$mensaje[$i]["msg"]=ICONV( "ISO-8859-1", "UTF-8", trim(buscaerror($idioma,"art_20002")));
		}		
		/* si hay error */
		if ($i>0) 	{
		echo $json->encode($mensaje);
		}
		/* si no hubo error */			
		else
		{
		
			$art = new inf_articulo($Compania,$aarcod);
			$tinv = new inf_tinventario($Compania, $tinventario);
			$nivel=$art->nivel_articulo($Compania, $tinventario,trim($aarcod));
			$ntt=$tinv->batiecat;
		
			if (strlen($aarcod)>=$ntt) {$tipoart='D';} else {$tipoart='T';}
		
		
			$sql="INSERT INTO IV05FP (acicod, aarcod, aardes, aticod, asicod, aartdt, aarniv, aarsts)VALUES('$Compania', '$aarcod', '$aardes', '$tinventario', '$stinventario', '$tipoart','$nivel', '01')";
			$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
			auditoriagrabar($modulo,"IV05FP","$Compania;$aarcod","Se agrego el Art. $aarcod Satisfactoriamente");
			/*guardar descripcion extendida*/
			/*valido si el archivo existe*/
			try{

				$listaNO = array("\\", "/", ":", "*", "?", "â", "€", "â€", "<", ">", "|", '"', "#", "%");
				$aarcodFile = str_replace($listaNO, "_", $aarcod);
				if(file_exists("descripcion/".$Compania.'_'.$aarcodFile.".txt")){
					if($file = fopen("descripcion/".$Compania.'_'.$aarcodFile.".txt", "w")){
						ftruncate($file, 0);
						fwrite($file, $aardesex);
						fclose($file);
					}
				}else{
					if($file = fopen("descripcion/".$Compania.'_'.$aarcodFile.".txt", "x")){
						ftruncate($file, 0);
						fwrite($file, $aardesex);
						fclose($file);
					}
					
				}
			}catch(Exception $e){
				echo "";
			}
			
			
			/*convercion por almacen*/
			if($almacen)
			{
				//print_r($almacen);
				foreach($almacen as $keyj => $valuej) 	
				{
					$key=utf8_decode($keyj);
					$value=utf8_decode($valuej);
					if(!empty($value))
					{
						$sql = "INSERT INTO IV39FP (ACICOD, AARCOD, AALCOD, AARUMB, AARSTM) VALUES ('$Compania', '$aarcod', '$key', '$value',".$stok[$key].")" ;
						$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
						auditoriagrabar($modulo,"IV39FP","$Compania;$aarcod;$key","Cambiado parametro adic. $aarcod-$key Satisf.");
					}
				}//fin foreach
			}
			if ($tipoart=='D') 
			{
		
				$sql="INSERT INTO IV06FP (acicod, aarcod, amccod, aarumb, aarlot, aarser) VALUES ('$Compania', '$aarcod', '$amccod', '$aarumb', '$aarlot', '$aarser')";
				$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
				auditoriagrabar($modulo,"IV06FP","$Compania;$aarcod","Se agrego el Art. $aarcod Satisfactoriamente");
				/*parametros adicionales*/
				if($paradi)
				{
					foreach($paradi as $keyj => $valuej) 	
					{
						$key=utf8_decode($keyj);
						$value=utf8_decode($valuej);
						$pa = new inf_parametroadic($Compania, $key); 
						if(!empty($value))
						{
							$sql="INSERT INTO IV17FP (ACICOD, APACOD, AARCOD, AAPDES, AAPLON, AAPLND, AAPSEC, AAPTIP, AAPVLA) VALUES('$Compania', '$key', '$aarcod', '$pa->baapdes', '$pa->baaplon' , '$pa->baaplnd', '$pa->baapsec', '$pa->baaptip', '$value')";
							$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
							auditoriagrabar($modulo,"IV17FP","$Compania;$aarcod;$key","Cambiado parametro adic. $aarcod-$key Satisf.");
						}
					}//fin foreach
				}
				
				
			}//fin if
		}//fin else
	}
?>