(CAMA PARA SALA DE TRABAJO DE PARTO) ITEM 40:

SMP-DSHW-1T-2L-060 Unidad de Lámpara Cabecera con toma de gases (oxígeno, aire y vacío), dos tomas de servicios eléctricos 125V/20A, provisión para llamado de enfermera y doble lámpara, Marca: SMP CANADA, Serie: DIAMOND.

SMP-400M-OBT Mesa de Comer, Marca: SMP CANADA, Modelo: SMP-400M-OBT con sistema ABS, tope de 40 x 80 cm, altura regulable de 79 - 104 cm con mecanismo gas/spring, superficie de laminado y ruedas bloqueables.
  
SMP-301M-BSC Gabinete Lateral; Marca: SMP CANADA, Modelo: SMP-301M-BSC con una gaveta, repisa y cerradura.