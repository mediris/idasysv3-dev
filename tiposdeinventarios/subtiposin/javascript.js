/*
jDavila
07/03/12
*/
function agregar()
{
	document.agregarform.asicod.value="";
	document.agregarform.asides.value="";
	
	$("#agregaraftsav").hide(1);
}

/*
jDavila
08/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "subtipoinagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}
	});
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
jDavila
07/03/12
*/
function editar(tipo, stipo) {

	var param = [];
	param['aticod']=tipo;
	param['asicod']=stipo;
	ejecutasqlp("subtipoininformacionphp.php",param);
	for(i in gdata)
	{
		document.editarform.asicod.value=gdata[i].ASICOD;
		document.getElementById("wsasicod").innerHTML=gdata[i].ASICOD;
		document.editarform.asides.value=gdata[i].ASIDES;
	};
	$("#editaraftsav").hide(1);
}

/*
jDavila
07/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "subtipoineditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}

}

/*
jDavila
07/03/12
*/
function eliminar(tipo, stipo) {
	if (confirm('Seguro que desea borrar el subtipo de inventario ' + stipo + ' perteneciente al tipo inventario ' + tipo + ' ?'))
	{
		var param = [];
		param['aticod']=tipo;
		param['asicod']=stipo;
		ejecutasqld("subtipoineliminar.php",param);
		location.reload();
	}
}
/**/


