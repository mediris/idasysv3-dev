/*
 * jDavila
 * 16/02/12
 * modificado: 22/02/2012
 */
function agregar()
{
	document.agregarform.atscod.value="";
	document.agregarform.atsdes.value="";
	//document.agregarform.aticod.value="";
	/*
	for(var i = 0; i < document.agregarform.aticod.length; i++){
		document.agregarform.aticod[i].checked = true;
	}
	*/
	document.agregarform.atrsal.value="";
	var i
    for (i=0;i<document.agregarform.atrspr.length;i++){
       if (document.agregarform.atrspr[i].value=='N')
	   	document.agregarform.atrspr[i].checked = 'checked';
	}
	$("#agregaraftsav").hide(1);
}

/*
jDavila
16/02/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tipodotacionagregarvalidarphp.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 * jDavila
 * 13/02/12
 * modificado: 22/02/2012
 */
function editar(astcod) {

	var param = [];
	param['astcod']=astcod;

	/*cual es la diferencia*/
	ejecutasqlp("tipodotacioninformacionphp.php",param);

	for(i in gdata){
		document.editarform.hatscod.value=gdata[i].ATSCOD;
		document.getElementById("wsatscod").innerHTML=gdata[i].ATSCOD;
		document.editarform.atsdes.value=gdata[i].ATSDES;
		/*utilizada en el select */
		/*document.editarform.aticod.value=gdata[i].ATICOD;*/
		var num = gdata[i].ATICOD
		document.getElementById("aticod["+num+"]").checked = 1;
		
		document.editarform.atrsal.value=gdata[i].ATRSAL;
		document.editarform.atrent.value=gdata[i].ATRENT;
		
		var i
		 var tem = gdata[i].ATSAPR;
		for (i=0;i<document.editarform.atrspr.length;i++){
		   if(document.editarform.atrspr[i].value == tem){
			  document.editarform.atrspr[i].checked = 'checked';
		   }
		} 
	};
	$("#editaraftsav").hide(1);
}

/*
 *jDavila
 *15/02/2012
 */
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "tipodotacioneditarvalidarphp.php",
			data: $("#editarform").serialize(),
			success: function(datos){
				vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#editarform").hide(1000);
		$("#editaraftsav").show(1000);	
	}
}


/*
 *jDavila
 *15/02/2012
 */
function eliminar(tiporeq) {
	if (confirm('Seguro que desea borrar el tipo de dotaci\u00f3n ' + tiporeq + '?'))
	{
		var param = [];
		param['atscod']=tiporeq;
		ejecutasqld("tipodotacioneliminar.php",param);
		location.reload();
	}
}

/*
 *jDavila
 *03/12/2014
 */
function agregarusu()
{
	document.agregarform.auscod.value="";

	$("#agregaraftsav").hide(1);
}

/*
 *jDavila
 *03/12/2014
 */
function agregarvalidarsus(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tipodotacionusuarioagregarvalidar.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}

/*
 *jDavila
 *03/12/2014
 */
function eliminarsus(id, atscod, atsdes) {
	if (confirm('Seguro que desea borrar el usuario ' + id + '?'))
	{
		var param = [];
		param['auscod']=id;
		param['atscod']=atscod;
		param['atsdes']=atsdes;
		
		ejecutasqld("tipodotacionusuarioeliminar.php",param);
		location.reload();
	}
}