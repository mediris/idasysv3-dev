/*
jDavila
20/03/12
*/
function agregar()
{
	document.form.aumcod.value="";
	document.form.aumdes.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "umedidasagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
20/03/12
*/
function editar(tipo){

	var param = [];
	param['aumcod']=tipo;
	ejecutasqlp("umedidasinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.aumcod.value=gdata[i].AUMCOD;
		document.getElementById("wsaumcod").innerHTML=gdata[i].AUMCOD;
		document.form.aumdes.value=gdata[i].AUMDES;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "umedidaseditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
20/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar la unidad de medida ' + tipo + '?'))
	{
		var param = [];
		param['aumcod']=tipo;
		ejecutasqld("umedidaseliminar.php",param);
		location.reload();
	}
}
