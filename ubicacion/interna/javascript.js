/*
jDavila
19/03/12
*/
function agregar()
{
	document.form.aubcod.value="";
	document.form.aubdes.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
19/03/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "ubicacionagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
19/03/12
*/
function editar(iubi, ubi) {

	var param = [];
	param['aiucod']=iubi;
	param['aubcod']=ubi;
	ejecutasqlp("ubicacioninformacionphp.php",param);

	for(i in gdata){
		document.form.aiucod.value=gdata[i].AIUCOD;
		document.form.aubcod.value=gdata[i].AUBCOD;
		document.getElementById("wsaubcod").innerHTML=gdata[i].AUBCOD;
		document.form.aubdes.value=gdata[i].AUBDES;
	};
	$("#aftsav").hide(1);
}

/*
jDavila
19/03/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "ubicacioneditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
19/03/12
*/
function eliminar(iubi, ubi) {
	if (confirm('Seguro que desea borrar indice de ubicacion ' + ubi + '?'))
	{
		var param = [];
		param['aiucod']=iubi;
		param['aubcod']=ubi;
		ejecutasqld("ubicacioneliminar.php",param);
		location.reload();
	}
}

