<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Agregar Solicitud</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar();cargarparametros(document.getElementById('atlcod'),'');">
<div id="agregardiv">
	<form id="agregarform" name="agregarform" method="post" class="form" action="">
      <input name="compania" id="compania" type="hidden" value="<?php echo $Compania;?>" />
       <input name="auncod" id="auncod" type="hidden" value="<?php echo trim($auncod);?>" />
	   <table class="tabla1">
            <tr>
                <td width="29%"  scope="col"><label>Tipo Solicitud</label></td>
                <td width="32%" scope="col">
                    <select name="atlcod" id="atlcod" onchange="cargarparametros(this,'');">
                      <!--<option value= "" >Seleccione...</option>-->
                    <?php //echo $sql="SELECT t.ATRCOD, t.ATRDES, tt.ATTCOD, tt.ATTDES FROM IV12FP t,IV27FP tt WHERE t.ACICOD = '14' AND t.ATTCOD = tt.ATTCOD AND t.ACICOD = tt.ACICOD ORDER BY tt.ATTDES";
                     $sql= "SELECT T1.ATLCOD, T2.ATLDES, T1.AUNCOD
                                  FROM SL06FP T1
                                  INNER JOIN SL01FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATLCOD=T2.ATLCOD)
                                   WHERE T1.ACICOD='$Compania' AND T1.AUNCOD='$auncod' ORDER BY T2.ATLDES";
                            $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                            while(odbc_fetch_row($result1)){
							                 $atlcod=trim(odbc_result($result1,1));
                                $atldes=odbc_result($result1,2);
								?>
                                <option value="<?php echo $atlcod; ?>"><?php echo $atldes; ?></option>
                    <?php } ?>
                  </select>
                </td>
                <td colspan="2" id="erratlcod"  scope="col">&nbsp;</td>
            </tr>
            <!--
            <tr>
                <td  scope="col">Numero Solicitud:</td>
                <td width="32%" scope="col"><div align="left">
	                <input name="asonro" type="text" id="asonro" value="" size="8" maxlength="8"/>
                    </div>
                </td>
                <td colspan="2" id="errasonro"  scope="col">&nbsp;</td>
            </tr>
            -->

            <!-- Ocultar campo descripcion si es solicitud transporte express -->
           <!-- <?php 

            if($atlcod == '01'){
              $visibility = 'visibility:visible';
            } else {
              $visibility = 'display:none';
            }

            ?> -->

            <tr id="lineadesc">
                <td scope="col"><label>Descripci&oacute;n</label></td>
                <td scope="col">

                <div align="left">
                	<textarea name="asoobs"  id="asoobs" cols="50" rows="2" maxlength="1000"/></textarea>
                    </div>

                </td>
                <td colspan="2" id="errasoobs" scope="col">&nbsp;</td>
            </tr>
            <tr>
            	<td colspan="4">
                	<table width="100%"  border="0" >
                      <tr>
                        <td width="100%" colspan="3" scope="col"><h3>Par�metros Adicionales: </h3>
                          <span class="header">
                          </span></td>
                        </tr>
                      	<tr>
                        	<td>
                            <div id="parametros">
                            </div>
                            </td>
                        </tr>
                       
                      </table>
                </td>
            </tr>
            
            <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col"></th>
                <th width="18%" scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Salir" id="cancelar"></th>
                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
            </tr>
        </table>

  </form></div>
  <div>
  <form id="solicitudes" name="solicitudes" class="form">
  <div id="reqst06fp" align="center" style="display:none">

  <legend></legend>
  <table width="100%" class="tabla1">
  	<tr>
  	<td>
  		<table width="90%" height="94" border="0" cellpadding="0" cellspacing="4" align="center">
            <tr>
                <td><strong>Almac�n</strong></td>
                <td>
                <input name="aalcod" type="hidden" id="aalcod" value="<?php echo $almacen;?>"   ReadOnly/>
                <div id="divaaldes"></div>
                </td>
            </tr>                    
            <tr>
                <td><strong>Transacci&oacute;n</strong></td>
                <td><input name="atrcod" id="atrcod" type="hidden"  ReadOnly/>
                <div id="divatrdes1"></div>
                </td>
            </tr>
            <tr>
                <td><strong>N�mero de Transacci&oacute;n</strong></td>
                <td><input name="atrnum" id="atrnum" type="hidden"  ReadOnly/>
                <div id="divatrnum"></div>
                </td>
            </tr>
            <tr>
                <td width="36%"><strong>Descripci&oacute;n</strong></td>
                <td width="64%"><div id="divatrdes"></div></td>
             </tr>
  		</table>
 	</td>
 </tr>
 <tr>
 <td>
  <table  id="grilla" width="100%"  border="0" >
    <thead>
      <tr>
        <th colspan="4" align="center" scope="col"><h3>Detalle de la salida por Transacciones: </h3>
          </th>
        </tr>
      <tr>	
        <th width="27%">Art�culo</th>
        <th width="19%">Cant. entregada</th>
        <th width="20%">Unid/Med </th>
        <th width="13%">Opciones</th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td>
          	<div align="left" id="ocultararticulo">
            <select id="aarcod" name="aarcod"  onchange="choiceart('parent',this,0,event)">
              </select>
            </div></td>
        <td><div ><input type="text" name="aslcan" id="aslcan" size="15" maxlength="15" onkeydown="esenter(event);" onchange="activaimagengrabar(1)"/></div></td>
        <td>
	        <!--<input name="aarumb" type="text" id="aarumb" size="6" maxlength="4"  ReadOnly>-->
			<div id="div_aarnum">
            </div>
          </td>
        
        <td >
          <table width="100%">
            <tr>
              <td>
                <div id="imagengrabar" style="display:none"><a href="javascript:grabardetallereq();"><img src="../images/aprobar.png" width="16" height="16" border="0" /></a> </div>
                </td>
              <td>&nbsp;</td>
              <td><div id="imageneliminar" style="display:none"><a href="javascript:detallereqeliminar();"><img src="../images/rechazar.png" width="16" height="16" /></a> </div></td>
              </tr>
            </table>
          </td>
        </tr>
      </tbody>
  </table>
  
       </td>
       </tr>
                <tr>
                    	
        	
        	<td width="21%"  scope="col"><strong><p id='finalizarboton' onclick="finalizarreq('<?php echo trim($coddpto);?>')" class="subir" align="right">Finalizar</p></strong></td>
        	
   		  </tr>
        
          </table>

    </div>
    </form>  
</div>

</div>
<div align="center" id="agregaraftsav" style="display:none">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>