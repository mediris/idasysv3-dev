<?php session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>

<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({
overlayOpacity: "0.5"
});

</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			       document.getElementById('info').style.display ="";
				  $('#info').dataTable( 
				  {
			        "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ] }, { "bSortable": false, "aTargets": [ 6 ] }] ,
					"aaSorting": [[1,'asc']],
					"bStateSave": true,
					"oLanguage": 
					{
			          "sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
					  "oPaginate": 
					  {
			            "sNext": "Siguiente",
						"sPrevious": "Anterior"
			          }
			        }					
			      } );
			  } );
</script>
</head>
<body>
<div id="wrapper">
  <?php include("../superior.php"); ?>
  <div id="page">
      <?php include("../validar.php"); ?>

<!--<div id="sidebar">
	    <ul>
	      <li>
	        <h2>Estad&iacute;sticas</h2>
	        <p>Aqu&iacute; se reflejar&aacute;n estad&iacute;sticas particulares seg&uacute;n en la opci&oacute;n del sistema que se encuentre</p>
          </li>
	      <li>
	        <h2>Categories</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
          </li>
	      <li>
	        <h2>Blogroll</h2>
	        <ul>
	          <li><a href="#">Aliquam libero</a></li>
	          <li><a href="#">Consectetuer adipiscing elit</a></li>
	          <li><a href="#">Metus aliquam pellentesque</a></li>
	          <li><a href="#">Suspendisse iaculis mauris</a></li>
	          <li><a href="#">Urnanet non molestie semper</a></li>
	          <li><a href="#">Proin gravida orci porttitor</a></li>
            </ul>
	      </li>
      </ul>
      </div>-->		<!-- end #sidebar -->
      <div id="content3">
      <table width="100%"   border="0">
              <tr>
                <td width="84%" scope="col"><h1 align="center" class="title">USUARIOS</h1><hr /></td>
                <td width="16%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="36%" scope="col"><a rel="shadowbox;width=650;height=345" title="Agregar Usuario" href="usuariofagregar.php"><img src="../images/agregar.gif" alt="Agregar" width="25" height="25" border="0" /></a></th>
                      <th width="30%" scope="col">
							<a href="exportaraexcel.php" target="_blank">
                        		<img src="../images/excel.jpg" alt="" width="25" height="25" />
                        	</a>
                      </th>
                      <th width="16%" scope="col"><img src="../images/pdf.jpg" alt="" width="25" height="25" /></th>
                      <th width="18%" scope="col"><img src="../images/impresora.gif" alt="" width="25" height="25" /></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
            </table>
		<div id="container">
			<div id="demo">
            	<table width="100%" id="info" style="display:none">
                  <thead>
                    <tr>
                    	<th>&nbsp;</th>
                        <th>Usuario</th>
                        <th>Apellidos y Nombres </th>
                        <th>Ultima Conexi&oacute;n </th>
                        <th>Supervisor</th>
                        <th>Status</th>
                        <th>opciones</th>
                    </tr>
                  </thead>
                    <tbody >
                              <?php $sql="Select * from mb20fp order by auscod ";
                                $result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                while(odbc_fetch_row($result)){			?>
                    <tr>
                        <td>
                        <?php if (odbc_result($result,21)=='F') { echo '<img src="../images/femenino.gif" width="20" height="20">' ;}
                        else {echo '<img src="../images/masculino.gif" width="20" height="20"> '; }?>
						</td>   
                        <td>
                        	<div>
                                <strong>             
                                    <?php echo odbc_result($result,1);?>
                                </strong>
                            </div>
                        </td>
                        <td ><?php echo trim(odbc_result($result,2))." ".trim(odbc_result($result,3)).", ".trim(odbc_result($result,4))." ".trim(odbc_result($result,5)).".";?></td>
                        <td ><?php echo fecdma(odbc_result($result,18),'amd','.')." - ".odbc_result($result,19);?></td>
                        <td ><?php echo odbc_result($result,13);?></td>
                        <td ><?php echo status('AUSSTS',odbc_result($result,20));?></td>
                        <td >
                        
                            <ul id="opciones">
                                <li><a rel="shadowbox;width=650;height=345" title="Editar Usuario" href="usuariofeditar.php?usuario=<?php echo trim(odbc_result($result,1));?>"><img src="../images/editar.gif" alt="Editar" width="15" height="15" border="0" /></a>
                                </li>
                                <li><a rel="shadowbox;width=650;height=345" title="Autorizar Usuario" href="usuariofautorizar.php?usuario=<?php echo trim(odbc_result($result,1));?>"><img src="../images/autorizaciones.gif" alt="Autorizar" width="15" height="15" border="0" /></a>
                                </li>
                                <li><a rel="shadowbox;width=650;height=345" title="Usuario-Compa�ia" href="usuariocompania.php?usuario=<?php echo trim(odbc_result($result,1));?>"><img src="../images/companiesICO.png" alt="Usuario-Compa�ia" width="18" height="18" border="0" /></a>
                                </li>
                                <li>
                            <a href="javascript:usuarioverhistorial('<?php echo trim(odbc_result($result,1));?>')"><img src="../images/historial.gif" alt="Ver Historial" width="15" height="15" border="0" /></a>
                                </li>
                                <li>
                            <a href="javascript:usuarioeliminar('<?php echo trim(odbc_result($result,1));?>')"><img src="../images/eliminar.gif" alt="Eliminar" width="15" height="15" border="0" /></a>
                                </li>
                              </ul>
                        </td>
                        
                          </tr>
                              <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                    	<th width="10px">&nbsp;</th>
                        <th >Usuario</th>
                        <th >Apellidos y Nombres </th>
                        <th >Ultima Conexi&oacute;n </th>
                        <th >Supervisor</th>
                        <th>Status</th>
                        <th >opciones</th>
                    </tr>
                  </tfoot>
                
                </table>
			</div>
		</div>
	  </div>
		<!-- end #content -->
	<div style="clear: both;">&nbsp;</div>
  </div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php");?></div>
	</div>
	<!-- end #footer -->
</body>
</html>
