<?php 
/*
 *jDavila
 *16/02/2012
 *modificado:22/02/2012
 *@tipo inventario modificado de select a check's
 */
session_start();
include("../../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar status</title>
<link href="../../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../../javascript/jquery.js" type="text/javascript"></script>
<script language="JavaScript" src="../../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar()">

	<div id="agregardiv" class="white_content_tnye">
        <form id="agregarform" name="agregarform" method="post" action="" class="form">

            <table width="60%"  border="0" >
                <tr>
                    <th  scope="col"><label>Tipo de Requerimiento</label></th>
                    <th width="30%" scope="col"><div align="left"><input name="atqcod" type="text" id="atqcod" size="4" maxlength="2" /></div></th>
                    <th colspan="2" id="errarqtip" class="Estilo5" scope="col">&nbsp;</th>
                 </tr>
                <tr>
                    <th scope="col"><label>Descripcion del Requerimiento</label></th>
                    <th scope="col"><div align="left"><input name="atqdes" type="text" id="atqdes" size="40" maxlength="60" onKeyUp="agregarform.atqdes.value=agregarform.atqdes.value.toUpperCase()"/></div></th>
                    <th colspan="2" id="erratqdes" class="Estilo5" scope="col">&nbsp;</th>
                </tr>
                <tr>
                    <th scope="col"><label>Tipo Inventario</label></th>
                    <th scope="col">
                    	<div align="left">
                            <!--
                            <select name="aticod" id="aticod">
                            <?php 
							/*
                                $sql="SELECT ATICOD, ATIDES FROM IV01FP WHERE ACICOD='$Compania' ORDER BY ATICOD";
                                $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                while(odbc_fetch_row($result1)){			
                                    $aticod=trim(odbc_result($result1,1));
                                    $atides=trim(odbc_result($result1,2));
							
                            ?>
                                    <option value= "<?php echo $aticod; ?>" ><?php echo $atides; ?></option>
                            <?php } */ ?>
                            </select>
                            -->
                            <!-- agreagado jDavila 22/02/2012 -->
                            <table border="0">
                            <?php 
                                $sql="SELECT ATICOD, ATIDES FROM IV01FP WHERE ACICOD='$Compania' ORDER BY ATICOD";
                                $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                                while(odbc_fetch_row($result1)){			
                                    $aticod=trim(odbc_result($result1,1));
                                    $atides=trim(odbc_result($result1,2));
									$checked = '';
									if($aticod == $listaticod)
									{
										$checked = "checked='checked'";
									}
                            ?>
                                    <tr>
                                    	<td>
                                        	<label name="Laticod<?php echo "[".$aticod."]"; ?>"><?php echo $atides; ?></label>
                                        </td>
                                        <td>
                                            <input name="aticod<?php echo "[".$aticod."]"; ?>" id="aticod<?php echo "[".$aticod."]"; ?>" type="checkbox" value="<?php echo $aticod; ?>" <?php echo $checked; ?> /><br />
                                        </td>
                                    
                            <?php } ?>
                            </table>
                        </div>
                    </th>
                    <th colspan="2" id="erraticod" class="Estilo5" scope="col">&nbsp;</th>
                </tr> 
                <tr>
                    <th scope="col"><label>Transacción Inventario Entrada</label></th>
                    <th scope="col"><div align="left"><select name="atrent" id="atrent">
						<?php 
							$sql="SELECT atrcod, atrdes FROM iv12fp WHERE ACICOD='$Compania' and ATRSIG = '+' ORDER BY atrcod";
							$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
							while(odbc_fetch_row($result1)){			
								$atrcod=trim(odbc_result($result1,1));
								$atrdes=odbc_result($result1,2);
						?>
								<option value= "<?php echo $atrcod; ?>" ><?php echo $atrdes ?></option>
                        <?php } ?>
                        </select></div>
                    </th>
                    <th>&nbsp;</th>
                </tr>
                <tr>
                    <th scope="col"><label>Transacción Inventario Salida</label></th>
                    <th scope="col"><div align="left"><select name="atrsal" id="atrsal">
						<?php 
							$sql="SELECT atrcod, atrdes FROM iv12fp WHERE ACICOD='$Compania' and ATRSIG = '-' ORDER BY atrcod";
							$result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
							while(odbc_fetch_row($result1)){			
								$atrcod=trim(odbc_result($result1,1));
								$atrdes=odbc_result($result1,2);
						?>
								<option value= "<?php echo $atrcod; ?>" ><?php echo $atrdes ?></option>
                        <?php } ?>
                        </select></div>
                    </th>
                    <th>&nbsp;</th>
                </tr>
                <tr >
                    <th scope="col">&nbsp;</th>
                    <th scope="col"></th>
                    <th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
					<th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
                </tr>
            </table>
        </form>
    </div>

<div align="center" id="agregaraftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>