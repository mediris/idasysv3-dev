<?php session_start();
	
	include_once("../conectar.php");
	require_once('html2pdf/html2pdf.class.php');

	$numped = trim($_GET['num']);
	$tipoped = trim($_GET['tipo']);


	/* Select para consultar los parametros adicionales del pedido */
	$sql = "SELECT T1.ACICOD, T1.ATRCOD, T1.AMDCOD, T1.APDCOD, T2.APDDES, T2.APDSEC, T2.APDLON, T2.APDDCR, T2.APDTIP, T2.APDVAL, T2.ATACOD, T2.ASBCOD, T2.APDSTS, T2.APDLND, T3.ASBDES, T3.ASBSTS, T4.APACOD, T4.ADSNRO, T4.AAPVLA, T4.AAPDES, T4.AAPLON, T4.AAPLND, T4.AAPSEC, T4.AAPTIP, T4.ATSCOD FROM mb10fp T3, MB03FP T2, iv37FP T1 LEFT JOIN IV46FP T4 ON ( T1.ACICOD=T4.ACICOD AND T4.ADSNRO='".$numped."' AND T1.APDCOD=T4.APACOD AND T4.ATSCOD='".$tipoped."') WHERE T1.ACICOD='".$Compania."' AND T1.AMDCOD='".$modulo."' AND T1.ATRCOD=(SELECT T7.ATRSAL FROM is02fp T7 WHERE T7.ACICOD=T1.ACICOD and T7.ATQCOD='".$tipoped."') AND T1.APDCOD=T2.APDCOD AND T1.AMDCOD=T2.AMDCOD AND T2.ATACOD=T3.ATACOD AND T2.ASBCOD=T3.ASBCOD ORDER BY T2.ASBCOD, T2.APDSEC, T1.APDCOD ";

	$result=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
	ob_start();
	$data = [];
	while(odbc_fetch_row($result)){
		array_push($data, odbc_result($result, "AAPVLA"));
	}


	/* Select para consultar la información de los articulos del pedido */
	$sql2 = "SELECT T1.ACICOD, T1.ARDCAN, T2.AARDES, T1.AARCOD, T3.ALTCOD FROM IS05FP T1 INNER JOIN IV05FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.AARCOD = T2.AARCOD) LEFT JOIN IV48FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.AARCOD = T3.AARCOD) WHERE T1.ACICOD='".$Compania."' AND T1.ARQNRO ='".$numped."'";

	$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
	ob_start();
	while(odbc_fetch_row($result2)){
		$cantidad = odbc_result($result2, "ARDCAN");
		$nombreArticulo = odbc_result($result2, "AARDES");
		$codArticulo = odbc_result($result2, "AARCOD");
		$loteArticulo = odbc_result($result2, "ALTCOD");

		if($tipoped == '04'){
			include('etiquetaLaboratorioViales.php');
		}else{
			include('etiquetaLaboratorio.php');
		}
	}
	
	$content = ob_get_clean();
	$html2pdf = new HTML2PDF('landscape', array(75,100), 'es',array(0, 0, 0, 0), 'UTF-8', 0);
	$html2pdf->WriteHTML($content);
	$html2pdf->Output('etiqueta.pdf');

?>