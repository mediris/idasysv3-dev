<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_devoluciones_MEDITRON.xls");

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Reporte de Devoluciones</title>
	</head>
	<style>

		h1, h2, h3, h4, h5{
			margin: 0;
			padding: 0;
			font-weight: normal;
			color: #32639A;
		}

		h1{
			font-size: 2em;
		}

		h2{
			font-size: 2.4em;	
		}

		h3{
			font-size: 1.6em;
			font-style: italic;
		}

		h4{
			font-size: 1.6em;
			font-style: italic;
			color: #FFF;
		}

		h5{
			font-size: 1.0em;
			font-style: italic;
			color: #666;
		}

		#background-image{
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 10px;
			margin: 0px;
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}

		#background-image th{
			padding: 12px;
			font-weight: normal;
			font-size: 12px;
			color: #339;
			border-bottom-style: solid;
			border-left-style: none;
			text-align: center;
		}

		#background-image td{
			color: #669;
			border-top: 1px solid #fff;
			padding-right: 4px;
			padding-left: 4px;
		}

		#background-image tfoot td{
			font-size: 9px;
		}

		#background-image tbody{
			background-repeat: no-repeat;
			background-position: left top;
		}

		#background-image tbody td{
			background-image: url(images/backn.png);
		}

		* html #background-image tbody td{
			/* 
	   		----------------------------
			PUT THIS ON IE6 ONLY STYLE 
			AS THE RULE INVALIDATES
			YOUR STYLESHEET
	   		----------------------------
			*/
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
			background: none;
		}	
	</style>
	<body>
		<?php 

	 		$wsolicitud = 0;
			if($solicitudpagina == 0){

				$sql = "SELECT T5.AAPVLA, T5.APACOD, T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='".$aalcod."' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF, (SELECT T7.AALUBI FROM IV07FP T7 WHERE T1.ACICOD = T7.ACICOD AND T7.AALCOD = '".$aalcod."') AS AALUBI

				FROM IV05FP T1 
				INNER JOIN IV06FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.AARCOD = T3.AARCOD) 
				INNER JOIN IV13FP T4 ON (T1.ACICOD = T4.ACICOD AND T3.AARUMB = T4.AUMCOD) 
				LEFT JOIN IV17FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AARCOD = T5.AARCOD AND T5.APACOD IN ('2101','2112')) 
				
				WHERE T1.ACICOD = '".$Compania."' AND (T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD = T1.ACICOD AND T2.AALCOD = '".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD) AND T1.AARSTS = '01') 
					
				ORDER BY T1.AARCOD";
				
				// echo $sql."<br/><br/>";
				$resultt = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				
				$z = 0;
				// $tothon = 0;
				// $totest = 0;
				$lin = 1;
				$limitep = $_SESSION['solicitudlineasporpaginat'];
				$pag = 1;
				// $primero = 'S';

				while(odbc_fetch_row($resultt)){ 

					$artcod = odbc_result($resultt,'AARCOD');
					$atrdes = odbc_result($resultt,'AARDES');
					$exp = substr($atrdes ,(strripos($atrdes,"(")) );
					$aumdes = odbc_result($resultt,'AUMDES');
					$existencia = odbc_result($resultt,'ASLSAF');
					$ubicacion = odbc_result($resultt,'AALUBI');
					
					/*CARGA DE ENTRADAS*/
					$listapacod = array('0102','0108', '1101', '1127');

					//$listapacod = explode(',', $listapacod);
					$sql2="";
					$sql3="";
					$sql2.="
						SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, 
								T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES, T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, ('".$existencia."') AS ASLSAF, ('".$ubicacion."') AS AALUBI, ";
					foreach($listapacod as $key=>$value){			
						$sql3.="
								(SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD=T3.ACICOD and T6.AARCOD=T3.ATRART AND T6.APACOD IN (".$value.") ) AS N".$value." , ";
					}
					$sql2.= substr($sql3,0, (strripos($sql3,",")));
					$sql2.="
						FROM IV16FP T3 
						INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
						INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+')
						WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."'
						ORDER BY T4.ATRNUM , T3.ATRART
					";
				
					// echo $sql2."<br/><br/>";
					// DIE();
					$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
					while(odbc_fetch_row($resultt2))
					{
						$jml = odbc_num_fields($resultt2);
						$row[$z]["pagina"] =  $pag;
						for($i=1;$i<=$jml;$i++)
						{	
							$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
						}
						$z++;
						if ($lin>=$limitep) 
						{
							$limitep+=$_SESSION['solicitudlineasporpaginat'];
							$pag++;
						}
						$lin++;
					}
				}

				$totsol = ($lin-1);
				$_SESSION['totalsolicitudes'] = $totsol;
				$_SESSION['solicitudarreglo'] = $row;
				$solicitudpagina = 1;
				$_SESSION['solicitudpaginas'] = $pag;
			}//fin de solicitudpagina
			/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
			$paginat = $_SESSION['solicitudarreglo'];
		?>

		<table width="100%" border="0">
			<tr>
				<td height="89">
					<h1>
						<?php if($Compania=='14'){?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
						<?php }else{ ?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
						<?php } ?>
	   				</h1>
	  				<h5>RIF:  <?php echo $Companiarif; ?></h5>
	  			</td>
			</tr>
			<tr>
				<td>
					<table width="100%" id="background-image" >
	  					<thead>
	  						<tr>
	        					<th colspan="14" scope="col">
	        						<h3>Reporte de Devoluciones MEDITRON</h3>
	        					</th>
	    					</tr>
	    					<tr>
	        					<th colspan="14" scope="col"><h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4></th>
	    					</tr>
	  						<tr>
	        					<th colspan="14" scope="col"><h3>Almac&eacute;n: <?php echo alamcen($aalcod, $Compania);?></h3></th>
	    					</tr>
	  						<tr style="border-bottom:solid;">

						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">C&oacute;digo Art.</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Art&iacute;culo</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Tipo inventario</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Ubicaci&oacute;n</th>  
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Nro referencia entrada</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Fecha entrada</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Cantidad entrada</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Observaciones entrada</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">Existencia actual</th>

	    					</tr>
	  					</thead>
	   					<tbody>
							<?php 

							$pagact = $solicitudpagina;
							$part = 1;

							for($g = 0; $g < (count($paginat)); $g++){
								// echo "//**".$paginat[$g]["ATRART"]."<br>";
								if($paginat[$g - 1]["ATRART"] != $paginat[$g]["ATRART"]){

									/*entradas*/
									$sql2 = "SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD, T8.AMCDES, T7.ATIDES, T6.AAPVLA 

									FROM IV16FP T1 
									INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD AND T3.ATRSTS = '02') 
									INNER JOIN IV12FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
									LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM)
									LEFT JOIN IV06FP T9 ON (T1.ACICOD = T9.ACICOD AND T1.ATRART = T9.AARCOD)
									LEFT JOIN IV04FP T8 ON (T1.ACICOD = T8.ACICOD AND T8.AMCCOD = T9.AMCCOD)
									LEFT JOIN IV05FP T4 ON (T1.ACICOD = T4.ACICOD AND T1.ATRART = T4.AARCOD)
									LEFT JOIN IV01FP T7 ON (T1.ACICOD = T7.ACICOD AND T4.ATICOD = T7.ATICOD)
									LEFT JOIN IV38FP T6 ON(T6.ACICOD = T1.ACICOD AND T6.ATRNUM = T3.ATRNUM AND T6.APACOD = '0123')

									WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$aalcod."' AND T1.ATRCOD IN ('0006', '0104') AND T1.ATRART='".$paginat[$g]["ATRART"]."' 

									ORDER BY T3.ATRFEC";

									// echo $sql2."<br/><br/>";
									$result2 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
									$result22 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
									$canEnt = 0;
									//$canEnt = odbc_num_rows($result22);
									while(odbc_fetch_row($result22)){
										$canEnt++;
									}
									//echo "<br>"."*".$canEnt;
										
									$canSal = 0;

									$resultES = '';
									$numRows = '';
									if($canSal > $canEnt){
										$resultES = $result3;
										$numRows = $canSal;
										//echo "<br>"."* salida";
									}else if($canSal <= $canEnt){
										$resultES = $result2;
										$numRows = $canEnt;
										//echo "<br>"."* entrada";
									}
									$rowpos = 0;
									//echo "numRows:".$numRows;
									//echo "rowpos:".$rowpos;				
	                         		?>     

	                         		<?php

								 	if($numRows > $rowpos){
									 	while($numRows > $rowpos){   
										 
										//echo "numRows:".$numRows;
										//echo "rowpos:".$rowpos;
									 		$rowpos++;       
										  	odbc_fetch_row($result2);
										?>

											

			                                	<tr>

		                                        	<!-- Código del artículo -->
			                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
			                                        	<?php echo $paginat[$g]["ATRART"] != '' ? $paginat[$g]["ATRART"] : '--';?>
			                                        </td>
				                                        
			                                        <!-- Artículo -->
			                                        <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
			                                        	<?php echo $paginat[$g]['AARDES'] != '' ? $paginat[$g]['AARDES'] : '--';?>
			                                        </td> 

		                                            <!-- Tipo inventario -->
		                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">                                   
		                                            	<?php echo odbc_result($result2, 'ATIDES') != '' ? odbc_result($result2, 'ATIDES') : '--';?>
		                                            </td>
		                                            
		                                            <!-- Ubicación -->
		                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle">
		                                            	<!-- <?php echo $paginat[$g]["AALUBI"] != '' ? $paginat[$g]["AALUBI"] : '--';?> -->
		                                            	<?php 
	                                                        $ubicacion = list_ubiart($cid, $Compania, $aalcod, $paginat[$g]["ATRART"],2);
	                                                        if(trim($ubicacion)!=''){
	                                                            echo $ubicacion; 
	                                                        }else{ 
	                                                            echo 'Sin Ubicaci&oacute;n';
	                                                        }
	                                                     ?>
		                                            </td>

	                                        <?php  
										     		// $rowpos++;       
												  	// odbc_fetch_row($result2);
												  	// odbc_fetch_row($result3);

		                                            /* <!-- entradas --> */
													if($rowpos <= $canEnt){?>

														<!-- N° referencia entrada -->
		                                            	<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">

		                                            		<?php if(odbc_result($result2, 'ATRCOD') == '0006'){ ?>

		                                            			<?php echo odbc_result($result2, 'ATRNUM') != '' ? odbc_result($result2, 'ATRNUM') : '--'; echo '(E) / '; echo odbc_result($result2, 'ATRDES'); ?>

		                                            		<?php }else{ ?>

		                                            			<?php echo odbc_result($result2, 'ADSNRO') != '' ? odbc_result($result2, 'ADSNRO') : '--'; echo '(E) / '; echo odbc_result($result2, 'ATRDES'); ?>

		                                            		<? } ?>

		                                            	</td>

		                                            	<!-- Fecha entrada -->
		                                            	<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
		                                            		<?php echo odbc_result($result2, 'ATRFEC') != '' ? formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd', 'dd/mm/aaaa' ) : '--';?>
		                                            	</td>

		                                            	<!-- Cantidad entrada -->
		                                            	<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;background-color:rgb(0,153,255)">
		                                            		<?php echo odbc_result($result2, 'ATRCAN') != '' ? number_format(odbc_result($result2, 'ATRCAN'),2,'.',',') : '--';?>
		                                            	</td>

		                                            	<!-- Observacion entrada -->
		                                            	<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
		                                            		<?php echo odbc_result($result2, 'ATROBS') != '' ? odbc_result($result2, 'ATROBS') : '--';?>
		                                            	</td>

		                             				
		                             				<?php }else{ ?>

		                            					<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
		                                                <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
		                                                <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
		                                                <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
		                                                
		                          					<?php } ?>

													<!-- Existencia -->
	                                               	<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;background-color:rgb(0,255,0)" valign="middle">
	                                               		<?php echo $paginat[$g]["ASLSAF"] != '' ? number_format($paginat[$g]["ASLSAF"],2,',','.') : '--';?>
	                                               	</td>

		                                        </tr>
	                       				<?php }
									}
									echo "";
								}
	        				}?>      
	    				</tbody>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
