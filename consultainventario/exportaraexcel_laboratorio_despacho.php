<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_despacho_Laboratorio.xls");

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Reporte despacho Laboratorio</title>
	</head>
	<style>

		h1, h2, h3, h4, h5{
			margin: 0;
			padding: 0;
			font-weight: normal;
			color: #32639A;
		}

		h1{
			font-size: 2em;
		}

		h2{
			font-size: 2.4em;	
		}

		h3{
			font-size: 1.6em;
			font-style: italic;
		}

		h4{
			font-size: 1.6em;
			font-style: italic;
			color: #FFF;
		}

		h5{
			font-size: 1.0em;
			font-style: italic;
			color: #666;
		}

		#background-image{
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 10px;
			margin: 0px;
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}

		#background-image th{
			padding: 12px;
			font-weight: normal;
			font-size: 12px;
			color: #339;
			border-bottom-style: solid;
			border-left-style: none;
			text-align: center;
		}

		#background-image td{
			color: #669;
			border-top: 1px solid #fff;
			padding-right: 4px;
			padding-left: 4px;
		}

		#background-image tfoot td{
			font-size: 9px;
		}

		#background-image tbody{
			background-repeat: no-repeat;
			background-position: left top;
		}

		#background-image tbody td{
			background-image: url(images/backn.png);
		}

		* html #background-image tbody td{
			/* 
	   		----------------------------
			PUT THIS ON IE6 ONLY STYLE 
			AS THE RULE INVALIDATES
			YOUR STYLESHEET
	   		----------------------------
			*/
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
			background: none;
		}	
	</style>
	<body>
		<?php 

	 		$wsolicitud = 0;
			if($solicitudpagina == 0){

				$sql = "SELECT T5.AAPVLA, T5.APACOD, T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='".$aalcod."' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF, (SELECT T7.AALUBI FROM IV07FP T7 WHERE T1.ACICOD = T7.ACICOD AND T7.AALCOD = '".$aalcod."') AS AALUBI

				FROM IV05FP T1 
				INNER JOIN IV06FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.AARCOD = T3.AARCOD) 
				INNER JOIN IV13FP T4 ON (T1.ACICOD = T4.ACICOD AND T3.AARUMB = T4.AUMCOD) 
				LEFT JOIN IV17FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AARCOD = T5.AARCOD AND T5.APACOD IN ('2101','2112')) 
				
				WHERE T1.ACICOD = '".$Compania."' AND (T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD = T1.ACICOD AND T2.AALCOD = '".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD)) 
					
				ORDER BY T1.AARCOD";
				
				// echo $sql."<br/><br/>";
				$resultt = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				
				$z = 0;
				// $tothon = 0;
				// $totest = 0;
				$lin = 1;
				$limitep = $_SESSION['solicitudlineasporpaginat'];
				$pag = 1;
				// $primero = 'S';

				while(odbc_fetch_row($resultt)){ 

					$artcod = odbc_result($resultt,'AARCOD');
					$atrdes = odbc_result($resultt,'AARDES');
					$exp = substr($atrdes ,(strripos($atrdes,"(")) );
					$aumdes = odbc_result($resultt,'AUMDES');
					$existencia = odbc_result($resultt,'ASLSAF');
					$ubicacion = odbc_result($resultt,'AALUBI');
					
					/*CARGA DE ENTRADAS*/
					$listapacod = array('2101','2112');

					//$listapacod = explode(',', $listapacod);
					$sql2="";
					$sql3="";
					$sql2.="
						SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, 
								T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES, T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, ('".$existencia."') AS ASLSAF, ('".$ubicacion."') AS AALUBI, ";
					foreach($listapacod as $key=>$value){			
						$sql3.="
								(SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD=T3.ACICOD and T6.AARCOD=T3.ATRART AND T6.APACOD IN (".$value.") ) AS N".$value." , ";
					}
					$sql2.= substr($sql3,0, (strripos($sql3,",")));
					$sql2.="	
						FROM IV16FP T3 
							INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
							INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+')
						WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."' AND ";

					if($aalcod == '0001'){

							$sql2.= "T4.ATRCOD IN ('0001', '0002') ORDER BY T4.ATRNUM , T3.ATRART";

						}else if($aalcod == '0002'){

							$sql2.= "T4.ATRCOD IN ('0004', '0007', '0009', '0010') ORDER BY T4.ATRNUM , T3.ATRART";

						}else if($aalcod ==  '0003'){

							$sql2.= "T4.ATRCOD IN ('0004', '0005', '0007', '0008', '0010') ORDER BY T4.ATRNUM , T3.ATRART";

						}
						
					// echo $sql2."<br/><br/>";
					// DIE();
					$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
					while(odbc_fetch_row($resultt2))
					{
						$jml = odbc_num_fields($resultt2);
						$row[$z]["pagina"] =  $pag;
						for($i=1;$i<=$jml;$i++)
						{	
							$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
						}
						$z++;
						if ($lin>=$limitep) 
						{
							$limitep+=$_SESSION['solicitudlineasporpaginat'];
							$pag++;
						}
						$lin++;
					}
				}

				$totsol = ($lin-1);
				$_SESSION['totalsolicitudes'] = $totsol;
				$_SESSION['solicitudarreglo'] = $row;
				$solicitudpagina = 1;
				$_SESSION['solicitudpaginas'] = $pag;
			}//fin de solicitudpagina
			/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
			$paginat = $_SESSION['solicitudarreglo'];
		?>

		<table width="100%" border="0">
			<tr>
				<td height="89">
					<h1>
						<?php if($Compania=='14'){?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
						<?php }else{ ?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
						<?php } ?>
	   				</h1>
	  				<h5>RIF:  <?php echo $Companiarif; ?></h5>
	  			</td>
			</tr>
			<tr>
				<td>
					<table width="100%" id="background-image" >
	  					<thead>
	  						<tr>
	        					<th colspan="14" scope="col">
	        						<h3>Reporte de despacho Laboratorio</h3>
	        					</th>
	    					</tr>
	    					<tr>
	        					<th colspan="14" scope="col"><h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4></th>
	    					</tr>
	  						<tr>
	        					<th colspan="14" scope="col"><h3>Almac&eacute;n: <?php echo alamcen($aalcod, $Compania);?></h3></th>
	    					</tr>
	  						<tr>
	                        	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Nro. Producci&oacute;n</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">C&oacute;digo Art.</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Art&iacute;culo</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Cantidad mCi</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Nro. Lote</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Nro. Pedido</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Nro. Factura</th>	
					        	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Nro. referencia salida</th>
					        	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Fecha salida</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Cantidad</th>
						        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Datos paciente</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">M&eacute;dico tratante</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Destino</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Observaciones</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Fecha elaboraci&oacute;n</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Actividad</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Actividad fecha elaboraci&oacute;n</th>
		                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Fecha y hora de calibraci&oacute;n</th>
	                        	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Fecha de caducidad</th>
	                        	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)">Fecha de entrega</th>
	    					</tr>
	  					</thead>
	   					<tbody>
							<?php 

							$pagact = $solicitudpagina;
							$part = 1;

							for($g = 0; $g < (count($paginat)); $g++){
								// echo "//**".$paginat[$g]["ATRART"]."<br>";
								if($paginat[$g - 1]["ATRART"] != $paginat[$g]["ATRART"]){

									/*entrada*/
									$sql2 = "SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD, T1.ATRLOT, T4.AAPVLA

									FROM IV16FP T1 
									INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD AND T3.ATRSTS = '02') 
									INNER JOIN IV12FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
									LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM)
									INNER JOIN IV17FP T4 ON (T4.ACICOD = T1.ACICOD AND T4.AARCOD = T1.ATRART AND T4.APACOD = '2112')

									WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$aalcod."' AND T1.ATRCOD in ('0001', '0003', '0004', '0007', '0008', '0011') AND T1.ATRART='".$paginat[$g]["ATRART"]."' 

									ORDER BY T3.ATRFEC DESC FETCH FIRST 1 ROWS ONLY";

									// echo $sql2."<br/><br/>";
									$result2 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
									$result22 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
									$canEnt = 0;
									//$canEnt = odbc_num_rows($result22);
									while(odbc_fetch_row($result22)){
										$canEnt++;
									}
									//echo "<br>"."*".$canEnt;
										
									/*salidas*/
									$sql3 = "SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD, T6.AISCOD, T6.AISDES, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2113') AS NROFAC, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2116') AS NROPED, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2114') AS FECELA, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2109') AS ACTIVI, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2118') AS ACTFEL, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2110') AS FECCAL, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2111') AS FECCAD, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2115') AS FECENT, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2117') AS NROPRO, (SELECT T10.AAPVLA FROM IV46FP T10 WHERE T10.ACICOD = T1.ACICOD AND T10.ADSNRO = T5.ADSNRO AND T10.APACOD = '2122') AS MEDTRA, T5.ADSSTS

									FROM IV16FP T1 
									INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD AND T3.ATRSTS = '02') 
									INNER JOIN IV12FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
									LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM) 
									LEFT JOIN IV42FP T6 ON (T5.ACICOD = T6.ACICOD AND T5.AISCOD = T6.AISCOD)

									WHERE T1.ACICOD = '$Compania' AND T1.AALCOD = '$aalcod' AND T1.ATRCOD in ('0002', '0005', '0006', '0009', '0010') AND T1.ATRART='".$paginat[$g]["ATRART"]."' 

									ORDER BY T3.ATRFEC";

									
									// echo $sql3."<br/><br/>";
									$result3 = odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
									$result33 = odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));

									//echo "<br>"."+".$canSal = odbc_num_rows($result3);
									$canSal = 0;
									//$canEnt = odbc_num_rows($result22);
									while(odbc_fetch_row($result33)){
										$canSal++;
									}
									//echo "<br>"."*".$canSal;

									$resultES = '';
									$numRows = '';
									if($canSal > $canEnt){
										$resultES = $result3;
										$numRows = $canSal;
										//echo "<br>"."* salida";
									}else if($canSal <= $canEnt){
										$resultES = $result2;
										$numRows = $canEnt;
										//echo "<br>"."* entrada";
									}
									$rowpos = 0;
									//echo "numRows:".$numRows;
									//echo "rowpos:".$rowpos;				
	                         		?>     
	                                        
	                         		<?php
	                                 	//while(odbc_fetch_row($resultES)){   
								 	if($numRows > $rowpos){
									 	while($numRows > $rowpos){   
										 
										//echo "numRows:".$numRows;
										//echo "rowpos:".$rowpos;
								          	$rowpos++;       
										  	odbc_fetch_row($result2);
										  	odbc_fetch_row($result3);
										?>
											<tr>
												
												<!-- Nro. Produccion -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'NROPRO') != '' ? odbc_result($result3, 'NROPRO') : '--';?>
	                                            </td>

												<!-- Código del artículo -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo $paginat[$g]["ATRART"] != '' ? $paginat[$g]["ATRART"] : '--';?> 
	                                            </td>

	                                            <!-- Artículo -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo $paginat[$g]['AARDES'] != '' ? $paginat[$g]['AARDES'] : '--';?>
	                                            </td>

	                                            <!-- Cantidad mCi -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result2, 'AAPVLA') != '' ? odbc_result($result2, 'AAPVLA') : '--';?>
	                                            </td>

	                                            <!-- Nro Lote -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result2, 'ATRLOT') != '' ? odbc_result($result2, 'ATRLOT') : '--';?>
	                                            </td>

	                                            <!-- Nro Pedido -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'NROPED') != '' ? odbc_result($result3, 'NROPED') : '--';?>
	                                            </td>

	                                            <!-- Nro Factura -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'NROFAC') != '' ? odbc_result($result3, 'NROFAC') : '--';?>
	                                            </td>

	                                            <!-- Nro referencia salida -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'ADSNRO') != '' ? odbc_result($result3, 'ADSNRO') : '--'; echo odbc_result($result3, 'ADSSTS') == 11 ? '(A)' : '(S)';?>
	                                            </td>

	                                            <!-- Fecha salida -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'ATRFEC') != '' ? formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd', 'dd/mm/aaaa' ) : '--';?>
	                                            </td>

	                                            <!-- Cantidad -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'ATRCAN') != '' ? number_format(odbc_result($result3, 'ATRCAN'),2,',','.') : '--';?>
	                                            </td>

	                                            <!-- Datos del paciente -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'DESATR') != '' ? odbc_result($result3, 'DESATR') : '--';?>
	                                            </td>

	                                            <!-- Médico tratante -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
                                            		<?php echo odbc_result($result3, 'MEDTRA') != '' ? odbc_result($result3, 'MEDTRA') : '--';?>
	                                            </td>

	                                            <!-- Destino -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'AISDES') != '' ? odbc_result($result3, 'AISDES') : '--';?>
	                                            </td>

	                                            <!-- Observaciones -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'ATROBS') != '' ? odbc_result($result3, 'ATROBS') : '--';?>
	                                            </td>

	                                            <!-- Fecha elaboracion -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'FECELA') != '' ? odbc_result($result3, 'FECELA') : '--';?>
	                                            </td>

	                                            <!-- Actividad -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'ACTIVI') != '' ? odbc_result($result3, 'ACTIVI') : '--';?>
	                                            </td>

	                                            <!-- Actividad fecha elaboracion-->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'ACTFEL') != '' ? odbc_result($result3, 'ACTFEL') : '--';?>
	                                            </td>

	                                            <!-- Fecha y hora de calibracion -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'FECCAL') != '' ? odbc_result($result3, 'FECCAL') : '--';?>
	                                            </td>

	                                            <!-- Fecha caducidad -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'FECCAD') != '' ? odbc_result($result3, 'FECCAD') : '--';?>
	                                            </td>

	                                            <!-- Fecha entrega -->
	                                            <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">
	                                            	<?php echo odbc_result($result3, 'FECENT') != '' ? odbc_result($result3, 'FECENT') : '--';?>
	                                            </td>
	                                        </tr>
	                       				<?php }
									}else{	?> 
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
	                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;">--</td>
									<?php }
									echo "";
								}
	        				}?>      
	    				</tbody>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
