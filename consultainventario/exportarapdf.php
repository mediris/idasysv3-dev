<?php 
session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
/*header("Pragma: ");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
header("Content-disposition: attachment; filename=listadeestudios.xls");*/


$html = '
<page>
<html><head>
<title>Listado de Estudios</title>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;   
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	/*color: #FFF;*/
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}
#background-image
{
	/*font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;*/
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 9px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 12px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}

* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'images/backn.png\',sizingMethod=\'crop\');
	background: none;
}	
#rowcolor  td{ background: #CCCCCC; } 

#opciones {
	margin-top: 0;
	padding: 0;
	text-align: center;
}

#opciones ul, li{
	padding: 0;
	margin: 0;
}
#opciones ul, li{
	list-style-type: none;
}

#opciones li{
	float: left;

}
';

/*aqui es donde se selecciona la imagen de fondo*/
/*si se coloca el fondo de la tabla se debe colocar a forzar la descarga*/
/*$html.='table { 
  background: url(http://$Direccionip/idasysv3/images/logoidacadef2005.png) no-repeat ;
  background-attachment:fixed;
  background-position:center center;
}';*/

$html.='
</style>
</head><body>';
				
					 		
				$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
					//$sql="SELECT T2.Addlof, T2.ALCSED,T2.ADDFEF, T2.ACCTRA,T2.ADDNRF,T2.ADDNRR, T1.ARGCOD, T3.AESDES ,T1.ARGTTE, T2.ADDPNM,T2.ADDPNI,T1.ARGTTM, T2.ACOPTP, T2.APCDES, T2.ACOPTR, T2.ACODDG, T2.ADDNRP FROM id72fl94 t2,id76fp t1, id19fp t3 WHERE T1.ACCCIA= T2.ACCCIA and T1.ACCTRA=T2.ACCTRA and T1.ACCTIP= T2.ACCTIP and T1.ACCNRO= T2.ACCNRO and T1.ACCGRC= T2.ACCGRC and T1.ACCCLI= T2.ACCCLI and T1.ADDNRP=  T2.ADDNRP and T1.ADDTPF= T2.ADDTPF and T1.ADDNRF= T2.ADDNRF and T1.ADDNEF= T2.ADDNEF and T1.ADDLOF= t2.ADDLOF and T1.ACCCIA=  T3.ACOCOD and T1.ARGCOD = T3.AESCOD and t2.addsbs='F' and t1.acccia='14' and t1.addlof='11' and t2.addsts>'01' and t2.addfef= '$fecsel'";
					//$sql="SELECT T2.Addlof, T2.ALCSED,T2.ADDFEF, T2.ACCTRA,T2.ADDNRF,T2.ADDNRR, T1.ARGCOD, T3.AESDES ,T1.ARGTTE, T2.ADDPNM,T2.ADDPNI,T1.ARGTTM, T2.ACOPTP, T2.APCDES, T2.ACOPTR, T2.ACODDG, T2.ADDNRP,T2.ADDHOR FROM id72fl94 t2,id76fp t1, id19fp t3 WHERE T1.ACCCIA= T2.ACCCIA and T1.ACCTRA=T2.ACCTRA and T1.ACCTIP= T2.ACCTIP and T1.ACCNRO= T2.ACCNRO and T1.ACCGRC= T2.ACCGRC and T1.ACCCLI= T2.ACCCLI and T1.ADDNRP=  T2.ADDNRP and T1.ADDTPF= T2.ADDTPF and T1.ADDNRF= T2.ADDNRF and T1.ADDNEF= T2.ADDNEF and T1.ADDLOF= t2.ADDLOF and T1.ACCCIA=  T3.ACOCOD and T1.ARGCOD = T3.AESCOD and t2.addsbs='F' and t1.acccia='14' and t1.addlof='11' and t2.addsts>'01' and t1.argtip='S' and t2.addfef= '$fecsel' order by T2.ADDFEF,T2.ADDHOR,T2.ADDNRF";

					
						$sql="SELECT (case when T7.AAPVLA is null then T3.AARDES else T7.AAPVLA end) as AAPVLA, T1.ACICOD, T1.AALCOD, T1.AARCOD, T3.AARDES, T1.AUMCOD,T4.AUMDES, T1.ALTCOD, T5.AARSTM, T5.AUBCOD, T6.AUBDES,
									   sum(case when T1.aslfef=(SELECT max(t2.aslfef) FROM iv40fp t2 where t2.acicod=T1.acicod and t2.aalcod=T1.aalcod and t2.aarcod=T1.aarcod and t2.altcod=T1.altcod and t2.aslfef<'$desde' ) then T1.aslsaf else 0 end) as salant, 
									   sum(case when T1.aslfef between '$desde' and '$hasta' then t1.ASLENT else 0 end) as ASLENT, 
									   sum(case when T1.aslfef between '$desde' and '$hasta' then t1.ASLSAL else 0 end) as ASLSAL 
								FROM iv40fp t1 
									INNER JOIN IV05FP T3 ON(T1.ACICOD=T3.ACICOD AND T1.AARCOD=T3.AARCOD) 
									INNER JOIN IV13FP T4 ON(T1.ACICOD=T4.ACICOD AND T1.AUMCOD=T4.AUMCOD) 
									INNER JOIN iv39fp T5 ON(T1.ACICOD=T5.ACICOD AND T1.AARCOD=T5.AARCOD AND T1.AALCOD=T5.AALCOD) 
									LEFT JOIN IV09FP T6 ON(T1.ACICOD=T6.ACICOD AND T5.AUBCOD=T6.AUBCOD)
									LEFT JOIN IV17FP T7 ON(T1.ACICOD=T7.ACICOD AND T1.AARCOD=T7.AARCOD AND T7.APACOD='0102')
								WHERE t1.acicod='$Compania' and t1.aalcod='$aalcod' 
								GROUP BY T7.AAPVLA, t1.ACICOD, t1.AALCOD, t1.AARCOD, t3.AARDES, t1.ALTCOD, T4.AUMDES, T1.AUMCOD, T5.AARSTM, T5.AUBCOD,  T6.AUBDES
								ORDER BY T3.AARDES";	
					$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
					
					$z=0;
					$lin=1;
					$limitep=1000000;
					$pag=1;
					$primero='S';
					
					while(odbc_fetch_row($resultt))
					{
						$jml = odbc_num_fields($resultt);
						$row[$z]["pagina"] =  $pag;
						for($i=1;$i<=$jml;$i++)
						{
							$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
						}
						$z++;
						if ($lin>=$limitep) 
						{
							$limitep+=$_SESSION['solicitudlineasporpaginat'];
							$pag++;
						}
						$lin++;
					}
					$totsol=($lin-1);
					$_SESSION['totalsolicitudes']=$totsol;
					$_SESSION['solicitudarreglo']=$row;
					$solicitudpagina=1;
					$_SESSION['solicitudpaginas']=$pag;
				}//fin de solicitudpagina
				/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
									$paginat=$_SESSION['solicitudarreglo'];
$html.='<table id="background-image" width="300px">
<thead>
	<tr>';
	if($Compania=='14'){
		$html.='<td rowspan="2" colspan="4"><img src="http://'.$Direccionip.'/idasysv3/images/logoidacadef20052.png"/><h5>RIF:  J-30229023-6</h5></td>';
	}else{
		$html.='<td rowspan="2" colspan="4"><img src="http://'.$Direccionip.'/idasysv3/images/logomeditronnuevo.png"/><h5>RIF:  J-00081466-0</h5></td>';
	}
$html.='
	<td colspan="5"><h4>Consulta de Existencia</h4></td>
	</tr>
	<tr>
	<td colspan="9"><h5>Desde: '.$desde.' / Hastas: '.$hasta.'<br/>Almacén: '.utf8_encode(alamcen($aalcod,$Compania)).'';
	if($stm==1)
		$html.='<br />Solo Stock en M&iacute;nimo';
$html.='</h5></td>
	</tr>

  	<tr>
    	<th scope="col">Art&iacute;culo</th>
		<th scope="col">Cant. M&iacute;nima</th>
		<th scope="col">Ubicaci&oacute;n</th>
		<th scope="col">Unidad de<br /> Medida</th>
		<th scope="col">Saldo<br /> Anterior</th>
		<th scope="col">Entrada</th>
		<th scope="col">Salida</th>
		<th scope="col">Reservada</th>
		<th scope="col">Saldo Final</th>
  	</tr></thead>';

									//print_r($paginat);
									$show = false;
									$pagact=$solicitudpagina;
									$t=0;
									for($g=0; $g < (count($paginat)); $g++)
									{
										
										$salFinal = $paginat[$g]["SALANT"]+$paginat[$g]["ASLENT"]+$paginat[$g]["ASLSAL"];
										
										/*validar si piden solo stm */
										  if($stm==1){//si 
											  if($salFinal<=$paginat[$g]["AARSTM"])	{$show = true;}
											  else									{$show = false;} 
										  }else if($stm==2){
											 $show = true; 
										  }
										  
										  if($show){
											$t++;  
											if($t%2)
												$bg = 'bgcolor="#CCCCCC"';	
											else
												$bg = '';
												$descripcion = $paginat[$g]["AAPVLA"]." <strong>(".$paginat[$g]["AARCOD"].")</strong> ";
												if(strlen($descripcion) <100){
													for($i=strlen($descripcion); $i<=139; $i++)
													{
														$descripcion=$descripcion.' ';
													}
												}
											//<td >'.$descripcion.'</td>
											$html.='<tr '.$bg.' >
												<td >'.wordwrap($descripcion,100,"<br />", true).'</td>
												<td align="right"><strong>'.number_format($paginat[$g]["AARSTM"],2,",",".").'</strong></td>
												<td scope="col" id="opciones">';
												$list ='';
												$list = list_ubiart($cid, $Compania, $paginat[$g]["AALCOD"], $paginat[$g]["AARCOD"],2);
												if(trim($list)!=''){
													$html.=$list;}
												else { 
													$html.=' - Sin Ubicación';
												}
												$html.='</td>
												<td>'.$paginat[$g]["AUMDES"].'</td>
												<td align="right">'.number_format($paginat[$g]["SALANT"],2,",",".").'</td>
												<td align="right">'.number_format($paginat[$g]["ASLENT"],2,",",".").'</td>
												<td align="right">'.number_format($paginat[$g]["ASLSAL"],2,",",".").'</td>
												<td align="right">'.number_format($paginat[$g]["ASLCTR"],2,",",".").'</td>
												<td align="right">'.number_format($salFinal,2,",",".").'</td>
											</tr>';
										  }
									}			
$html.='<tfoot> 
			<tr>
              <td colspan="9" align="right"> </td>
            </tr> 
 			<tr>
              <td colspan="9" align="right"> pag [[page_cu]]/[[page_nb]]</td>
            </tr> 
        </tfoot>';
 $html.='</table>
</body>
</html>
</page>';



	//echo $html;
    require_once(dirname(__FILE__).'/html2pdf/html2pdf.class.php');

	$html2pdf = new HTML2PDF('L','letter','es');
	//$html2pdf = new HTML2PDF('landscape','A4','es',array(0, 0, 0, 0));//colocando tamño de hoja(A$), y margen
	//$html2pdf = new HTML2PDF('landscape',array(200,360),'es',array(0, 0, 0, 0));//colocando tamño de hoja(200,360), y margen
	$html2pdf->WriteHTML($html);
    //$html2pdf->Output('Listado de Estudios.pdf', 'D');//para forzar la descarga
	$html2pdf->Output('Listado de Estudios.pdf');
	auditoriagrabar($modulo,"*PDF","$fecsel","Se ha generado reporte en pdf Listado de Estudios");
?>