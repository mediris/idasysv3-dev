<?php
/*
 * Luis Ramos (lRamos)
 * 10/11/2016 
 */ 
session_start();
include("../conectar.php");
include("../PHPExcel/PHPExcel.php");
$arqnro = trim($_GET["num"]);
$tinva = unserialize( urldecode( $_GET['tinvan'] ) ) ;
$aalcod = $_GET['aalcod'];
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-disposition: attachment; filename=Reporte_Costos_MEDIX_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xlsx");
header('Cache-Control: max-age=0');
?>
<?php 

					$sql="SELECT T1.ACICOD, T1.ATRART, T1.ATRNUM, T1.ATRCOD, T1.ATRCUT, (SUM(T1.ATRCAN)) AS ATRCAN, T2.ATRDES, T3.ATRFEC, T3.ATROBS, T5.AARCOD, T5.AARDES, T7.AMCDES, T8.ATIDES,
					( SELECT T10.AAPVLA FROM IDASYSW.IV38FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATRNUM=T1.ATRNUM AND T10.ATRCOD='0103' AND T10.APACOD='0123' ) AS N0123,
					( SELECT T10.AAPVLA FROM IDASYSW.IV38FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATRNUM=T1.ATRNUM AND T10.ATRCOD='0103' AND T10.APACOD='0124' ) AS N0124,
					( SELECT T10.AAPVLA FROM IDASYSW.IV38FP T10 WHERE T10.ACICOD=T1.ACICOD and T10.ATRNUM=T1.ATRNUM AND T10.ATRCOD='0103' AND T10.APACOD='0125' ) AS N0125
					FROM IV16FP T1
						INNER JOIN IV12FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATRCOD=T2.ATRCOD)
						INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T3.ATRSTS='02')
						LEFT JOIN IV35FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.AALCOD=T4.AALCOD AND T1.ATRCOD=T4.ATRCOD AND T1.ATRNUM=T4.ATRNUM)
						INNER JOIN IV05FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.ATRART=T5.AARCOD)
						INNER JOIN IV06FP T6 ON (T1.ACICOD=T6.ACICOD AND T5.AARCOD=T6.AARCOD)
						INNER JOIN IV04FP T7 ON (T1.ACICOD=T7.ACICOD AND T6.AMCCOD=T7.AMCCOD)
						INNER JOIN IV01FP T8 ON (T5.ACICOD=T8.ACICOD AND T5.ATICOD=T8.ATICOD)
					WHERE T1.ACICOD='$Compania' AND T1.AALCOD IN ('$aalcod') AND T1.ATRCOD='0103' AND T3.ATRFEC BETWEEN '$desde' AND '$hasta'";
					if(!empty($tipoInve)){
						$sql.="  AND T5.ATICOD IN (".$tipoInve.")";
					}
					$sql.="  
						GROUP BY T1.ACICOD, T1.ATRART, T1.ATRNUM, T1.ATRCOD, T1.ATRCUT, T2.ATRDES, T3.ATRFEC, T3.ATROBS, T4.ADSNRO, T4.ATSCOD, T5.AARCOD, T5.AARDES, T7.AMCDES, T8.ATIDES
						ORDER BY T5.AARDES, T1.ATRNUM, T1.ATRART";

					//echo $sql.'<br>';
					$result = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));
					$resultrow = odbc_exec($cid, $sql)or die(exit("Error en odbc_exec 11111"));

					$excel = new PHPExcel();

						//Propiedades del Excel
						$excel->getProperties()
						   ->setCreator('IDASYS WEB')
						   ->setTitle('Reporte MEDIX / Entrada con Costos')
						   ->setLastModifiedBy('IDASYS WEB')
						   ->setDescription('A demo to show how to use PHPExcel to manipulate an Excel file')
						   ->setSubject('Reporte de Entradas con Costos')
						   ->setKeywords('excel php office phpexcel')
						   ->setCategory('inventario')
						   ;

						$exceldata = $excel->getSheet(0);
						$exceldata->setTitle('Reporte_Costos__MEDIX');

						//Titulo
						$styleArray = array(
							    'font'  => array(
							        'bold'  => true
							    ),
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => 'BDBDBD')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Head Logo
					    $objDrawing = new PHPExcel_Worksheet_Drawing();
				        $objDrawing->setName('Logo');
				        $objDrawing->setDescription('Logo');
				        if($Compania == '14'){ 
					        $objDrawing->setPath('../images/logoidacadef2005.png');
					        $objDrawing->setHeight(70);
					        $objDrawing->setWidth(200);
					    } else {
					    	$objDrawing->setPath('../images/MEDITRON_logo_rif.png');
					        $objDrawing->setHeight(70);
				        	$objDrawing->setWidth(330);
					    }

				        $objDrawing->setCoordinates('A1');
				        $objDrawing->setWorksheet($excel->getActiveSheet());
				        //

						$excel->getActiveSheet()->getCell('A4')->setValue('RIF: '.$Companiarif);

						//Titulo 
						$excel->getActiveSheet()->getCell('A7')->setValue('REPORTE IDACA');
						$exceldata->mergeCells('A7:K7');
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A7')->getFont()->setSize(25);
						$excel->getActiveSheet()->getStyle('A7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						//

						//Tipo de Inventario	
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						$sql2="SELECT T6.ATIDES
						FROM IV01FP T6 
						WHERE T6.ACICOD='$Compania'  AND T6.ATICOD in (".$tipoInve.") ";
						//echo $sql2;	
						$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
						$tInventario = ''; 
						while(odbc_fetch_row($result2))
						{
							$tInventario .= trim(odbc_result($result2,'ATIDES'));
						}

						if(empty($tInventario)){$tInventario='N/A';}

						//Sub titulo 
						$excel->getActiveSheet()->getCell('A8')->setValue('Inventario con Costos de Entrada');
						$exceldata->mergeCells('A8:K8');
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A8')->getFont()->setSize(20);
						$excel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A9')->setValue('Elaborado el '.$Fechaactual.' a las '.$Horaactual2);
						$exceldata->mergeCells('A9:K9');
						$excel->getActiveSheet()->getStyle('A9')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A10')->setValue('Almacén: '.alamcen($aalcod, $Compania).'('.$aalcod.')');
						$exceldata->mergeCells('A10:K10');
						$excel->getActiveSheet()->getStyle('A4')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$excel->getActiveSheet()->getCell('A11')->setValue('Filtrado por: '.$tInventario);
						$exceldata->mergeCells('A11:K11');
						$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(true);
						$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

						$exceldata->setCellValue('A12', 'Código');
						$exceldata->setCellValue('B12', 'Artículo');
						$exceldata->setCellValue('C12', 'Marca');
						$exceldata->setCellValue('D12', 'Tipo Inventario');
						$exceldata->setCellValue('E12', 'Ubicación');
						$exceldata->setCellValue('F12', 'Número Referencia Entrada');
						$exceldata->mergeCells('A12:A13');
						$exceldata->mergeCells('B12:B13');
						$exceldata->mergeCells('C12:C13');
						$exceldata->mergeCells('D12:D13');
						$exceldata->mergeCells('E12:E13');
						$exceldata->mergeCells('F12:F13');
						$excel->getActiveSheet()->getStyle('A12:N12')->applyFromArray($styleArray);

						//Entradas
						$exceldata->setCellValue('G12', 'Entradas'); // Sets cell 'a1' to value 'ID
						$exceldata->mergeCells('G12:J12'); 
					    $exceldata->setCellValue('G13', 'Fecha');
					    $exceldata->setCellValue('H13', 'Cantidad');
					    //$exceldata->setCellValue('I13', 'Observaciones');
						$exceldata->setCellValue('I13', 'Descripcion');
						$exceldata->setCellValue('J13', 'Orden de Compra');
						$exceldata->setCellValue('K13', 'Fecha Orden de Compra');
						$exceldata->setCellValue('L13', 'Descripción Proveedor');
					    $exceldata->setCellValue('M13', 'Costo Unitario $');
					    $exceldata->setCellValue('N13', 'Costo Total $');
					    $excel->getActiveSheet()->getStyle('A13:N13')->applyFromArray($styleArray);
					    				   		
					    //Genera Data
					    $pos = 14;
					    $startline = 14;
					    $count = 1;
					    $rowCount = 0;
					   	while(odbc_fetch_row($resultrow)){
					   		$rowCount++;
					    }

					    while(odbc_fetch_row($result))
						{
							//Codigo
							$exceldata->setCellValue('A' . $pos, trim(odbc_result($result, 'AARCOD'))); 

							//Articulo
							$exceldata->setCellValue('B' . $pos, utf8_encode(trim(odbc_result($result, 'AARDES'))));

							//Marca
							$cellc = odbc_result($result, 'AMCDES')!=''?odbc_result($result, 'AMCDES'):'N/A';
							$exceldata->setCellValue('C' . $pos, trim($cellc)); 

							//Tipo Inventario
							$exceldata->setCellValue('D' . $pos, utf8_encode(trim(odbc_result($result, 'ATIDES'))));

							//Ubicacion 
	                        $list = list_ubiart($cid, $Compania, '$tipoAlm', odbc_result($result, 'AARCOD'),3);
	                                            
	                        if(trim($list)!=''){
	                            $exceldata->setCellValue('E' . $pos, trim($list)); 
	                        } else { 
	                            $exceldata->setCellValue('E' . $pos, ' - Sin Ubicación'); 
	                        }

	                        //Merge Celdas a la cantidad de NumRows
	                        if($prevcod==odbc_result($result, 'AARCOD') || is_null($prevcod)){
	                        	$mergepos = $pos;
	                        } else {
	                        	$exceldata->mergeCells('A'.$startline.':A'.$mergepos);
						    	$exceldata->mergeCells('B'.$startline.':B'.$mergepos);
						    	$exceldata->mergeCells('C'.$startline.':C'.$mergepos);
						    	$exceldata->mergeCells('D'.$startline.':D'.$mergepos);
						    	$exceldata->mergeCells('E'.$startline.':E'.$mergepos);
						    	$startline = $mergepos+1;
						    	$mergepos++;
	                        }

	                        //IF Especifico para hacer merge en caso de que en la ultima linea del result sea $prevcod==odbc_result($result, 'AARCOD')
	                        if($count >= $rowCount){
	                        	$exceldata->mergeCells('A'.$startline.':A'.$mergepos);
						    	$exceldata->mergeCells('B'.$startline.':B'.$mergepos);
						    	$exceldata->mergeCells('C'.$startline.':C'.$mergepos);
						    	$exceldata->mergeCells('D'.$startline.':D'.$mergepos);
						    	$exceldata->mergeCells('E'.$startline.':E'.$mergepos);
						    	$startline = $mergepos+1;
						    	$mergepos++;
	                        }
			
							//Numero Referencia Entrada
							if(odbc_result($result, 'ATRCOD')=='0210') { 
								$cellf = odbc_result($result, 'ADSNRO')!=''?add_ceros(odbc_result($result, 'ADSNRO'),6):' ';
								$exceldata->setCellValue('F' . $pos, $cellf.'(D)');
							}else{
	                           $cellf = odbc_result($result, 'ATRNUM')!=''?add_ceros(odbc_result($result, 'ATRNUM'),6):'N/A';
	                           $exceldata->setCellValue('F' . $pos, $cellf.'(E)');
							}	

							//Fecha
							$cellg = odbc_result($result, 'ATRFEC')!=''?formatDate(odbc_result($result, 'ATRFEC'),'aaaa-mm-dd','dd/mm/aaaa' ):'--/--/----';
							$exceldata->setCellValue('G' . $pos, $cellg);

							//Cantidad
							$cellh = odbc_result($result, 'ATRCAN')!=''?odbc_result($result, 'ATRCAN'):' ';
							$exceldata->setCellValue('H' . $pos, $cellh);

							/*
							//Observaciones
							$celli = odbc_result($result, 'ATROBS')!=''?odbc_result($result, 'ATROBS'):' ';
							$exceldata->setCellValue('I' . $pos, utf8_encode($celli));
							*/
							//DESCRIPCION T3ARTDES
							$celli = odbc_result($result, 'ATRDES')!=''?odbc_result($result, 'ATRDES'):' ';
							$exceldata->setCellValue('I' . $pos, utf8_encode($celli));

							//Orden de Compra
							$cellj = odbc_result($result, 'N0123')!=''?odbc_result($result, 'N0123'):' ';
							$exceldata->setCellValue('J' . $pos, utf8_encode($cellj));

							//Fecha Orden de Compra
							$cellk = odbc_result($result, 'N0124')!=''?odbc_result($result, 'N0124'):' ';
							$exceldata->setCellValue('K' . $pos, utf8_encode($cellk));

							//Descripcion Proveedor
							$celll = odbc_result($result, 'N0125')!=''?odbc_result($result, 'N0125'):' ';
							$exceldata->setCellValue('L' . $pos, utf8_encode($celll));

							//Costo Unitario $
							$cellm = odbc_result($result, 'ATRCUT')!=''?number_format(odbc_result($result, 'ATRCUT'),2,'.',''):' ';
							$exceldata->setCellValue('M' . $pos, $cellm);

							//Costo Total $
							$celln = odbc_result($result, 'ATRCUT')!=''?number_format((odbc_result($result, 'ATRCUT')*odbc_result($result, 'ATRCAN')),2,'.',''):' ';
							$exceldata->setCellValue('N' . $pos, $celln);	

							$prevcod = odbc_result($result, 'AARCOD');
               				$pos++;
               				$count++;
						}//fin while                        

						//Obtiene la cantidad de rows del excel		
						$rowCount = $excel->getActiveSheet()->getHighestRow();

						//Calculo de cuantas columnas tiene la hoja
						$highestColumn = $excel->getActiveSheet()->getHighestColumn();

						/*METODO PARA COLOCAR ESTILOS Y MEJORA DE CARGA DEL SERVIDOR */
						//Arreglo de estilos para el bordeado de celdas
						$borderStyle = array(
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				'wrap' => true
	            				),
	            				'borders' => array(
					            'allborders' => array(
					                'style' => PHPExcel_Style_Border::BORDER_THIN,
					            	)
					        	)
						    );

						//Arreglo para colores y alineamiento de celdas 
						$colorStyle = array(
							    'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => array('rgb' => '3399FF')
					            ),
					            'alignment' => array(
	            				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	            				'wrap' => true
	            				)
						    );


						$excel->getActiveSheet()->getStyle('A14:'.$highestColumn.$rowCount)->applyFromArray($borderStyle);


						/*Este for modifica la anchura (width) de cada columna segun su tamaño aproximado manualmente con un switch case. Tambien se modifica los colores (para cantidades) y alineamiento */
						for($col = 'A'; $col <= $highestColumn; $col++){
							switch ($col) {
								case 'A':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
									$excel->getActiveSheet()->getStyle('A14:A'.$rowCount)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
									$excel->getActiveSheet()->getStyle('A14:A'.$rowCount)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
									break;

								case 'B':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(30);
									$excel->getActiveSheet()->getStyle('B14:B'.$rowCount)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
									$excel->getActiveSheet()->getStyle('B14:B'.$rowCount)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
									break;
								
								case 'C':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(10);
									$excel->getActiveSheet()->getStyle('C14:C'.$rowCount)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
									$excel->getActiveSheet()->getStyle('C14:C'.$rowCount)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
									break;

								case 'E':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(10);
									$excel->getActiveSheet()->getStyle('E14:E'.$rowCount)->applyFromArray(array('alignment' => array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER)));
									$excel->getActiveSheet()->getStyle('E14:E'.$rowCount)->applyFromArray(array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT)));
									break;
								
								case 'G':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(13);
									break;

								case 'H':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(10);
									$excel->getActiveSheet()->getStyle('H14:H'.$rowCount)->applyFromArray($colorStyle);
									break;

								case 'I':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(40);
									break;

								case 'N':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(30);
									$colorStyle['fill']['color']['rgb'] = 'FFFF99'; //Se modifica el color
									$excel->getActiveSheet()->getStyle('N14:N'.$rowCount)->applyFromArray($colorStyle);
									break;

								case 'O':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(30);
									break;

								case 'P':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(35);
									break;

								case 'Q':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(35);
									break;

								case 'R':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(30);
									break;	
								
								case 'S':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(35);
									break;

								case 'U':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(30);
									break;

								case 'V':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(25);
									$colorStyle['fill']['color']['rgb'] = '00CDBC'; //Se modifica el color
									$excel->getActiveSheet()->getStyle('V14:V'.$rowCount)->applyFromArray($colorStyle);
									break;		

								case 'W':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(18);
									$colorStyle['fill']['color']['rgb'] = '00FF00'; //Se modifica el color
									$excel->getActiveSheet()->getStyle('W14:W'.$rowCount)->applyFromArray($colorStyle);
									break;	

								case 'X':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(18);
									$colorStyle['fill']['color']['rgb'] = 'FFAE00'; //Se modifica el color
									$excel->getActiveSheet()->getStyle('X14:X'.$rowCount)->applyFromArray($colorStyle);
									break;	

								case 'Y':
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(18);
									$colorStyle['fill']['color']['rgb'] = '0066FF'; //Se modifica el color
									$excel->getActiveSheet()->getStyle('Y14:Y'.$rowCount)->applyFromArray($colorStyle);
									break;	

								default:
									$excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(false);
									$excel->getActiveSheet()->getColumnDimension($col)->setWidth(20);
									break;
							}
						}

						//Ocultar Columnas
						//$excel->getActiveSheet()->getColumnDimension('D')->setVisible(false);
						//$excel->getActiveSheet()->getColumnDimension('K')->setVisible(false);
						//$excel->getActiveSheet()->getColumnDimension('S')->setVisible(false);

						//
						//Se genera el excel
				        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
						$writer->save('php://output');
						exit();
						?>