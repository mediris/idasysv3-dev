<?php 
session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");

header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_entradas_INVAP_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').".xls");


?>
 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Reporte IDASYS</title>
</head>
<style>

h1, h2, h3, h4, h5 {
	margin: 0;
	padding: 0;
	font-weight: normal;
	color: #32639A;
}

h1 {
	font-size: 2em;
}

h2 {
	font-size: 2.4em;	
}

h3 {
	font-size: 1.6em;
	font-style: italic;
}
h4 {
	font-size: 1.6em;
	font-style: italic;
	color: #FFF;
}
h5 {
	font-size: 1.0em;
	font-style: italic;
	color: #666;
}

#background-image
{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 10px;
	margin: 0px;
	width: 100%;
	text-align: left;
	border-collapse: collapse;
}
#background-image th
{
	padding: 12px;
	font-weight: normal;
	font-size: 12px;
	color: #339;
	border-bottom-style: solid;
	border-left-style: none;
	text-align: center;
}
#background-image td
{
	color: #669;
	border-top: 1px solid #fff;
	padding-right: 4px;
	padding-left: 4px;
}
#background-image tfoot td
{
	font-size: 9px;
}
#background-image tbody 
{

	background-repeat: no-repeat;
	background-position: left top;
}
#background-image tbody td
{
	background-image: url(images/backn.png);
}
* html #background-image tbody td
{
	/* 
	   ----------------------------
		PUT THIS ON IE6 ONLY STYLE 
		AS THE RULE INVALIDATES
		YOUR STYLESHEET
	   ----------------------------
	*/
	filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
	background: none;
}	
</style>
<body>
<?php 
				
				
		 		$wsolicitud=0;
				if ($solicitudpagina==0) 	
				{
						
						$z=0;
						$tothon=0;
						$totest=0;
						$lin=1;
						$limitep=$_SESSION['solicitudlineasporpaginat'];
						$pag=1;
						$primero='S';
						
						$sql="SELECT (SELECT COUNT(T7.ATRNUM) FROM IV16FP T7 WHERE T7.ACICOD=T1.ACICOD AND T7.AALCOD=T1.AALCOD AND T7.ATRCOD=T1.ATRCOD AND T7.ATRNUM=T3.ATRNUM) AS CANTI,
									T3.ATRNUM, T3.ATRCOD, T4.AARCOD, T4.AARDES, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T1.ATRCUT, T6.AMCCOD,  T6.AMCDES,
									(SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ATRNUM=T3.ATRNUM and T6.ATRCOD=T3.ATRCOD AND T6.APACOD IN ('1506') ) AS NROGIA,
									(SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T4.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF
								FROM IV16FP T1 
									INNER JOIN IV05FP T4 ON (T1.ACICOD=T4.ACICOD AND T1.ATRART=T4.AARCOD)
									INNER JOIN IV06FP T5 ON (T1.ACICOD=T5.ACICOD AND T4.AARCOD=T5.AARCOD )
									INNER JOIN IV04FP T6 ON (T1.ACICOD=T6.ACICOD AND T5.AMCCOD=T6.AMCCOD )
									INNER JOIN IV15FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.ATRCOD=T3.ATRCOD AND T1.ATRNUM=T3.ATRNUM AND T3.ATRSTS='02' ) 
									INNER JOIN iv12fp T2 ON (T1.ACICOD=T2.ACICOD AND T1.ATRCOD=T2.ATRCOD)
							WHERE T1.ACICOD='".$Compania."' AND T1.AALCOD='0001' AND T1.ATRCOD in ( '0001') 
							ORDER BY T3.ATRNUM";
						
						//echo $sql."<br/><br/>";
						$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
							while(odbc_fetch_row($resultt))
							{
								$jml = odbc_num_fields($resultt);
								$row[$z]["pagina"] =  $pag;
								for($i=1;$i<=$jml;$i++)
								{	
									$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
								}
								$z++;
								if ($lin>=$limitep) 
								{
									$limitep+=$_SESSION['solicitudlineasporpaginat'];
									$pag++;
								}
								$lin++;
							}

						$totsol=($lin-1);
						$_SESSION['totalsolicitudes']=$totsol;
						$_SESSION['solicitudarreglo']=$row;
						$solicitudpagina=1;
						$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
			?>      
<table width="100%" border="0">
    <tr>
        <td height="89"><h1>
			<?php if($Compania=='14'){?>
                <img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
            <?php }else if($Compania=='40'){?>
                <img src="http://<?php echo $Direccionip; ?>/idasysv3/images/MEDITRON_logo_rif.png" width="300" />
            <?php }else{ ?>
                <img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
            <?php } ?>
               </h1>
          	<h5>RIF:  <?php echo $Companiarif; ?></h5>
        </td>
    </tr>

<table width="100%" id="background-image" >
	
  <thead>
  	<tr>
        <th colspan="11" scope="col"><h2>Reporte de Entrada Detallado INVAP</h2></th>
    </tr>
    <tr>
        <th colspan="11" scope="col">Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></th>
    </tr>
  	<tr>
        <th colspan="11" scope="col">Almacén Principal: INVAP</th>
    </tr>

    				<tr style="border-bottom:solid;">
                    	<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Número Referencia Entrada</strong></th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Código</th>
                   		<th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Artículo</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Ubicación</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)">Marca</th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Número Guía</strong></th>

                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;solid;background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;background-color:rgb(204,204,204)"><strong>Descripción</strong></th>
                        <th scope="col" style="border-width:thin;border-top:solid;border-bottom:solid;border-left:solid;border-right:solid;background-color:rgb(204,204,204)"><strong>Observaciones</strong></th>
                    </tr>
                  </thead>
    				<tbody>
							<?php
								//print_r($paginat);
								$pagact=$solicitudpagina;
								$part= 1;
								for($g=0; $g < (count($paginat)); $g++)
								{
									
									$cod_pre = $paginat[$g]["ATRNUM"];
									$cod_ant = $paginat[$g-1]["ATRNUM"];
									echo "<tr>"	;
									if($cod_pre != $cod_ant){
										
										$numRows = $paginat[$g]["CANTI"];
										$TRANCA = TRUE;
										
										
									
							?>      
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>">&nbsp;<?php echo $paginat[$g]["ATRNUM"]!=''?add_ceros($paginat[$g]["ATRNUM"],6):'&nbsp;'; echo '(E)';?></td>
                          	<?php 
									}
							?>
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" ><div>&nbsp;<?php echo $paginat[$g]["AARCOD"]; ?></div></td>
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" ><div><?php echo $paginat[$g]["AARDES"]; ?></div></td>
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" >
                                        <div>
                                        <?php 
											$list = list_ubiart($cid, $Compania, '0001', $paginat[$g]["AARCOD"],2);
											if(trim($list)!=''){echo $list; } 
											else { echo ' - Sin Ubicación';}
                                        ?>
                                        </div>
                                    </td>
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:left;vertical-align:middle;" valign="middle" ><div><?php echo $paginat[$g]["AMCDES"]; ?></div></td>
                                    <?php
										if($cod_pre != $cod_ant){
                                    		if($paginat[$g]["ATRCOD"]=='0003'){
                                    ?>
                                    			<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>">&nbsp;<?php echo $paginat[$g]["NROGIA"]!=''?$paginat[$g]["NROGIA"]:'N/A';?></td>
                                    <?php
                                    		}else{
                                    ?>
                                    			<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>">&nbsp;<?php echo $paginat[$g]["NROGIA"]!=''?$paginat[$g]["NROGIA"]:'---&nbsp;';?></td>
                                    <?php
                                    		}
										}
                                    ?>
                                    
									<?php 
										if($cod_pre != $cod_ant){
									?>
                                    		<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><?php echo $paginat[$g]["ATRFEC"]!=''?formatDate($paginat[$g]["ATRFEC"],'aaaa-mm-dd','dd/mm/aaaa' ):'&nbsp;';?></td>
                                    <?php
										}
									?>
                                    <td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle"><?php echo $paginat[$g]["ATRCAN"]!=''?number_format($paginat[$g]["ATRCAN"],2,',',''):'&nbsp;';?></td>
                                    <?php 
										if($cod_pre != $cod_ant){
									?>
                                 			<td style="border-width:thin;border-bottom:solid;border-left:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>">&nbsp;<?php echo $paginat[$g]["DESATR"]!=''?$paginat[$g]["DESATR"]:'&nbsp;';?></td>
                                    
                                    		<td style="border-width:thin;border-bottom:solid;border-left:solid;border-right:solid;text-align:center;vertical-align:middle;" valign="middle" rowspan="<?php echo $numRows; ?>"><?php echo $paginat[$g]["ATROBS"]!=''?$paginat[$g]["ATROBS"]:'&nbsp;';?></td>
									<?php
										}
									?>
							<?php	
									if($TRANCA)
									{
										echo "</tr>"	;
										//$numRows = $paginat[$g]["CANTI"];
										
									}									
							}
							?>                                            
                  </tbody>

</table>
</td>
</tr>
</table>
</body>
</html>
