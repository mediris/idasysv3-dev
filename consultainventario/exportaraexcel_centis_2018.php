<?php session_start();
include("../conectar.php");
$arqnro = trim($_GET["num"]);
//header("Pragma: ");
header("Pragma: no-cache");
header('Cache-control: ');
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Content-type: application/vnd.ms-excel");
//header("Content-type: application/octet-stream");
header("Content-disposition: attachment; filename=Reporte_CENTIS_2018.xls");

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Reporte CENTIS</title>
	</head>
	<style>

		h1, h2, h3, h4, h5{
			margin: 0;
			padding: 0;
			font-weight: normal;
			color: #32639A;
		}

		h1{
			font-size: 2em;
		}

		h2{
			font-size: 2.4em;	
		}

		h3{
			font-size: 1.6em;
			font-style: italic;
		}

		h4{
			font-size: 1.6em;
			font-style: italic;
			color: #FFF;
		}

		h5{
			font-size: 1.0em;
			font-style: italic;
			color: #666;
		}

		#background-image{
			font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
			font-size: 10px;
			margin: 0px;
			width: 100%;
			text-align: left;
			border-collapse: collapse;
		}

		#background-image th{
			padding: 12px;
			font-weight: normal;
			font-size: 12px;
			color: #339;
			border-bottom-style: solid;
			border-left-style: none;
			text-align: center;
		}

		#background-image td{
			color: #669;
			border-top: 1px solid #fff;
			padding-right: 4px;
			padding-left: 4px;
		}

		#background-image tfoot td{
			font-size: 9px;
		}

		#background-image tbody{
			background-repeat: no-repeat;
			background-position: left top;
		}

		#background-image tbody td{
			background-image: url(images/backn.png);
		}

		* html #background-image tbody td{
			/* 
	   		----------------------------
			PUT THIS ON IE6 ONLY STYLE 
			AS THE RULE INVALIDATES
			YOUR STYLESHEET
	   		----------------------------
			*/
			filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/backn.png',sizingMethod='crop');
			background: none;
		}	
	</style>
	<body>
		<?php 

	 		$wsolicitud = 0;
			if($solicitudpagina == 0){

				$sql = "SELECT T5.AAPVLA, T5.APACOD, T3.AARUMB, T4.AUMDES, T1.ACICOD, T1.AARCOD, T1.AARNIV, T1.ATICOD, T1.ASICOD, T1.AARDES, T1.AARTDT, T1.AARSTS, (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.AALCOD='0001' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS ASLSAF

				FROM IV05FP T1 
				INNER JOIN IV06FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.AARCOD = T3.AARCOD) 
				INNER JOIN IV13FP T4 ON (T1.ACICOD = T4.ACICOD AND T3.AARUMB = T4.AUMCOD) 
				LEFT JOIN IV17FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AARCOD = T5.AARCOD AND T5.APACOD = '0135') 
				
				WHERE T1.ACICOD = '".$Compania."' AND (T1.AARCOD IN (SELECT T2.AARCOD FROM IV40FP T2 WHERE T2.ACICOD = T1.ACICOD AND T2.AALCOD = '".$aalcod."' GROUP BY T2.AARCOD ORDER BY T2.AARCOD)) 
					
				ORDER BY T1.AARCOD";
				
				// echo $sql."<br/><br/>";
				$resultt = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
				
				$z = 0;
				// $tothon = 0;
				// $totest = 0;
				$lin = 1;
				$limitep = $_SESSION['solicitudlineasporpaginat'];
				$pag = 1;
				// $primero = 'S';

				while(odbc_fetch_row($resultt)){ 

					$artcod = odbc_result($resultt,'AARCOD');
					$atrdes = odbc_result($resultt,'AARDES');
					$exp = substr($atrdes ,(strripos($atrdes,"(")) );
					$aumdes = odbc_result($resultt,'AUMDES');
					$existencia = odbc_result($resultt,'ASLSAF');
					
					/*CARGA DE ENTRADAS*/
					// $listapacod = array('0133','0134','0135','0136','0137','0138','0139','0140','0141','0142','0143','0144','0145','0146','0147','0148','0149','0159','0160','0161','0162','0163','0164');

					$listapacod = array('0137','0140','0142','0164');

					//$listapacod = explode(',', $listapacod);
					$sql2="";
					$sql3="";
					$sql2.="
						SELECT T4.ACICOD, T4.AALCOD, T4.ADPCOD, T4.ATRCOD, (T5.ATRDES) as ATRNOM, T5.ATRSIG, T4.ATRNUM, T4.ATRDES, 
								T4.ATRFEC, T4.AUSCOD, T4.ATROBS, T3.ATRSEC, T3.ATRART,('".$atrdes."') AS AARDES, T3.ATRCAN, T3.ATRUMB, ('".$aumdes."') AS AUMDES, ('".$existencia."') AS ASLSAF, ";
					foreach($listapacod as $key=>$value){			
						$sql3.="
								(SELECT T6.AAPVLA FROM iv17fp T6 WHERE T6.ACICOD=T3.ACICOD and T6.AARCOD=T3.ATRART AND T6.APACOD IN (".$value.") ) AS N".$value." , ";
					}
					$sql2.= substr($sql3,0, (strripos($sql3,",")));
					$sql2.="	
						FROM IV16FP T3 
							INNER JOIN IV15FP T4 ON ( T3.ACICOD=T4.ACICOD AND T3.AALCOD=T4.AALCOD AND T3.ATRCOD=T4.ATRCOD AND T3.ATRNUM=T4.ATRNUM )                                                                   
							INNER JOIN IV12FP T5 ON ( T4.ACICOD=T5.ACICOD AND T4.ATRCOD=T5.ATRCOD AND T5.ATRSIG='+')
						WHERE T4.ACICOD='".$Compania."' AND T4.AALCOD='".$aalcod."' AND T3.ATRART = '".$artcod."' AND T4.ATRCOD IN ('01') 
						ORDER BY T4.ATRNUM , T3.ATRART
						";
						
					// echo $sql2."<br/><br/>";
					// DIE();
					$resultt2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111 - ".odbc_error($resultt2) ));
					while(odbc_fetch_row($resultt2))
					{
						$jml = odbc_num_fields($resultt2);
						$row[$z]["pagina"] =  $pag;
						for($i=1;$i<=$jml;$i++)
						{	
							$row[$z][odbc_field_name($resultt2,$i)] =  odbc_result($resultt2,$i);
						}
						$z++;
						if ($lin>=$limitep) 
						{
							$limitep+=$_SESSION['solicitudlineasporpaginat'];
							$pag++;
						}
						$lin++;
					}
				}

				$totsol = ($lin-1);
				$_SESSION['totalsolicitudes'] = $totsol;
				$_SESSION['solicitudarreglo'] = $row;
				$solicitudpagina = 1;
				$_SESSION['solicitudpaginas'] = $pag;
			}//fin de solicitudpagina
			/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
			$paginat = $_SESSION['solicitudarreglo'];
		?>

		<table width="100%" border="0">
			<tr>
				<td height="89">
					<h1>
						<?php if($Compania=='14'){?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logoidacadef2005.png" width="280" height="68" />
						<?php }else{ ?>
							<img src="http://<?php echo $Direccionip; ?>/idasysv3/images/logomeditronnuevo.png" width="280" height="68" />
						<?php } ?>
	   				</h1>
	  				<h5>RIF:  <?php echo $Companiarif; ?></h5>
	  			</td>
			</tr>
			<tr>
				<td>
					<table width="100%" id="background-image" >
	  					<thead>
	  						<tr>
	        					<th colspan="17" scope="col">
	        						<h3>Reporte de CENTIS</h3>
	        					</th>
	    					</tr>
	    					<tr>
	        					<th colspan="17" scope="col"><h4>Elaborado el <?php echo $Fechaactual; ?> a las <?php echo $Horaactual2; ?></h4></th>
	    					</tr>
	  						<tr>
	        					<th colspan="17" scope="col"><h3>Almac&eacute;n: <?php echo alamcen($aalcod, $Compania);?></h3></th>
	    					</tr>
	  						<tr>
						        <th scope="col">Nro transacci&oacute;n</th>
						        <th scope="col">Fecha llegada a Meditron</th>
						        <th scope="col">C&oacute;digo Art.</th>
						        <th scope="col">Art&iacute;culo</th>
						        <th scope="col">Descripci&oacute;n Larga</th>
						        <th scope="col">Nro gu&iacute;a</th>
						        <th scope="col">Nro empaque</th>
						        <th scope="col">Nro referencia entrada</th>
						        <th scope="col">Fecha entrada</th>
		                        <th scope="col">Cantidad entrada</th>
		                        <th scope="col">Observaciones entrada</th>
		                        <th scope="col">Nro referencia salida</th>
		                        <th scope="col">Fecha salida</th>
		                        <th scope="col">Cantidad salida</th>
		                        <th scope="col">Destino</th>
		                        <th scope="col">Observaciones salida</th>
		                        <th scope="col">Existencia actual</th>
	    					</tr>
	  					</thead>
	   					<tbody>
							<?php 

							$pagact = $solicitudpagina;
							$part = 1;

							for($g = 0; $g < (count($paginat)); $g++){
								// echo "//**".$paginat[$g]["ATRART"]."<br>";
								/*entradas*/

								$sql2 = "(SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD, (SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD = T1.ACICOD AND T6.ATRNUM = T3.ATRNUM AND T6.ATRCOD = T3.ATRCOD AND T6.APACOD IN ('1506')) AS NROGIA FROM IV16FP T1 
							
									INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD AND T3.ATRSTS = '02') 
									INNER JOIN IV12FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
									LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM) 

									WHERE T1.ACICOD = '$Compania' AND T1.AALCOD = '$aalcod' AND T1.ATRCOD in ( '01', '03', '04', '06', '0101' ) AND T1.ATRART='".$paginat[$g]["ATRART"]."' 

									ORDER BY T3.ATRFEC)

									UNION

									(SELECT T3.ATRNUM, T3.ATRCOD, T2.ATRDES, T3.ATRFEC, T1.ATRCAN, (T3.ATRDES) AS DESATR, T3.ATROBS, T5.ADSNRO, T5.ATSCOD, (SELECT T6.AAPVLA FROM IV38FP T6 WHERE T6.ACICOD = T1.ACICOD AND T6.ATRNUM = T3.ATRNUM AND T6.ATRCOD = T3.ATRCOD AND T6.APACOD IN ('1506')) AS NROGIA 

									FROM IV16FP T1 
									INNER JOIN IV15FP T3 ON (T1.ACICOD = T3.ACICOD AND T1.ATRCOD = T3.ATRCOD AND T1.ATRNUM = T3.ATRNUM AND T1.ADPCOD = T3.ADPCOD) 
									INNER JOIN iv12fp T2 ON (T1.ACICOD = T2.ACICOD AND T1.ATRCOD = T2.ATRCOD) 
									LEFT JOIN IV35FP T5 ON (T1.ACICOD = T5.ACICOD AND T1.AALCOD = T5.AALCOD AND T3.ADPCOD = T5.ADPCOD AND T1.ATRCOD = T5.ATRCOD AND T1.ATRNUM = T5.ATRNUM) 

									WHERE T1.ACICOD = '$Compania' AND T1.AALCOD <> '$aalcod' AND T1.ATRCOD in ('03') AND T1.ATRART = '".$paginat[$g]["ATRART"]."' 

									ORDER BY T3.ATRFEC)";
								
								//echo $sql2."<br/><br/>";
								$result2 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
								$result22 = odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
								$canEnt = 0;
								//$canEnt = odbc_num_rows($result22);
								while(odbc_fetch_row($result22)){
									$canEnt++;
								}
								//echo "<br>"."*".$canEnt;
									
								/*salidas*/
								$sql3 = "SELECT T2.ADSNRO, T3.ATSCOD, T3.ATSDES, T2.ATRFEC, T1.ATRCAN, T2.ATROBS, T4.AISCOD, T4.AISDES, (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1501')) AS N1501, (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1502')) AS N1502, (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1504')) AS N1504, (SELECT T6.AAPVLA FROM IV46FP T6 WHERE T6.ACICOD=T1.ACICOD and T6.ADSNRO=T2.ADSNRO AND T6.APACOD IN ('1505')) AS N1505 

									FROM IV36FP T1 
									INNER JOIN IV35FP T2 ON (T1.ACICOD = T2.ACICOD AND T1.AALCOD = T2.AALCOD AND T1.ADPCOD = T2.ADPCOD AND T1.ATSCOD = T2.ATSCOD AND T1.ADSNRO = T2.ADSNRO) 
									INNER JOIN IS15FP T3 ON (T1.ACICOD = T3.ACICOD AND T2.ATSCOD = T3.ATSCOD) 
									INNER JOIN IV42FP T4 ON (T2.ACICOD = T4.ACICOD AND T2.AISCOD = T4.AISCOD) 

									WHERE T1.ACICOD = '$Compania' AND T1.AARCOD = '".$paginat[$g]["ATRART"]."' AND T1.AALCOD NOT IN ('06','07') AND T3.ATSCOD IN ('01','04') AND T1.ADSNRO <> '321' AND T1.ADSNRO <> '325' AND T1.ADSNRO <> '491' AND T1.ADSNRO <> '492' AND T1.ADSNRO <> '485' AND T1.ADSNRO <> '486' 

									ORDER BY T2.ATRFEC";
								
								//echo $sql3."<br/><br/>";
								$result3 = odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
								$result33 = odbc_exec($cid,$sql3)or die(exit("Error en odbc_exec 11111"));
								//echo "<br>"."+".$canSal = odbc_num_rows($result3);
								$canSal = 0;
								//$canEnt = odbc_num_rows($result22);
								while(odbc_fetch_row($result33)){
									$canSal++;
								}
								//echo "<br>"."*".$canSal;

								$resultES = '';
								$numRows = '';
								if($canSal > $canEnt){
									$resultES = $result3;
									$numRows = $canSal;
									//echo "<br>"."* salida";
								}else if($canSal <= $canEnt){
									$resultES = $result2;
									$numRows = $canEnt;
									//echo "<br>"."* entrada";
								}
								$rowpos = 0;
								//echo "numRows:".$numRows;
								//echo "rowpos:".$rowpos;				
                         		?>     
                                        
                         		<?php
                                 	//while(odbc_fetch_row($resultES)){   
							 	if($numRows > $rowpos){
								 	while($numRows > $rowpos){   
									 
									//echo "numRows:".$numRows;
									//echo "rowpos:".$rowpos;
									?>
                                    	<tr>

                                    		<!-- N° transaccion -->
                                            <td style="text-align: left; vertical-align: middle;">
                                            	<?php echo $paginat[$g]["ATRNUM"] != '' ? $paginat[$g]["ATRNUM"] : '--';?>
                                            </td>

                                            <!-- Fecha llegada a meditron -->
                                            <td style="text-align: left; vertical-align: middle;">
                                            	<?php echo $paginat[$g]["N0140"] != '' ? formatDate($paginat[$g]["N0140"],'dd.mm.aaaa', 'dd/mm/aaaa' ) : '--';?>
                                            </td>

                                            <!-- Código del artículo -->
                                            <td style="text-align: left; vertical-align: middle;">
                                            	<?php echo $paginat[$g]["ATRART"] != '' ? $paginat[$g]["ATRART"] : '--';?>
                                            </td>

                                            <!-- Artículo -->
                                            <td style="text-align: center; vertical-align: middle;">
                                            	<?php echo $paginat[$g]['AARDES'] != '' ? $paginat[$g]['AARDES'] : '--';?>
                                            </td> 

                                            <!-- Descripción -->
                                            <td style="text-align: center; vertical-align: middle;">
                                            	<?php echo $paginat[$g]['N0142'] != '' ? $paginat[$g]['N0142'] : '--';?>
                                            </td>

                                            <!-- N° guía -->
                                            <td style="text-align: center; vertical-align: middle;">
                                            	<?php echo $paginat[$g]['N0137'] != '' ? $paginat[$g]['N0137'] : '--';?>
                                            </td>

                                            <!-- N° empaque -->
                                            <td style="text-align: center; vertical-align: middle;">
                                            	<?php echo $paginat[$g]['N0164'] != '' ? $paginat[$g]['N0164'] : '--';?>
                                            </td>

                                        	<?php
										
								          	$rowpos++;       
										  	odbc_fetch_row($result2);
										  	odbc_fetch_row($result3);

                                            /* <!-- entradas --> */
											if($rowpos <= $canEnt){?>

												<!-- N° referencia entrada -->
                                            	<td style="text-align: center; vertical-align: middle;">
                                            		<?php echo odbc_result($result2, 'ATRNUM') != '' ? odbc_result($result2, 'ATRNUM') : '--'; echo '(E)';?>
                                            	</td>

                                            	<!-- Fecha entrada -->
                                            	<td style="text-align: center; vertical-align: middle;">
                                            		<?php echo odbc_result($result2, 'ATRFEC') != '' ? formatDate(odbc_result($result2, 'ATRFEC'),'aaaa-mm-dd', 'dd/mm/aaaa' ) : '--';?>
                                            	</td>

                                            	<!-- Cantidad entrada -->
                                            	<td style="text-align: center; vertical-align: middle;">
                                            		<?php echo odbc_result($result2, 'ATRCAN') != '' ? number_format(odbc_result($result2, 'ATRCAN'),2,',','.') : '--';?>
                                            	</td>

                                            	<!-- Observacion entrada -->
                                            	<td style="text-align: center; vertical-align: middle;">
                                            		<?php echo odbc_result($result2, 'ATROBS') != '' ? odbc_result($result2, 'ATROBS') : '--';?>
                                            	</td>

                             				
                             				<?php }else{ ?>

                            					<td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>

                          					<?php }
											 
                                            /* <!-- salidas --> */
										 	if(($rowpos <= $canSal) && (number_format(odbc_result($result3, 'ATRCAN'),0,',','.') >= 0)) { ?>     

										 		<!-- N° referencia salida -->
                                                <td style="text-align: center; vertical-align: middle;">
                                                	<?php echo odbc_result($result3, 'ADSNRO') != '' ? odbc_result($result3, 'ADSNRO') : '--'; echo "(S)";?>
                                                </td>	
													
													<!-- Fecha salida -->
                                                <td style="text-align: center; vertical-align: middle;">
                                                	<?php echo odbc_result($result3, 'ATRFEC') != '' ? formatDate(odbc_result($result3, 'ATRFEC'),'aaaa-mm-dd', 'dd/mm/aaaa' ) : '--';?>
                                                </td>

                                                <!-- Cantidad salida -->
                                                <td style="text-align: center; vertical-align: middle;">
                                                	<?php echo odbc_result($result3, 'ATRCAN') != '' ? number_format(odbc_result($result3, 'ATRCAN'),2,',','.') : '--';?>
                                                </td>
												
												<!-- Destino salida -->
                                                <td style="text-align: center; vertical-align: middle;">
                                                	<?php echo odbc_result($result3, 'AISDES') != '' ? odbc_result($result3, 'AISDES') : '--';?>
                                                </td>

                                                <!-- Observacion salida -->
                                                <td style="text-align: center; vertical-align: middle;">
                                                	<?php echo odbc_result($result3, 'ATROBS') != '' ? odbc_result($result3, 'ATROBS') : '--';?>
                                                </td>

                           		 			<?php }else{ ?>

                            					<td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>
                                                <td style="text-align: center; vertical-align: middle;">--</td>

                          					<?php }
                                        	
											{	?>

												<!-- Existencia -->
                                               	<td style="text-align: center; vertical-align: middle;">
                                               		<?php echo $paginat[$g]["ASLSAF"] != '' ? number_format($paginat[$g]["ASLSAF"],2,',','.') : '--';?>
                                               	</td>

											<?php }?>
                                        </tr>
                       				<?php }
								}else{	?> 
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
                                    <td style="text-align: center; vertical-align: middle;">--</td>
								<?php }
								echo "";
	        				}?>      
	    				</tbody>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
