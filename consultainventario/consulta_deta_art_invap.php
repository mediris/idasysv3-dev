<?php 
/*
 * jDavila
 * 23/05/2012 
 * modificado: 24/05/2012
 */
session_start();
include("../conectar.php");

$tinva = $_GET['tinvan'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<title>Idasys V3</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
<script src="../shadowbox.js" type="text/javascript"/> </script>
<script language="JavaScript" type="text/JavaScript">
Shadowbox.init({overlayOpacity: "0.5"});
</script>
<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
</style>
<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
			
			 $(document).ready(function() 
			 {
			      document.getElementById('info').style.display="";
				  $('#info').dataTable( 
				  {
					"bStateSave": true,
			        "oLanguage": 
					{
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": 
						{
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
					/*,
					"aoColumnDefs": [
						{ "bVisible": false, "aTargets": [ 0 ] }
					]*/
			      } );
			  } );
</script>

</head>
<body background="../images/fondo idaca.jpg" >
<div id="wrapper">
  <?php include("../superior.php");?>
  <div id="page">
     <?php include("../validar.php");?>

	  
 		<?php 
				
				if ($bandera==1) 
				{	
					$wsolicitud=0;
					if ($solicitudpagina==0) 	
					{
						$tipoInve ='';
						if(!empty($tinva)){
							foreach($tinva as $key => $value){
								$tipoInve .= "'".$value."',";
								/*
								echo '<script>
										$("#tinvan").prop("checked", "checked");
									  </script>';
									  */
							}
							$tipoInve = substr($tipoInve,0, (strripos($tipoInve,",")));
						}
						
						
						$sql="SELECT T1.ACICOD, T1.AALCOD, T1.AARCOD, T2.AARDES, T1.AUMCOD,T3.AUMDES, T4.AMCCOD, T5.AMCDES, 
									SUM(CASE WHEN T1.ASLFEF=(SELECT MAX(T2.ASLFEF) FROM IV40FP T2 WHERE T2.ACICOD=T1.ACICOD AND T2.AALCOD=T1.AALCOD AND T2.AARCOD=T1.AARCOD AND T2.ALTCOD=T1.ALTCOD AND T2.ASLFEF<'16.11.2015') THEN T1.ASLSAF ELSE 0 END) AS ASLSAF , 
									(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('0111') ) AS DESLAR, 
									(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('0116') ) AS MODELO, 
									(SELECT T6.AAPVLA FROM IV17FP T6 WHERE T6.ACICOD=T1.ACICOD AND T6.AARCOD=T1.AARCOD AND T6.APACOD IN ('0112') ) AS SERIAL 
								FROM IV40FP T1 
									INNER JOIN IV05FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AARCOD=T2.AARCOD) 
									INNER JOIN IV13FP T3 ON (T1.ACICOD=T3.ACICOD AND T1.AUMCOD=T3.AUMCOD) 
									INNER JOIN IV06FP T4 ON (T2.ACICOD=T4.ACICOD AND T2.AARCOD=T4.AARCOD) 
									INNER JOIN IV04FP T5 ON (T1.ACICOD=T5.ACICOD AND T4.AMCCOD=T5.AMCCOD)
								WHERE T1.ACICOD='$Compania' AND T1.AALCOD='$aalcod' 
								";
						if(!empty($tipoInve)){
							$sql.="  AND T2.ATICOD IN (".$tipoInve.")";
						}
						$sql.=" GROUP BY T1.ACICOD, T1.AALCOD, T1.AARCOD, T2.AARDES, T1.AUMCOD, T3.AUMDES, T4.AMCCOD, T5.AMCDES 
								ORDER BY T1.AARCOD     ";
								
								//echo $sql;
								
								$resultt=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
								
								
								$z=0;
								$tothon=0;
								$totest=0;
								$lin=1;
								$limitep=$_SESSION['solicitudlineasporpaginat'];
								$pag=1;
								$primero='S';
								
								while(odbc_fetch_row($resultt))
								{
									$jml = odbc_num_fields($resultt);
									$row[$z]["pagina"] =  $pag;
									for($i=1;$i<=$jml;$i++)
									{	if (odbc_field_name($resultt,$i)=='ARGTTE') {$totest+=odbc_result($resultt,$i);}
										if (odbc_field_name($resultt,$i)=='ARGTTM') {$tothon+=odbc_result($resultt,$i);}
										$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
									}
									$z++;
									if ($lin>=$limitep) 
									{
										$limitep+=$_SESSION['solicitudlineasporpaginat'];
										$pag++;
									}
									$lin++;
								}
								$totsol=($lin-1);
								$_SESSION['totalsolicitudes']=$totsol;
								$_SESSION['solicitudarreglo']=$row;
								$solicitudpagina=1;
								$_SESSION['solicitudpaginas']=$pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat=$_SESSION['solicitudarreglo'];
					//echo "**";
					//print_r($paginat);
					//echo "**";
				}
			?>
        <div id="content3" >   
        	<table width="100%"   border="0">
              <tr>
                <td width="65%" scope="col" colspan="2"><h1  class="title">Consulta Detalle Art&iacute;culos</h1>
                  <hr /></td>
                <td width="13%" scope="col"><div align="left">
                  <table width="100%"  border="0">
                    <tr>
                      <th width="30%" scope="col" >
                      <div class="flechas">
                      	<a href="exportaraexcel_d_art_invap.php?&aalcod=<?php echo $aalcod; ?>&tinvan=<?php echo $url222 = urlencode( serialize($tinva) );?>" target="_blank">
                        	<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel"/>
                        </a>
                      </div></th>
                    </tr>
                  </table>
                </div></td>
              </tr>
              <tr>
              	<td colspan="3">
                	<form name="form" id="form" method="post" on>
                    <input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                	<table>
                        <tr>
                        	<td>&nbsp;Almac�n:</td>
                            <td>&nbsp;<?php //echo $aalcod; ?>
                            	<select name="aalcod" id="aalcod" onchange="cargarSelectTpInven(<?php ?>)" >
									<?php 
                                    //$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
									if(accesotodasunisol_alma('4')=='S')
									{
										$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania'  order by AALDES ";//and AALCOD <> '0017'
									}
									else
									{
										$sql = "SELECT T1.AALCOD, T2.AALDES FROM iv44fp T1, IV07FP T2 WHERE T1.acicod ='$Compania' AND T1.ACICOD=T2.ACICOD AND T1.auscod='$Usuario' AND T1.AALCOD= T2.AALCOD order by t2.AALDES";
									}
                                    $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
									$select = '';
                                    while(odbc_fetch_row($result1)){
                                        $cod=trim(odbc_result($result1,1));
                                        $des=trim(odbc_result($result1,2));
										if(!empty($aalcod))
										{
											if($cod == $aalcod){
												$select = ' selected="selected" ';
											}
											else{
												$select = '';
											}
										}											
                                    ?>
                                        <option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                             <td rowspan="2"><a href="javascript:busqueda3();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
                        </tr>
                        <tr>
                        	<td>Tipo de Inventario</td>
                            <td colspan="4">
                            	<div id="table_tpinven">
                                </div>
                                <script>cargarSelectTpInven();</script>
                            </td>
                        </tr>
                    </table>
                    </form>
                    <hr />
                </td>
                
              </tr>
              <!--<tr>
                <td colspan="2" scope="col"><h5 align="left"><?php echo $paginat[0]["ALCSED"];?>wwww</h5></td>
                <td scope="col"> Pagina: <?php if (!$solicitudpagina) {$paginaarriba=1;} else {$paginaarriba=$solicitudpagina;}; echo $paginaarriba."/".$_SESSION['solicitudpaginas']; ?></td>
              </tr>-->
            </table>   
		<div id="container">
            <div id="demo">
                <table width="100%" id="info" style="display:none">
                  <thead>
                    <tr>
                        
                        <th width="30%" scope="col">Art&iacute;culo</th>
                        <th width="30%" scope="col">Descripci&oacute;n</th>
                        <th width="30%" scope="col">Descripci&oacute;n Larga</th>
                        <!--<th width="30%" scope="col">Descripci&oacute;n Extendida</th>-->
                        <th width="10%" scope="col">Ubicaci&oacute;n</th>
                        <th width="10%" scope="col">Unidad de Medida</th>
                        <th width="10%" scope="col">Marca</th>
                        <th width="10%" scope="col">Modelo</th>
                        <th width="10%" scope="col">Serial</th>
                        <th width="10%" scope="col">Saldo Final</th>
                        <th width="5%" scope="col">Opciones</th>
                    </tr>
                  </thead>
    				<tbody>
        						<?php 
									//print_r(count($paginat));
									$pagact=$solicitudpagina;
									for($g=0; $g < (count($paginat)); $g++)
									{
                                ?>
                                        <tr>
                                        	
                                        	<td scope="col"><div><?php echo $paginat[$g]["AARCOD"]."</strong>";?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["AARDES"];?></div></td>
                                            <td scope="col"><div><?php echo str_replace("\n","<br>",($paginat[$g]["DESLAR"]));?></div></td>
                                            <!--<td scope="col"><div>
                                            	<?php 
													$descripcionART = trim("");
													if(file_exists("../tiposdeinventarios/articulos/descripcion/".$Compania.'_'.trim($aarcod).".txt")){
														$file = fopen("../tiposdeinventarios/articulos/descripcion/".$Compania.'_'.trim($aarcod).".txt", "r");
														while(!feof($file)) {
															//$descripcionART.= wordwrap(str_replace( "\n","<br />",( ( fgets($file) ) ) ),100,"<br />");
															$descripcionART.= str_replace( "\n","<br />",( fgets($file) )  );
														}
														fclose($file);
													}else{
														$descripcionART .= trim("");
													}
													
													echo '<p style="margin-bottom:0px;margin-top:2;">'.$descripcionART.'</p>';
													//echo '<br /><p style="margin-bottom:0px;margin-top:-5px;margin-left:125px;">'.$descripcionART.'</p><br />';
													
												?>
                                            	</div>
                                            </td>-->
                                           
                                            <td scope="col">
                                                <div>
                                                	<strong>
													<?php 
                                                        $list = list_ubiart($cid, $Compania, $paginat[$g]["AALCOD"], $paginat[$g]["AARCOD"],2);
                                                        //echo "*".trim($paginat[$g]["AUBCOD"])."*";
                                                        echo '<a rel="shadowbox;width=400;height=300" href="editarfubicacion.php?&aarcod='.trim($paginat[$g]["AARCOD"]).'&aalcod='.$aalcod.'" title="Editar" >';
                                                        
                                                        if(trim($list)!=''){
                                                            echo $list; } 
                                                        else { 
                                                            echo ' - Sin Ubicaci�n';
                                                        }
                                                        echo '</a>';
                                                     ?>
                                                	</strong>
                                                </div>
                                            </td>
                                           
                                            <td scope="col"><div><?php echo $paginat[$g]["AUMDES"];?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["AMCDES"];?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["MODELO"];?></div></td>
                                            <td scope="col"><div><?php echo $paginat[$g]["SERIAL"];?></div></td>
                                            
                                            <td scope="col"><div><?php echo number_format(($paginat[$g]["ASLSAF"]),2,",",".");?></div>  </td>
                                            <td scope="col">
                                                    <?php if($Compania == '40') { ?>
                                                    	<a href="javascript:abriretiquetas('<?php echo $aalcod; ?>','<?php echo trim($paginat[$g]["AARCOD"]); ?>','','' )"><img src="../images/etiqueta.png" title="Ver Detalle" width="31"  border="0"></a>		
                                                    <?php } ?>
                                            </td>
                                        </tr>
                            	<?php } ?>      
                  </tbody>
                </table>
</div>
		  </div>
	  </div>
	</div>
		<!-- end #content -->
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->

	<div id="footer">
		<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
	</div>
    <?php 
		echo "<script>
				var list = [];
				var pos = 1;
				var lista = \"".$tipoInve."\";
				$('input[name=\"tinvan[]\"]').each(function() { 
					//alert($(this).val() );
					if( lista.indexOf($(this).val()) != -1 ){
						//alert('paso '+$(this).val()) ;
						 $(this).attr('checked', true); 
					}
					pos++;
				});
			  </script>";
	?>
 
	<!-- end #footer -->
</body>
</html>
