<?php 
/*
 * BGONZALEZ
 * 24/09/2020
 */
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<title>Idasys V3</title>
		<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
		<script language="JavaScript" src="../javascript/javascript.js"></script>
		<script language="JavaScript" src="../calendario/javascripts.js"></script>
		<script language="JavaScript" src="javascript.js"></script>
		<script language="JavaScript" src="../javascript/jquery.js"></script>
		<link href="../shadowbox.css"  rel="stylesheet" type="text/css"/>
		<script src="../shadowbox.js" type="text/javascript"/> </script>
		<script language="JavaScript" type="text/JavaScript">
			Shadowbox.init({overlayOpacity: "0.5"});
		</script>
		<style type="text/css" title="currentStyle">
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_page;?>";
			@import "../DataTables-1.9.4/media/css/<?php echo $demo_table;?>";
		</style>
		<script type="text/javascript" language="javascript" src="../DataTables-1.9.4/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
	      		document.getElementById('info').style.display="";

				$('#info').dataTable({
					"bStateSave": true,
			        "oLanguage": {
			          	"sLengthMenu": "Mostrar _MENU_ registros por Pag.",
						"sZeroRecords": "No Existen Registros",
						"sInfo": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
						"sInfoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
						"sInfoFiltered": "(filtrando de _MAX_ de registros)",
						"sSearch": "Buscar",
						"oPaginate": {
							"sNext": "Siguiente",
							"sPrevious": "Anterior"
						}
			        }
		      	});
		  	});
		</script>
	</head>

	<body background="../images/fondo idaca.jpg" >
		<div id="wrapper">
  			<?php include("../superior.php");?>
  			<div id="page">
     			<?php  include("../validar.php");?>
 				<?php 
					
				if($bandera == 1){	

					
					$desde = $_GET['desde'];
					$hasta = $_GET['hasta'];
					$aalcod = $_GET['aalcod'];
					$wsolicitud = 0;

					if($solicitudpagina == 0){

						$sql = "SELECT T2.AISCOD, T1.AARCOD, T3.AARDES, T4.AARUMB, T5.AUMDES, T6.AISDES, SUM(T1.ATRCAN) AS ATRCAN, (SUBSTR(CHAR(T2.ATRFEC), 1, 4) || SUBSTR(CHAR(T2.ATRFEC), 6, 2)) AS ANOMES

						FROM IV36FP T1
						JOIN IV35FP T2 ON(T2.ACICOD = T1.ACICOD AND T2.ADSNRO = T1.ADSNRO AND T2.ATRFEC BETWEEN '".$desde."' AND '".$hasta."')
						JOIN IV05FP T3 ON(T3.ACICOD = T1.ACICOD AND T3.AARCOD = T1.AARCOD)
						JOIN IV06FP T4 ON(T4.ACICOD = T1.ACICOD AND T4.AARCOD = T1.AARCOD)
						JOIN IV13FP T5 ON(T5.ACICOD = T1.ACICOD AND T5.AUMCOD = T4.AARUMB)
						JOIN IV42FP T6 ON(T6.ACICOD = T1.ACICOD AND T6.AISCOD = T2.AISCOD)

						WHERE T1.ACICOD = '".$Compania."' AND T1.AALCOD = '".$aalcod."' AND T1.ATSCOD IN('01', '02', '03', '04', '05', '06', '07')

						GROUP BY T2.AISCOD, T1.AARCOD, T3.AARDES, T4.AARUMB, T5.AUMDES, T6.AISDES, (SUBSTR(CHAR(T2.ATRFEC), 1, 4) || SUBSTR(CHAR(T2.ATRFEC), 6, 2))

						ORDER BY T2.AISCOD, T1.AARCOD, T3.AARDES, T4.AARUMB, T5.AUMDES, T6.AISDES, (SUBSTR(CHAR(T2.ATRFEC), 1, 4) || SUBSTR(CHAR(T2.ATRFEC), 6, 2))";

						$resultt = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111"));
						
						$z = 0;
						$lin = 1;
						$limitep = $_SESSION['solicitudlineasporpaginat'];
						$pag = 1;

						while(odbc_fetch_row($resultt)){ 

							$jml = odbc_num_fields($resultt);
							$row[$z]["pagina"] =  $pag;
							for($i=1;$i<=$jml;$i++){	
								$row[$z][odbc_field_name($resultt,$i)] =  odbc_result($resultt,$i);
							}
							$z++;
							if ($lin>=$limitep) 
							{
								$limitep+=$_SESSION['solicitudlineasporpaginat'];
								$pag++;
							}
							$lin++;
						}

						$totsol = ($lin-1);
						$_SESSION['totalsolicitudes'] = $totsol;
						$_SESSION['solicitudarreglo'] = $row;
						$solicitudpagina = 1;
						$_SESSION['solicitudpaginas'] = $pag;
					}//fin de solicitudpagina
					/*se muestra la cantidad de elementos segun la solicitada en $solicitudpagina*/
					$paginat = $_SESSION['solicitudarreglo'];
				}?>
   	 			<div id="content3" >   
        			<table width="100%"   border="0">
              			<tr>
                			<td width="65%" scope="col" colspan="2"><h1  class="title">Reporte de Dotaciones por Servicios IDACA</h1><hr /></td>
                			<td width="13%" scope="col">
                				<div align="left">
                  					<table width="100%"  border="0">
                    					<tr>
                      						<th width="30%" scope="col" >
                      							<div class="flechas">
                      								<a href="exportaraexcel_idaca_servicios.php?&aalcod=<?php echo $aalcod; ?>&desde=<?php echo $desde?>&hasta=<?php echo $hasta?>" target="_blank">
                      									<img src="../images/excel.jpg" alt="" width="30" height="30" title="Exportar a Excel Salida"/>
                        							</a>
                      							</div>
                      						</th>
                    					</tr>
                  					</table>
                				</div>
                			</td>
              			</tr>
              			<tr>
              				<td colspan="3">
                				<form name="form" id="form" method="post">
                    				<input type="hidden" name="bandera" id="bandera" value="<?php echo $bandera;?>" />
                    				<input type="hidden" name="tipo" id="tipo" value="<?php echo $tipo;?>" />
                					<table>

                						<tr>
				                        	<td>&nbsp;Desde:</td>
				                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('desde','form',$desde); ?></td>
				                            <td>&nbsp;</td>
				                            <td>&nbsp;Hasta:</td>
				                            <td>&nbsp;<?php echo  escribe_formulario_fecha_vacio('hasta','form',$hasta); ?></td>
                            				<td rowspan="2"><a href="javascript:busquedaservicios();"><img src="../images/precesarconsulta.png" width="50" height="50" alt="Realizar Consulta" style="border:none;"></a></td>
				                        </tr>
                        				<tr>
                        					<td>&nbsp;Almac&eacute;n:</td>
                            				<td>&nbsp;<?php //echo $fecha; ?>
                                				<!-- <select name="aalcod" id="aalcod">
	                                    			<option value= "0001">Sede Principal.(0001)</option>
                                				</select> -->
                                				<select name="aalcod" id="aalcod">
													<?php 
		                                			//$sql="SELECT AALCOD, AALDES FROM iv07fp where acicod ='$Compania' order by AALDES ";
													if(accesotodasunisol_alma('4') == 'S'){
														$sql = "SELECT AALCOD, AALDES FROM IV07FP WHERE ACICOD = '$Compania' ORDER BY AALDES ";//and AALCOD <> '0017'
													}else{
														$sql = "SELECT T1.AALCOD, T2.AALDES FROM IV44FP T1, IV07FP T2 WHERE T1.ACICOD = '$Compania' AND T1.ACICOD = T2.ACICOD AND T1.AUSCOD = '$Usuario' AND T1.AALCOD = T2.AALCOD ORDER BY T2.AALDES";
													}
		                                			$result1 = odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
													$select = '';
		                                			while(odbc_fetch_row($result1)){
		                                    			$cod = trim(odbc_result($result1,1));
		                                    			$des = trim(odbc_result($result1,2));
														if(!empty($aalcod)){
															if($cod == $aalcod){
																$select = ' selected="selected" ';
															}else{
																$select = '';
															}
														}?>
		                                    			<option value= "<?php echo $cod; ?>" <?php echo $select?> ><?php echo $des."(".$cod.")"; ?></option>
		                            				<?php } ?>
                                				</select>
	                            			</td>
	                            			<td>&nbsp;</td>
	                            			<td>&nbsp;</td>
	                            			<td>&nbsp;</td>
                        				</tr>
                					</table>
                    			</form>
	                    		<hr />
	                		</td>
              			</tr>
            		</table>   
					<div id="container">
            			<div id="demo">
                			<table width="100%" id="info" style="display:">
                  				<thead>
                    				<tr>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">C&oacute;d Art.</th>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Art&iacute;culo</th>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Unidad de medida</th>
				                        <th scope="col" rowspan="2" style="background-color:rgb(204,204,204)">Servicio</th>
				                        <th scope="col" colspan="2" style="background-color:rgb(204,204,204)">Cantidades Mensuales</th>
				                    </tr>
				                    <tr>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Cantidad</strong></th>
				                        <th scope="col" style="background-color:rgb(204,204,204)"><strong>Fecha</strong></th>
				                    </tr>
                 				</thead> 
    							<tbody>

        							<?php
									$pagact = $solicitudpagina;
									$part = 1;
									for($g = 0; $g < (count($paginat)); $g++){
										// echo "//**".$paginat[$g]["AARCOD"]."<br>";

										$ano = substr($paginat[$g]["ANOMES"],0, 4);
										$mes = substr($paginat[$g]["ANOMES"],4, 2); ?>

                                    	<tr>

                                     		<!-- Código del artículo -->
	                                        <td style="text-align: left; vertical-align: middle;">
	                                        	<?php echo $paginat[$g]["AARCOD"] != '' ? $paginat[$g]["AARCOD"] : '--';?>
	                                        </td>

	                                        <!-- Artículo -->
	                                        <td style="text-align: left; vertical-align: middle;">
	                                        	<?php echo $paginat[$g]["AARDES"] != '' ? $paginat[$g]["AARDES"] : '--';?>
	                                        </td>

	                                        <!-- Unidad de medida -->
	                                        <td style="text-align: left; vertical-align: middle;">
	                                        	<?php echo $paginat[$g]["AUMDES"] != '' ? $paginat[$g]["AUMDES"] : '--';?>
	                                        </td>

	                                        <!-- Servicio -->
	                                        <td style="text-align: left; vertical-align: middle;">
	                                        	<?php echo $paginat[$g]["AISDES"] != '' ? $paginat[$g]["AISDES"] : '--';?>
	                                        </td>

	                                        <!-- Cantidad -->
	                                        <td style="text-align: left; vertical-align: middle;">
	                                        	<?php echo $paginat[$g]["ATRCAN"] != '' ?  number_format($paginat[$g]["ATRCAN"],2,',','.') : '--';?>

	                                        </td>

	                                        <!-- Fecha -->
	                                        <td style="text-align: left; vertical-align: middle;">
	                                        	<?php echo $paginat[$g]["ANOMES"] != '' ? $ano . "/" .$mes : '--';?>
	                                        </td>

                                        </tr>

                           				
									<? }
									echo "";?>    
                  				</tbody>
                			</table>
						</div>
		  			</div>
	  			</div>
			</div>
			<div style="clear: both;">&nbsp;</div>
		</div>
		<div id="footer">
			<div id="opcionesmasusadas"><?php include("../opcionesmasusadasphp.php"); ?></div>
		</div>
	</body>
</html>
