/*
jDavila
19/03/13
*/
function agregar()
{
	document.form.aarcod.value="";
	document.form.altcod.value="";
	document.form.altfvf.value="";
	document.form.altfef.value="";
	document.form.altfra.value="";
	
	$("#aftsav").hide(1);
}

/*
jDavila
20/03/13
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait"; 

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "lotesagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
21/03/13
*/
function editar(aarcod, altcod) {

	var param = [];
	param['aarcod']=aarcod;
	param['altcod']=altcod;

	ejecutasqlp("lotesinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.aarcod.value=gdata[i].AARCOD;
		document.getElementById("wsaarcod").innerHTML=gdata[i].AARCOD;
		document.form.altcod.value=gdata[i].ALTCOD;
		document.getElementById("wsaltcod").innerHTML=gdata[i].ALTCOD;
		//alert(gdata[i].ALTFEF); 
		document.form.altfef.value=formatDate(gdata[i].ALTFEF,'aaaa-mm-dd','dd.mm.aaaa' );
		document.form.altfvf.value=formatDate(gdata[i].ALTFVF,'aaaa-mm-dd','dd.mm.aaaa' );
		document.form.altfra.value=formatDate(gdata[i].ALTFRA,'aaaa-mm-dd','dd.mm.aaaa' );
		
	};
	$("#aftsav").hide(1);
}

/*
jDavila
21/03/13
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "loteseditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
22/03/13
*/
function eliminar(art, cod ) {
	if (confirm('Seguro que desea borrar el lote' + cod + ' del articulo ' + art + '?'))
	{
		var param = [];
		param['aarcod']=art;
		param['altcod']=cod;
		ejecutasqld("loteseliminar.php",param);
		location.reload();
	}
}

function cerrarpopup(){
	window.close();
	combo_lote2(document.getElementById('aarcod').value);
}

function choiceart(targ,selObj,restore,e){ //v3.0
	dat = selObj.options[selObj.selectedIndex].value;
	var datos = dat.split("@");
	/*document.getElementById("aarumb").value=datos[1];*/
	//var nro = document.getElementById('atrnum').value;
	//var tran = document.getElementById('atrcod').value;
	//combo_lote_trans(datos[0],nro, tran);
	//alert(datos[0]);
	combo_loterela(datos[0]);
	//alert(document.getElementById("aarumb").value + "-" +datos[1]);
	//activaimagengrabar(1);
}

function combo_loterela(art)
{
	var param = [];
	param['art']=art;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarloterela.php',
		beforeSend:function(){
			$('#div_altltr').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#div_altltr').html(html);
		}
	});
}