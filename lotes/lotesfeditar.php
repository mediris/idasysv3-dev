<?php 
/*
* jdavila 
* 16/03/2012
*/
session_start();
include("../conectar.php");
include("../JSON.php");
$aarcod=$_REQUEST["art"];
$altcod=$_REQUEST["cod"];
$loterl=$_REQUEST["loterl"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>editar tipo inventario</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="../calendario/javascripts.js"></script>
<script language="JavaScript" src="javascript.js"></script>
<script language="JavaScript" src="../javascript/jquery.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:editar('<?php echo $aarcod;?>', '<?php echo $altcod;?>')">

<div id="aediv" class="white_content">

        <form id="form" name="form" method="post" action="" class="form">
                <table width="100%"  border="0">
                 <tr>
                    <th width="20%" scope="col"><label>C�digo Art�culo</label></th>
                    <th width="39%" scope="col">
                    	<input type="hidden" name="aarcod" id="aarcod" />
	                    <div align="left" id="wsaarcod"></div>
                    </th>    
                    <th colspan="2" id="erraarcod" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th width="20%" scope="col"><label>Cod. Lote</label></th>
                    <th width="39%" scope="col">
						<input name="altcod" type="hidden" id="altcod"  />
                    	<div align="left" id="wsaltcod"></div>
                    </th>    
                    <th colspan="2" id="erraltcod" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>

                  <tr>
                    <th width="20%" scope="col"><label>Lote Relacionado</label></th>
                    <th width="39%" scope="col">
                    <div align="left" id="saltltr">
                        <select name="altltr" id="altltr">
                          <?php 
                            echo '<option value="" >Eliminar Lote Relacionado</option>';
                            $sql = "SELECT T1.ALTCOD, T1.ALTLTR, T2.AARDES, T2.AARCOD 
                        FROM IV48FP T1
                          INNER JOIN IV05FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AARCOD=T2.AARCOD)   
                          WHERE T1.ACICOD ='$Compania' AND T1.ALTLTR = '' AND T1.AARCOD = '$aarcod'
                          ORDER BY T2.AARDES";
                          $result1=odbc_exec($cid,$sql)or die(exit("Error en odbc_exec 11111")); 
                          while(odbc_fetch_row($result1)){      
                            $altcod=trim(odbc_result($result1,1));
                            $altltr=trim(odbc_result($result1,2));
                            $aardes=trim(odbc_result($result1,3));  
                            $aarcod=trim(odbc_result($result1,4));

                            if($loterl==$altcod)
                              $selec='selected="selected"';
                            else
                              $selec='';

                            echo '<option value='.$altcod.' '.$selec.'>'.$altcod.' ('.$aardes.' -'.$aarcod.')</option>';
                           }
                          ?>
                        </select>
                      </div>
                    </th>    
                    <th colspan="2" id="erraltltr" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th width="20%" scope="col"><label>Fecha Emisi�n Proveedor</label></th>
                    <th width="39%" scope="col"><div align="left">
                      <?php  echo escribe_formulario_fecha_vacio('altfef','form',date('d.m.Y')); ?>
                    </div></th>    
                    <th colspan="2" id="erraltfef" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th width="20%" scope="col"><label>Fecha Vencimiento Proveedor</label></th>
                    <th width="39%" scope="col"><div align="left">
                      <?php  echo escribe_formulario_fecha_vacio('altfvf','form',date('d.m.Y')); ?>
                    </div></th>    
                    <th colspan="2" id="erraltfvf" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                  
                  <tr>
                    <th width="20%" scope="col"><label>Fecha Pre - Vencimiento</label></th>
                    <th width="39%" scope="col"><div align="left">
                      <?php  echo escribe_formulario_fecha_vacio('altfra','form',date('d.m.Y')); ?>
                    </div></th>    
                    <th colspan="2" id="erraltfra" class="Estilo5" scope="col">&nbsp;</th>
                  </tr>
                     
                  <tr>
                    <th scope="col">&nbsp;</th>
                    <th scope="col"></th>
                    <!--<th width="23%"  scope="col"><p onclick="marcaseditarcerrar()" class="subir" align="right">salir</p></th>
                    <th width="21%"  scope="col"><p align="right" class="subir" onclick="marcaseditarvalidar()">Grabar</p></th>-->
    				<th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
	                <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="editarvalidar()" /></th>
                  </tr>
                </table>
    	</form>
</div>

<div align="center" id="aftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>