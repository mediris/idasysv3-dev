<?php 
session_start();
include_once("conectar.php");
$dir  = 'D:/consultas_INVAP_correos/Reporte_INVAP_'.formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').'.pdf';
$name = "Reporte_INVAP_".formatDate($Fechaactual,'dd.mm.aaa','aaaa_mm_dd').'.pdf';
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

//require 'PHPMailer 5.2.10/PHPMailerAutoload.php';
include( 'PHPMailer 5.2.10/class.phpmailer.php' );
include( 'PHPMailer 5.2.10/class.smtp.php' );

//Create a new PHPMailer instance
$mail = new PHPMailer;

//Tell PHPMailer to use SMTP
$mail->isSMTP();

//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 2;

//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';

//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';

//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;

//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';

//Whether to use SMTP authentication
$mail->SMTPAuth = true;

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "idasys.inventario@meditron.com.ve";

//Password to use for SMTP authentication
$mail->Password = "soporte.2015";

//Set who the message is to be sent from
$mail->setFrom('idasys.inventario@meditron.com.ve', 'IDASYS Inventario');

//Set an alternative reply-to address
//$mail->addReplyTo('o.tamiche@meditron.com.ve', 'Oraidee Tamiche');

//Set who the message is to be sent to
$mail->addAddress('g.caldoni@meditron.com.ve', 'Giancarlo Caldoni');
$mail->addAddress('j.alarcon@meditron.com.ve', 'Jesus Alarcon');
$mail->addAddress('a.cardenas@meditron.com.ve', 'Arturo Cardenas');
$mail->addAddress('l.porras@meditron.com.ve', 'Liborio Porras');
$mail->addAddress('a.porras@meditron.com.ve', 'Amable Porras');
$mail->addAddress('a.orlando@meditron.com.ve', 'Antonio Orlando');
$mail->addAddress('v.gonzalez@meditron.com.ve', 'Vilma Gonzalez');

//$mail->addCC('g.caldoni@meditron.com.ve', 'Giancarlo Caldoni');
$mail->addCC('c.marquez@meditron.com.ve', 'Carlos Marquez');
$mail->addCC('a.tirado@meditron.com.ve', 'Audrey Tirado');
$mail->addCC('s.marquez@meditron.com.ve', 'Simon Marquez');
$mail->addCC('m.loreto@meditron.com.ve', 'Margi Loreto');
$mail->addCC('almacen3@meditron.com.ve', 'Samuel Vega');
$mail->addCC('r.causa@meditron.com.ve', 'Rossana Causa');
//$mail->addCC('o.tamiche@meditron.com.ve', 'Oraidee Tamiche');

//$mail->addCC('s.marquez@meditron.com.ve', 'Simon Marquez');
//$mail->addCC('a.tirado@meditron.com.ve', 'Audrey Tirado');

$mail->addBCC('j.davila@meditron.com.ve', 'Johan Davila');


//Set the subject line
$mail->Subject = 'REPORTE DE ENTRADA Y SALIDA INVAP';

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));

//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';

$body  = "MENSAJE AUTOMATICO ENVIADO POR EL SISTEMA IDASYS<BR /><BR />";
$body.="Se anexa Reporte de Entradas y Salidas de Repuestos y Equipos<BR/>";
$body.="<strong>ARCHIVO</strong>: ".$name."<BR/>";

$body.="<BR/><BR/><BR/>
Soporte:<br />
Gerencia de Tecnologia de Informaci&oacute;n<br />
Departamento de Desarrollo<br />
Tlf.:  (0212) 240.0007<br />
Email: <a href=\"mailto:secretaria.ti@meditron.com.ve\">secretaria.ti@meditron.com.ve</a><br />";

// Añadimos el contenido al mail creado 
$mail->msgHTML($body);
//$mail->Body = ($body);

//Attach an image file
$mail->addAttachment($dir);

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
	unlink($dir);
}
