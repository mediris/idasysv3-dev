<?php 
/*
* jdavila 
* 15/02/2012
*/
session_start();
include("../conectar.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="sinhead">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>agregar status</title>
<link href="../<?php echo $css; ?>" rel="stylesheet" type="text/css" media="screen" />
<script language="JavaScript" src="../javascript/jquery.js"></script>
<script language="JavaScript" src="../javascript/javascript.js"></script>
<script language="JavaScript" src="javascript.js"></script>
</head>
<body class="sinbody" bgcolor="#FFFFFF" onload="javascript:agregar()">

<div id="agregardiv" class="white_content">

        <form id="agregarform" name="agregarform" method="post" action="" class="form">
               
                <table width="100%"  border="0">
                    <tr id="agregarc">
                        <th  scope="col"><label>Campo de Status</label></th>
                        <th width="36%" scope="col"><div align="left"><input name="astfld" type="text" id="astfld" size="10" maxlength="10" ></div></th>
                        <th colspan="2" id="errastfld" class="Estilo5" scope="col">&nbsp;</th>
                    </tr>
                    <tr id="agregarc1">
                        <th  scope="col"><label>Valor</label></th>
                        <th width="36%" scope="col"><div align="left"><input name="astcod" type="text" id="astcod" size="2" maxlength="2" onKeyPress="return Tecla(event, this);"></div></th>
                        <th colspan="2" id="errastcod" class="Estilo5" scope="col">&nbsp;</th>
                    </tr>
                    <tr>
                        <th scope="col"><label>Descripción Larga</label></th>
                        <th scope="col"><div align="left"><input name="astdes" id="astdes" type="text" size="40" maxlength="40"></input></div></th>
                        <th colspan="2" id="errastdes" class="Estilo5" scope="col">&nbsp;</th>
                    </tr>
                    <tr>
                        <th scope="col"><label>Descripción Corta</label></th>
                        <th scope="col"><div align="left"><input name="astdcr" type="text" id="astdcr" size="10" maxlength="10" ></div></th>
                        <th colspan="2" id="errastdcr" class="Estilo5" scope="col">&nbsp;</th>
                    </tr>
                    <tr>
                        <th scope="col">&nbsp;</th>
                        <th scope="col"></th>
                        <th width="23%"  scope="col"><input name="cancelar" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"  value="Salir" id="cancelar"></th>
				        <th width="21%"  scope="col"><input type="button" name="submit" id="submit" value="Grabar" onClick="agregarvalidar()" /></th>
                    </tr>
                </table>
    	</form>
</div>
<div align="center" id="agregaraftsav">
  <br />
  <br />
  <br />
  <br />
  <br />
  <br />
  Registro Agregado con exito !
  <br />
  <br />
  <br />
  <input name="Submit3" type="button" onClick="parent.location.reload();parent.Shadowbox.close();"" " value="Cerrar"></div>
</body>
</html>