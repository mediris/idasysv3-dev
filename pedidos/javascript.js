
function agregar() {
	document.agregarform.arqobs.value="";
}

function agregarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";
	var indice1 = document.agregarform.arqtip.selectedIndex;
	var val1 = document.agregarform.arqtip.options[indice1].value;
		
	capturararticulo(val1);

	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "pedidoagregarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo){
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';		
		}
		if(vdata[i].ARQNRO){
			document.agregarform.arqnro.value=vdata[i].ARQNRO;
			document.solicitudes.arqnro.value=vdata[i].ARQNRO;
			
			//DESCRIPCION DE UNIDADES SOLICITANTES
			document.solicitudes.auncod.value=vdata[i].AUNCOD;
			//document.solicitudes.aundes.value=vdata[i].AUNDES;
			document.solicitudes.auncod2.value=vdata[i].AUNCOD2;
			//document.solicitudes.aundes2.value=vdata[i].AUNDES2;
			document.getElementById("ord2").innerHTML=vdata[i].AUNDES;
			document.getElementById("ord3").innerHTML=vdata[i].AUNDES2;
		}
	}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
		if(nodetalle=="S"){
			finalizarreq();}
		else{
    		var indice1 = document.agregarform.arqtip.selectedIndex;
			
    		var texto1 = document.agregarform.arqtip.options[indice1].text;
			
			$("#agregarc").css("display", "none");
			$("#reqst06fp").css("display", "block");
			document.getElementById("ord1").innerHTML=texto1;
			document.getElementById("ord4").innerHTML=document.agregarform.arqobs.value;

			
			
			document.getElementById("aarcod").focus();
			$("#ocultarform").css("display", "none");}//fin del else
	}
}//fin del primer if

function choiceart(targ,selObj,restore,e){ //v3.0
	dat = selObj.options[selObj.selectedIndex].value;
	var datos = dat.split("@");
	//document.getElementById("aarumb").value=datos[1];
	combo_uni(datos[0], datos[1]);
	activaimagengrabar(1);
}

function capturararticulo(code)
{
	var temp = document.getElementById('auncod2').value;//UNIDAD DESPACHA
	$.get ("buscararticulo.php", { code: code, auncod2: temp },function(resultado)
		{
			if(resultado == false)
			{
				alert("No existe ningun articulo para tipo de requisición");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
		}
	);
}

function cargararticulo(code, alm)
{
	
	$.get ("buscararticulodespacho.php", { code: code , alm: alm  },function(resultado)
		{
			// alert(resultado);
			if(resultado == false)
			{
				alert("No existe ningun articulo para tipo de requisición");
				document.getElementById("aarcod").value=0;
				$("#aarcod").attr("disabled",true);
			}
			else
			{
				$("#aarcod").attr("disabled",false);
				document.getElementById("aarcod").options.length=1;
				$('#aarcod').append(resultado);
			}
		}
	);
}

function finalizarreq(){
	$("#agregaraftsav").hide(1000);		
	parent.location.reload();
	parent.Shadowbox.close();
}	
function finalizarreq_art_ext(){
	//alert($('#arqnro').val());
	guardardespacho2($('#arqnro').val());
	$("#agregaraftsav").hide(1000);		
	parent.location.reload();
	parent.Shadowbox.close();
}	

function enviarpedido(nrord){
	if (confirm('Seguro que desea enviar a aprobaci\u00f3n el pedido ' + nrord + '?'))
	{
		var param = [];
		param['arqnro']=nrord;
		//ejecutasqld("enviarrequerimiento.php",param);
		ejecutasqlp("enviarrequerimiento.php",param);
		
		for(i in gdata){
			if(gdata[i].EJEC == 1)//bien
			{
				var param = [];
				param['arqnro']=nrord;
				
				var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
				OpenWindow = window.open("correo.php?&arqnro="+nrord, "enviodecorreo", windowprops);
		
				//ejecutasqla("correo.php",param);
		
				$("#agregaraftsav").hide(1000);		
				parent.location.reload();
				parent.Shadowbox.close();
			}else if(gdata[i].EJEC == 0)//mal
			{
				alert("No se puede enviar aprobaci\u00f3n del Pedido \n Se debe ingresar Detalle.");
			}
		}
	}	
}

function activaimagengrabar(num){ 
	document.getElementById("aslcan").focus();
	if ((document.getElementById("aslcan").value !==0) && (document.solicitudes.aarcod.selectedIndex!=='')) {
		document.getElementById("imagengrabar").style.display='block';}
	if ((document.getElementById("aslcan").value == 0) || (document.solicitudes.aarcod.selectedIndex=='')) {
		document.getElementById("imagengrabar").style.display='none'}
}

/*LOTES Añadido 06-10-2016*/
function activaimagengrabarlote(num){ 
	document.body.style.cursor="wait";
	document.getElementById("aslcan").focus();
	
	if ( (document.getElementById("aslcan").value !==0) ) {
		document.getElementById("imagengrabar").style.display='block';}
	if ( (document.getElementById("aslcan").value == 0) ) {
		document.getElementById("imagengrabar").style.display='none'}
	document.body.style.cursor="default";
}

function activaimagengrabar2(){ 
	document.body.style.cursor="wait";
	document.getElementById("aslcan").focus();
	
	if ((document.getElementById("aslcan").value !=0) ) {/*&& (document.solicitudes.atlcod.options[document.solicitudes.atlcod.selectedIndex].value!=='') && (document.solicitudes.aarumb.options[document.solicitudes.aarumb.selectedIndex].value!=='') */
		document.getElementById("imagengrabar").style.display='block';}
	if ((document.getElementById("aslcan").value ==0) ) {/*|| (document.solicitudes.atlcod.options[document.solicitudes.atlcod.selectedIndex].value=='') || (document.solicitudes.aarumb.options[document.solicitudes.aarumb.selectedIndex].value=='') */
		document.getElementById("imagengrabar").style.display='none'}
	document.body.style.cursor="default";
}
/* LOTES */ 

function vertransaccion(){
	var code = $("#atocod").val(); 
	//alert(code);
	var param = [];
	param['atocod']=code;
	ejecutasqlp("buscarinventariotransaccion.php",param);
	for(i in gdata){
		document.agregarform.atrcod.value=gdata[i].ATRCOD;
		//var str=document.requerimagregarform.atrcod.value;
		//alert(str.length);
	};
}

function editar(nrord) {
			var param = [];
			param['arqnro']=nrord;
			ejecutasqlp("informacionphp.php",param);
			for(i in gdata){
				document.agregarform.arqobs.value=gdata[i].ARQOBS;
				document.getElementById("nroreq").innerHTML=gdata[i].ARQNRO;
				document.agregarform.arqnro.value=gdata[i].ARQNRO;
				document.agregarform.arqdes.value=gdata[i].ATQDES;
				document.agregarform.arqtip.value=gdata[i].ARQTIP;
				
				document.agregarform.auncod.value=gdata[i].AUNCOD;//UNIDA SOLICITANTE
				//document.agregarform.aundes.value=gdata[i].AUNDES;//UNIDA SOLICITANTE
				//document.agregarform.arqtip.value=gdata[i].ARDALE;//ALMACEN SOLICITANTE
				
				//document.agregarform.arqtip.value=gdata[i].ARDALS;//ALMACEN DESPACHA
				document.agregarform.auncod2.value=gdata[i].UNICODS;//UNIDAD A DESPACHAR
				document.getElementById('aundes2').innerHTML=gdata[i].UNIDESS;//DES UNIDAD DESPACHAR
				
				if (gdata[i].AARCOD!='') {
	
					var value4 = gdata[i].AARCOD+'@'+gdata[i].AARUMB;
					var texto4 = gdata[i].AARDES;
					var cantidad = gdata[i].ARDCAN;
					//var texto5 = gdata[i].AARUMB;
					var texto5 = gdata[i].AUMDES;
					cadena = "<tr>";
					//cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
					cadena = cadena + "<td>" + texto4 + "</td>";
					cadena = cadena + "<td ><div align='right'>" + NumFormat(Number(cantidad),'2', '10', '.', '') + "</div></td>";
					cadena = cadena + "<td>" + texto5 + "</td>";
					//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
					//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
					
					cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'   href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
				  
					cadena =  cadena + "</tr>";
					$("#grilla").append(cadena);
	
				};
			};
}
function editarextr(nrord) {
			var param = [];
			param['arqnro']=nrord;
			ejecutasqlp("informacionextraphp.php",param);
			for(i in gdata){
				
				if(gdata[i].AARCOD) {				
					if (gdata[i].AARCOD.trim()!='') {
						var value4 = gdata[i].AARCOD+'@'+gdata[i].AARUMR;
						var texto4 = gdata[i].AARDES;
						var cantidad = gdata[i].ARDCAN;
						//var texto5 = gdata[i].AARUMB;
						var texto5 = gdata[i].AUMDES;
						cadena = "<tr>";
						//cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
						cadena = cadena + "<td>" + texto4 + "</td>";
						cadena = cadena + "<td ><div align='right'>" + NumFormat(Number(cantidad),'2', '10', '.', '') + "</div></td>";
						cadena = cadena + "<td>" + texto5 + "</td>";
						//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
						//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
						
						cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'   href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
					  
						cadena =  cadena + "</tr>";
						$("#grilla").append(cadena);
		
					};
				};
			};
}

function editarcerrar() {
	document.getElementById('agregardiv').style.display='none';
	document.getElementById('fade').style.display='none';
}

function editarvalidar(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	

	document.body.style.cursor="wait";

	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "pedidoeditarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
		document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
		sierror='S';		}
	}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
	if(nodetalle=="S"){
		finalizarreq();}
	else{
		var texto1 = document.agregarform.arqdes.value;
		var val1 = document.agregarform.arqtip.value;
		
		capturararticulo(val1);

		$("#reqst06fp").css("display", "block");
		document.getElementById("ord1").innerHTML=texto1;
		document.getElementById("ord3").innerHTML=document.agregarform.arqobs.value;
		document.solicitudes.arqnro.value=document.agregarform.arqnro.value;
		document.getElementById("aarcod").focus();
		$("#ocultarform").css("display", "none");}//fin del else
	}
}//fin del primer if


function editarvalidar2(){	
	var contenido= "";
	var elemento;
	var nodetalle= "";
	
	document.body.style.cursor="wait";
	alert('** entro **');
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "pedidoeditarvalidarphp.php",
			data: $("#agregarform").serialize(),
			success: function(datos){alert('**'+datos+'**');
				vdata = (datos);},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			}								
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
		document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
		sierror='S';		}
	}
	
	if(sierror=='N'){
	document.body.style.cursor="default";
	if(nodetalle=="S"){
		finalizarreq();}
	else{
		var texto1 = document.agregarform.arqdes.value;
		var val1 = document.agregarform.arqtip.value;
		capturararticulo(val1);

		$("#reqst06fp").css("display", "block");
		document.getElementById("ord1").innerHTML=texto1;
		document.getElementById("ord3").innerHTML=document.agregarform.arqobs.value;
		document.solicitudes.arqnro.value=document.agregarform.arqnro.value;
		document.getElementById("aarcod").focus();
		$("#ocultarform").css("display", "none");}//fin del else
	}
}//fin del primer if


function eliminar(nrord) {
	if (confirm('Seguro que desea borrar el pedido ' + nrord + '?')){
		var param = [];
		param['arqnro']=nrord;
		ejecutasqld("procesarrequerimeliminar.php",param);
		location.reload();
	}
}

//aprobar 
function aprobar(nrord) {
	if (confirm('Seguro que desea aprobar el pedido ' + nrord + '?'))
	{
		var param = [];
		param['arqnro']=nrord;
		ejecutasqld("aprobarrequerimiento.php",param);
		var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
		OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
		location.reload();
	}
}
//rechazar pedido 
function rechazar(nrord) {
	if (confirm('Seguro que desea procesar el pedido ' + nrord + ' como No Aprobado?'))
	{
		var param = [];
		param['arqnro']=nrord;
		ejecutasqld("rechazarrequerimiento.php",param);
		var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
		OpenWindow = window.open("correorechazar.php?&arqnro="+nrord, "enviodecorreo", windowprops);
		location.reload();
	}
}

//aprobar 
function aprobare(nrord) {
	var param = [];
	param['arqnro']=nrord;
	ejecutasqld("aprobarrequerimiento.php",param);
	var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=600,height=300";
	OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
}

//Despachar , rechazar orden de servicio pendiente
function procesardespacho(nrord,array) {
	if (confirm('Una vez despachado el requerimiento ' + nrord + ' no se podr\u00e1  hacer modificaciones. Desea continuar?')){
		var cant = 0;
		for(var i=0; i<array.length; i++){
			var valor = document.getElementById(array[i]).value.replace(' ', '');
			//alert("**"+valor+"**");
			if(valor == ""){
				cant++;
			}
		}
		//alert(cant);
		
		if(cant==0){
			var param = [];
			guardardespacho3(nrord);
			param['arqnro']=nrord;
			//ejecutasqlp("despacharrequerimiento.php",param);
			$.ajax({
				type: "POST",
				url: "despacharrequerimiento.php",
				data: convertToJson(param),
				success: function(data) {
					ejecutasqld("correo_art_ext.php",param);
					//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
					//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
					detalleimprimirpdf(nrord);
					window.opener.document.location.reload()
					window.close();
				},
				error: function(data){
					alert("error:"+data);
				}
			});
		}else{
			alert('La Cantidad a Despachar deben tener valor. (0 ~ *)');
		}		
	}
}


function recibirdespacho(nrord) {
if (confirm('Desea recibir el requerimiento ' + nrord + ' ?'))
{
		var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "recibirrequerimiento.php",
        data: $("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});						

			
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);
			detalleimprimirpdf(nrord);

			window.opener.document.location.reload()
			window.close();
}
}


function guardardespacho(nrord) {
		
		var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "guardarrequerimiento.php",
        data: $("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});	
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);

location.reload();
}

function guardardespacho2(nrord) {
		
		var param = [];
			$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "guardarrequerimiento.php",
        data: parent.$("#despachoform").serialize(),
        success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
});	
			
			//var windowprops = "toolbar=0,location=0,directories=0,status=0, " + "menubar=0,scrollbars=0,resizable=0,width=400,height=200";
//OpenWindow = window.open("correoaprobado.php?&arqnro="+nrord, "enviodecorreo", windowprops);

location.reload();
}
function guardardespacho3(nrord) {
		
		var param = [];
		$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "guardarrequerimiento.php",
			data: parent.$("#despachoform").serialize(),
			success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);
			}
		});	
}

function guardardespacholote(nrord) {
	
	document.body.style.cursor="wait";
	//alert("guardar lostes")	;
	var param = [];
	$.ajax({
		async: false,
		type: "POST",
		dataType: "JSON",
		url: "guardarrequerimiento.php",
		data: parent.$("#despachoform").serialize(),
		success: function(datos){//alert(datos);
			vdata = parseJSONx(datos);}							
	});	
	//alert(parent.window.document.despachoform.id);jdavila
	//var ardc = parent.window.document.getElementById('ardcdsb[CHP0011]');	
	
	/* cargo informacion de articulos y guardo en campos hidden */
	
	var param = [];
	param['arqnro']=nrord;
	ejecutasqlp("informacionphp.php",param);
	var form = parent.document.despachoform;
	for(i in gdata){
		if (gdata[i].AARCOD) {
			for(j=0; j<form.length; j++)
			{
				if(form[j].name=='ardcdsb['+gdata[i].AARCOD+']')
				{
					//alert(j+"-"+form[j].name+"-"+form[j].value+"*-"+gdata[i].ARDCDS);
					form[j].value=gdata[i].ARDCDS;
				}
			}
		};
	};
	document.body.style.cursor="default";
}


//Requerimiento grabar en is05fp

			function fn_cantidad(){
				cantidad = $("#grilla tbody").find("tr").length;
				$("#span_cantidad").html(cantidad);
			};
            
            function grabardetallereq(){
				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
                cadena = "<tr>";
                //cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
				cadena = cadena + "<td>" + texto4 + "</td>";
                cadena = cadena + "<td> <div align='right'>" + NumFormat(Number($("#aslcan").val()),'2', '10', '.', '') + "</div></td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
				//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
				//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
				
                cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'   href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
              
				cadena =  cadena + "</tr>";
				
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardar_is05fp.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){
						//alert(datos);
						vdata = parseJSONx(datos);
					}
				});			
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
					alert("Producto ya solicitado en el pedido")
				}else{
					$("#grilla").append(cadena);
					//alert("Art\u00edculo agregado");
				}
				document.getElementById("aarcod").value="";
				document.getElementById("aslcan").value="";
				document.getElementById("aarumb").value="";
				combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
				activaimagengrabar();
				document.getElementById("aarcod").focus();
            };
            
            function fn_dar_eliminar(cod,obj){
                $("a.elimina").click(function(){
					hcd = document.solicitudes.arqnro.value;
                    id = $(this).parents("tr").find("td").eq(0).html();
                    respuesta = confirm("Desea eliminar el Art\u00edculo: " + id);
                    if (respuesta){//alert($(this));
                        //$(this).parents("tr").fadeOut("normal", function(){
							$(this).parents("#grilla tr").remove();
                            var param = [];
							param['arqnro']=hcd;
							param['aarcod']=this.id;//param['aarcod']=cod;
							$.ajax({
								async: false,
								type: "POST",
								dataType: "JSON",
								url: "eliminar_is05fp.php",
								data: convertToJson(param),
								success: function(datos){//alert(datos);
									eliminardetalleextra(param['aarcod']);
									//alert("Art\u00edculo " + id + " eliminado")
								}							
							});
                    }
                });
            };
						
			function grabardetalleextra(hcd,cod,can,umb){
				
				var param = [];
				param['arqnro']=hcd;
				param['aarcod']=cod;
				param['aslcan']=can;
				param['aarumb']=umb;
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardar_is18fp.php",
					//data: $("#solicitudes").serialize(),
					data: convertToJson(param),
					success: function(datos){//alert(datos);
						vdata = parseJSONx(datos);
					}							
				});	
						
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
					alert("se produjo un error");
				}else{
					//alert("Art\u00edculo agregado");
				}
            };
			
			function eliminardetalleextra(cod){
				hcd = document.solicitudes.arqnro.value;
				var param = [];
				param['arqnro']=hcd;
				param['aarcod']=cod;
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "eliminar_is18fp.php",
					data: convertToJson(param),
					success: function(datos){
						//alert(datos);
						//alert("Art\u00edculo eliminado");
					}							
				});
            };
			
			function agregardetalleextra()
			{
				var hcd = $( '#arqnro' ).val();
				var cod = $( '#aarcod' ).val();
				var can = $( '#aslcan' ).val();
				var umb = $( '#aarumb' ).val();
				//grabardetallereq();
				/**/
				var indice4 = document.solicitudes.aarcod.selectedIndex;
    			var texto4 = document.solicitudes.aarcod.options[indice4].text;
				var value4 = document.solicitudes.aarcod.options[indice4].value;
    			//var texto5 = document.solicitudes.aarumb.value;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
    			var texto5 = document.solicitudes.aarumb.options[indice5].text;
				var indice5 = document.solicitudes.aarumb.selectedIndex;
                cadena = "<tr>";
                //cadena = cadena + "<td>" + $("#adpcod2").val() + "</td>";
				cadena = cadena + "<td>" + texto4 + "</td>";
                cadena = cadena + "<td> <div align='right'>" + $("#aslcan").val() + "</div></td>";
                cadena = cadena + "<td>" + texto5 + "</td>";
				//cadena = cadena + "<td>" + $("#atrfac").val() + "</td>";
				//cadena = cadena + "<td>" + $("#aslcos").val() + "</td>";
				
                cadena = cadena + "<td><a class='elimina'  id='"+value4+"' name='"+value4+"'  href='javascript:fn_dar_eliminar(\"" + value4 + "\",this);'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
              
				cadena =  cadena + "</tr>";
				
				$.ajax({
					async: false,
					type: "POST",
					dataType: "JSON",
					url: "guardar_is05fp.php",
					data: $("#solicitudes").serialize(),
					success: function(datos){
						//alert(datos);
						vdata = parseJSONx(datos);
					}
				});			
                sierror='N';
				for(i in vdata)	{
					if(vdata[i].campo) 	{
						//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
						sierror='S';
					}
				}
												
				if (sierror=='S')	{
					alert("Producto ya solicitado en el pedido")
				}else{
					$("#grilla").append(cadena);
					grabardetalleextra(hcd,cod,can,umb);
					//alert("Art\u00edculo agregado");
				}
				document.getElementById("aarcod").value="";
				document.getElementById("aslcan").value="";
				document.getElementById("aarumb").value="";
				combo_uni(document.getElementById("aarcod").value, document.getElementById("aarumb").value);
				activaimagengrabar();
				document.getElementById("aarcod").focus();
				/**/
				
				//grabardetalleextra(hcd,cod,can,umb);
				
			};
			
function requerimverdetalle(num) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	
	var ventana="requerimverdetalle.php?&num="+num;
	
	window.open(ventana,"", windowprops); // opens remote control

}

function requerimdespachar(num) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	
	var ventana="requerimdespachardetalle.php?&num="+num;
	
	window.open(ventana,"", windowprops); // opens remote control

}
function requerimrecibir(num, sts) {
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	if (sts=='05')	{var ventana="requerimrecibirdetalle.php?&num="+num;}
	else			{var ventana="requerimrecibirdetalle2.php?&num="+num;}
	
	
	window.open(ventana,"", windowprops); // opens remote control

}

function esenter(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else if (e) tecla = e.which;
	else return false;
	if (tecla == 13)   	{
	if ((document.getElementById("aslcan").value !=0) && (document.solicitudes.aarcod.selectedIndex!='')) {
	grabardetallereq(); 	}}
	else   				{return false;  }

}
function esenter1(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else 			if (e) {tecla = e.which;}
	else 			return false;
	if (tecla == 13)   	{return true; 	}
	else   				{return false;  }

}
function esenter2(e) {
	var tecla;
	if (window.event) {tecla = window.event.keyCode;}
	else 			if (e) {tecla = e.which;}
	else 			return false;
	//lert(tecla);
	if (tecla == 13)   	{return false; 	}
	else   				{return true;  }

}
function nada() {
}

function validarcantrec(candes, observ,cantrec) {alert("Debe llenar el campo de observaci\u00f3n, colocando motivo");
        if(cantrec.value != candes)	{	document.getElementById(observ).disabled=false;
										document.getElementById(observ).focus();		}								
		else 						{	document.getElementById(observ).disabled=true;	}
}
function validarobserv(observ) {
        if (observ.value=="")  {		observ.style.border = "1px solid #F00";
										alert("Se debe colocar una observaci\u00f3 n v\u00e1lida");                               
										observ.focus();}
}
function validarobservon(observ) {
        if (observ.value=="")  {		observ.style.border = "1px solid #F00";
										alert("Se debe colocar una observaci\u00f3 n v\u00e1lida");                               
										observ.focus();}
}

/*
 * @jDavila
 *13/04/2012
 */
function combo_uni(art, medi)
{
	var param = [];
	param['medi']=medi;
	param['art']=art;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarunimedida.php',
		beforeSend:function(){
			$('#div_aarnum').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#div_aarnum').html(html);
		}
	});
}

function detalleimprimirpdf(num)
{
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	//var ventana="exportarapdf.php?&num="+num;//ORIGINAL
	var ventana="exportarapdf2.php?&num="+num;//SGUNDARIO
	window.open(ventana,"", windowprops); // opens remote control
}

function pdf_pakinglist(num)
{
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=950,height=800";
	var ventana="exportapdf_despacho.php?&num="+num;//solo despacho 
	window.open(ventana,"", windowprops); // opens remote control
}

/*
 * @jdavila
 * 22/10/2013
 * 24/01/2014
 */
 
 function buscarexistencia(aarcod, aalcod, guardada)
 {

	document.body.style.cursor="wait";
	//var aalcod = document.despachoform.aalcod.options[document.despachoform.aalcod.selectedIndex].value;
	var can = (guardada.value);//cantidad existente en el despacho
	
	var param = [];
	param['aalcod']=aalcod;
	//param['aarcod']=document.getElementById("aarcod").value;
	param['aarcod']=aarcod;
	ejecutasqlp("buscarexistenciaart.php",param);
	
	for(i in gdata)
	{
		if(gdata[i].EJEC != 0)//bien
		{
			var tem =0;
			if(!isNaN(can))tem = parseFloat(gdata[i].CANT);
			if(isNaN(can))tem = parseFloat(gdata[i].CANT)+parseFloat(can);
			document.getElementById('ardcdse['+aarcod+']').value=tem;
			document.body.style.cursor="default";
			document.getElementById('canexalmpri['+aarcod+']').value=tem;
		}
		else if(gdata[i].EJEC == 0)//mal
		{
			document.getElementById('canexalmpri['+aarcod+']').value=0;
			document.getElementById('ardcdse['+aarcod+']').value=0;
			document.body.style.cursor="default";
		}
	}
 }

/*LOTES AGREGADO 06-10-2016*/
 function cargar(arqnro, aarcod, aarumb, cantid, aalcod)
{
	
	var param = [];
	param['arqnro']=arqnro;
	param['aarcod']=aarcod;
	param['aarumb']=aarumb;
	param['cantid']=cantid;
	param['aalcod']=aalcod;
	document.body.style.cursor="wait";
	
	ejecutasqlp("lotesinformacionphp.php",param);
	for(i in gdata){
		//alert(gdata[i].AARCOD);
		if (gdata[i].AARCOD) {
			var value6 = gdata[i].ALTCOD;
			var cantidad = gdata[i].ARDCDS;
			var can = cantidad!=''? NumFormat(cantidad, '2', '10', '.', ','):'';

			cadena = "<tr name=\"tr"+value6+"\" id=\"tr"+value6+"\">";
			cadena = cadena + "<td><div align='right'>" +  can + "</div></td>";
			cadena = cadena + "<td>" + value6 + "</td>";
			cadena = cadena + "<td><a class='elimina' href='javascript:fn_dar_eliminar_lote(\"" + aarcod + "\", \"" + value6 + "\");'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
			cadena = cadena + "</tr>";
			$("#grilla").append(cadena);
		};
	};
	
	combo_lote(aarcod,aalcod);
	actualizarcantidad(arqnro, aarcod);
	//combo_uni(aarcod, aarumb);
	document.body.style.cursor="default";
}

function grabardetallereqlote(){
	document.body.style.cursor="wait";
	var value4 = document.getElementById('aarcod').value;
	//var texto5 = document.solicitudes.aarumb.value;
	var value5  = document.getElementById('aarumb').value;
	var text5   = document.getElementById('aarumbdes').value;
	
	var indice6 = document.solicitudes.atlcod.selectedIndex;
	var texto6  = document.solicitudes.atlcod.options[indice6].text;
	var value6  = document.solicitudes.atlcod.options[indice6].value;
	var tes = texto6.split('-');

	if( (value6!='') )
	{
		var can = $("#aslcan").val();
		var tem = tes[1].substring(0,(tes[1].length -1));
		//alert(can+"-"+tem);
		if( Number(can)>Number(tem) )
		{
			alert("No puede despachar una cantidad superiro a la disponible en el lote");
			document.getElementById("aslcan").value='';
			document.getElementById("aslcan").focus();
		}
		else
		{
			cadena = "<tr name=\"tr"+value6+"\" id=\"tr"+value6+"\">";
			var can = $("#aslcan").val()!=''?NumFormat($("#aslcan").val(), '2', '10', '.', ','):'';
			cadena = cadena + "<td> <div align='right'>" + can + "</div></td>";
			cadena = cadena + "<td>" + value6 + "</td>";
			cadena = cadena + "<td><a class='elimina' href='javascript:fn_dar_eliminar_lote(\"" + value4 + "\", \"" + value6 + "\");'><img src='../images/eliminar.gif' alt='Eliminar' width='15' height='15' border='0'></a></td>";
			cadena = cadena + "</tr>";
			
			$.ajax({
				async: false,
				type: "POST",
				dataType: "JSON",
				url: "guardar_is17fp.php",
				data: $("#solicitudes").serialize(),
				success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);}							
			});			
			sierror='N';
			for(i in vdata)	{
				if(vdata[i].campo) 	{
					//document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
					sierror='S';
				}
			}
			
			if (sierror=='S')	{
				alert("Producto ya solicitado en la entrega.")}								
			else				{
				$("#grilla").append(cadena);
				//alert("Articulo agregado");
			}
			//actualizarcantidad(value4);
			document.getElementById("aslcan").value="";
			var alm = document.getElementById('aalcod').value
			combo_lote(value4,alm);
			activaimagengrabar();
			document.getElementById("atlcod").focus();
		}
	}
	else
	{
		if(value6 == ''){alert('Debe seleccionar el Lote a trabajar.');document.getElementById("atlcod").focus();}
	}
	
	actualizarcantidad(document.getElementById('arqnro').value, value4);	
	document.body.style.cursor="default";
};

function actualizarcantidad(arqnro, aarcod){
	document.body.style.cursor="wait";
	var param = [];
	param['arqnro']=arqnro;
	param['aarcod']=aarcod;
	ejecutasqlp("lotesinformacionphp.php",param);
	var canre = 0;
	var cande = 0;
	for(i in gdata){
		if (gdata[i].AARCOD) {
			//canre = gdata[i].ARDCAN;
			cande = Number(cande) + Number(gdata[i].ARDCDS);
		};
	};
	var nombre ='ardcds['+aarcod+']';
	parent.document.getElementById(nombre).value = cande;
	
	var cadena = 'Asignadas '+cande+' de '+$("#cantid").val();
	$("#div_total").html(cadena);
	guardardespacholote(arqnro);
	document.body.style.cursor="default";
};


function fn_dar_eliminar_lote(cod, lot){
	respuesta = confirm("Desea eliminar los Articulo del lote " + lot);
	hcd = document.solicitudes.arqnro.value;
	if (respuesta){
		document.body.style.cursor="wait";
		var param = [];
		param['arqnro']=hcd;
		param['aarcod']=cod;
		param['atlcod']=lot;
		$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "eliminar_is17fp.php",
			data: convertToJson(param),
			success: function(datos){//alert(datos);
				vdata = parseJSONx(datos);
				nombre = "#tr"+lot;
				$(nombre).remove();
			}							
		});	
		sierror='N';
		for(i in vdata)	{
			if(vdata[i].campo) 	{
				sierror='S';
			}
		}
	
		if (sierror=='S')	{
			document.body.style.cursor="default";
			alert("Se Presento un error al momento de eliminar el producto.\nSe Recargara la pagina.");
			parent.location.reload();
		}
		else				{
			document.getElementById("aslcan").value="";
			var alm = document.getElementById('aalcod').value;
			combo_lote(cod,alm);
			document.getElementById("atlcod").focus();
			document.body.style.cursor="default";
			alert("Los Articulos del lote " + lot + " se han eliminado."+cod);
		}
		actualizarcantidad(document.solicitudes.arqnro.value, cod);
		document.body.style.cursor="default";
	}
};

/* LOTES */
 
 function validarcantidadexist(canti, exist, guard)
 {
	/*cant  = NumFormat(Number(canti.value),'2', '10', '.', '');
	exist = NumFormat(Number(exist),'2', '10', '.', '')+cant;
	*/
	/*
	cant  = NumFormat(parseFloat(canti.value),'2', '10', '.', '');
	exist = NumFormat(parseFloat(exist),'2', '10', '.', '');
	guar  = NumFormat(parseFloat(guard),'2', '10', '.', '');
	*/

	/*NUEVO*/
	/*cant  = NumFormat(parseFloat(canti.value),'2', '10', '.', ',');
	exist = NumFormat(parseFloat(exist),'2', '10', '.', ',');
	guar  = NumFormat(parseFloat(guard),'2', '10', '.', ',');*/

	cant = parseInt(canti.value);
	exist = parseInt(exist);
	guar = parseInt(guard);
	/*NUEVO*/

	//if(isNaN(guar))exist = parseFloat(exist);
	//if(!isNaN(guar))exist = parseFloat(exist)+parseFloat(guar);
	
	if(cant>-1){
		if(cant>exist)
		{
			alert('La cantidad a despachas es mayor que \n la cantidad que posee en el almac\u00e9n.');
			canti.value=0;
			//canti.select();
		}
	}else{
		alert('No se pueden despachar cantidades negativas o el valor no es valido.');
		canti.value=0;
		
	}
	 canti.focus();
	 canti.select();
 }
 
function cargarparametros(obj,num)
{
	//alert(obj);
	//alert(num);
	//if(num=='')num=0;
	var param = [];
	var atrnum = num;
	var value = $('#'+obj.id).val();
	if (value==0){
		value = 99999999;
	}
	param['code']=value;
	param['num']=atrnum;
	$.ajax({
		async: false,
		type: 'POST',
		url: 'cargarparametros.php',
		beforeSend:function(){
			$('#parametros').html('<span class="input-notification attention png_bg">Cargando...</span>');
		},
		data: convertToJson(param),
		success: function(html){                
			$('#parametros').html(html);
		}
	});
}

function etiquetasPedidos(num, tipo){
	self.name = "main"; // names current window as "main"
	
	var windowprops ="toolbar=0,location=0,directories=0,status=0,"+"menubar=0,scrollbars=1,resizable=0,width=650,height=600";
	
	var ventana="../etiquetas2/exportpdf_pedidos.php?&num="+num+"&tipo="+tipo;

	
	window.open(ventana,"", windowprops); // opens remote control
}
