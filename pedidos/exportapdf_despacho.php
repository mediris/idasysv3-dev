<?php 
session_start();
include("../conectar.php");
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

$arqnro = trim($_GET["num"]);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Johan Davila');
$pdf->SetTitle('Paking List '.$arqnro.'');
$pdf->SetSubject('Paking List '.$arqnro.'');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', PDF_HEADER_STRING);
//$pdf->SetHeaderData('../../../images/logoidacadef20052.png', PDF_HEADER_LOGO_WIDTH, 'Despachar Pedido', 'R.I.F.: '.$Companiarif);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);



// ---------------------------------------------------------


			 $arqnro = trim($_GET["num"]);
			/*
			$sql2="SELECT T1.ARQNRO, T2.AUNCOD, T2.AUNDES, T1.AFESOL, T1.AHRSOL, T5.ATQDES, T1.ARQOBS, T1.AUSCOD, 
			 					T3.AUSAP1, T3.AUSAP2, T3.AUSNO1, T3.AUSNO2, T4.AEMMAI, T1.ARQSTS, T2.AUNUBI, T2.AUNTLF 
						 FROM is04fp t1, is01fp t2, is02fp t5, 
						 	mb20fp  t3 left join mb21fp t4 on (t3.auscod=t4.auscod and t4.aemprd='S')  
						 WHERE T1.ACICOD= T2.ACICOD and T1.AUNCOD= T2.AUNCOD and 
						 	   T1.AUSCOD= T3.AUSCOD and t1.arqtip= T5.ATQCOD and 
							   t1.arqnro=$arqnro and t1.acicod='$Compania'";
			*/
			// $sql2="SELECT T1.ARQNRO, T2.AUNCOD, T2.AUNDES, T1.AFESOL, T1.AHRSOL,     
			// 		T5.ATQDES, T1.ARQOBS, T1.AUSCOD, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1, 
			// 		T3.AUSNO2, T4.AEMMAI, T1.ARQSTS, T2.AUNUBI, T2.AUNTLF, T1.ARDALS, 
			// 		T6.AUNCOD as AUNCOD2, T6.AUNDES AS AUNDES2, T6.AUNUBI AS AUNUBI2
			// 	FROM IS04FP T1
			// 		INNER JOIN IS01FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AUNCOD=T2.AUNCOD)
			// 		INNER JOIN IS01FP T6 ON (T1.ACICOD=T6.ACICOD AND T1.ARDALS=T6.AALCOD)
			// 		INNER JOIN IS02FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.ARQTIP=T5.ATQCOD)
			// 		INNER JOIN MB20FP T3 ON (T1.AUSCOD=T3.AUSCOD)
			// 		LEFT JOIN MB21FP T4 ON (T3.AUSCOD=T4.AUSCOD AND T4.AEMPRD='S') 
			// WHERE t1.arqnro=$arqnro and t1.acicod='$Compania'";

			$sql2="SELECT T1.ARQNRO, T2.AUNCOD, T2.AUNDES, T2.AALCOD, T1.AFESOL, T1.AHRSOL,     
					T5.ATQDES, T1.ARQOBS, T1.AUSCOD, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1, 
					T3.AUSNO2, T4.AEMMAI, T1.ARQSTS, T2.AUNUBI, T2.AUNTLF, T1.ARDALS, 
					T6.AUNCOD as AUNCOD2, T6.AUNDES AS AUNDES2, T6.AUNUBI AS AUNUBI2
				FROM IS04FP T1
					INNER JOIN IS01FP T2 ON (T1.ACICOD=T2.ACICOD AND T1.AUNCOD=T2.AUNCOD)
					INNER JOIN IS01FP T6 ON (T1.ACICOD=T6.ACICOD AND T1.ARDALS=T6.AALCOD)
					INNER JOIN IS02FP T5 ON (T1.ACICOD=T5.ACICOD AND T1.ARQTIP=T5.ATQCOD)
					INNER JOIN MB20FP T3 ON (T1.AUSCOD=T3.AUSCOD)
					LEFT JOIN MB21FP T4 ON (T3.AUSCOD=T4.AUSCOD AND T4.AEMPRD='S') 
			WHERE t1.arqnro=$arqnro and t1.acicod='$Compania'";

			$result2=odbc_exec($cid,$sql2)or die(exit("Error en odbc_exec 11111"));
			$auncod=trim(odbc_result($result2,'AUNCOD'));			
			$auscod=trim(odbc_result($result2,'AUSCOD'));//usuario quien realizo la ord
			$atqdes=trim(odbc_result($result2,'ATQDES'));
			$aundes=trim(odbc_result($result2,'AUNDES'));
			$afesol=trim(odbc_result($result2,'AFESOL'));
			$ahrsol=trim(odbc_result($result2,'AHRSOL'));
			$arqobs=trim(odbc_result($result2,'ARQOBS'));
			$aemmai=trim(odbc_result($result2,'AEMMAI'));//destinatario
			$arqsts=trim(odbc_result($result2,'ARQSTS'));
			$aunubi=trim(odbc_result($result2,'AUNUBI'));//ubicacion unid. solicitante
			$auntlf=trim(odbc_result($result2,'AUNTLF'));//telefono unid. solicitante
			
			$auncod2=trim(odbc_result($result2,'AUNCOD2'));
			$aundes2=trim(odbc_result($result2,'AUNDES2'));
			$aunubi2=trim(odbc_result($result2,'AUNUBI2'));

			$almacen = trim(odbc_result($result2,'AALCOD'));
			
			$pdf->SetHeaderData('../../../images/logoidacadef20052.png', PDF_HEADER_LOGO_WIDTH, 'Packing List', 'R.I.F.: '.$Companiarif);
			// add a page
			$pdf->AddPage();
			// set font
			$pdf->SetFont('helvetica', '', 10);
			
			//detalle del Requerimiento			
			/*
			$sqla="SELECT t1.ArdCAN, t1.AARUMb, t2.AARDES,t1.aarcod, t1.ARDCDS, T1.ARDCRC , T1.ARDOBS , T1.ARDCR2 
				   FROM is05FP t1 
				   			left join IV05FP t2 on (t1.aarcod=t2.aarcod) 
				   WHERE t1.ACICOD='$Compania' and  t1.acicod=t2.acicod and t1.arqnro=$arqnro";
			*/
			/*
				SELECT T1.ArdCAN, T1.AARUMb, T2.AARDES, T1.AARCOD, T1.ARDCDS, T1.ARDCRC, T1.ARDOBS, T1.ARDCR2, 
				(CASE WHEN T3.AARCOD IS NOT NULL THEN 'N' ELSE 'S' END) as AAREXT, 
				( SELECT (CASE WHEN T4.ASLSAF IS NOT NULL THEN T4.ASLSAF ELSE 0 END - CASE WHEN T5.ASLCTR IS NOT NULL THEN T5.ASLCTR ELSE 0 END ) AS CANDIS                                                              
				 FROM IV40FP T4 LEFT JOIN IV41FP T5 ON (T4.ACICOD=T5.ACICOD AND T4.AALCOD=T5.AALCOD AND T4.AARCOD=T5.AARCOD) WHERE T4.ACICOD=T1.ACICOD and T4.AALCOD='0001' and T4.AARCOD=T1.AARCOD ORDER BY T4.ASLFEF desc FETCH FIRST 1 ROWS ONLY ) AS CANDIS 
				FROM is05FP T1 
				left join IV05FP T2 on (T1.AARCOD=T2.AARCOD) 
				left join IS18FP T3 on(T1.ACICOD=T3.ACICOD and T1.ARQNRO=T3.ARQNRO and T1.AARCOD=T3.AARCOD) 
				WHERE T1.ACICOD='14' and T1.ACICOD=T2.ACICOD and T1.ARQNRO=991
				ORDER BY T3.AARCOD, T2.AARDES
			*/


			// if($Compania == '14' && $auncod == '012'){
			// 	$auncod = '0007';
			// }

			// $sqla="SELECT T1.ArdCAN, T1.AARUMb, T2.AARDES, T1.AARCOD, T1.ARDCDS, T1.ARDCRC, T1.ARDOBS, T1.ARDCR2, 
			// 				(CASE WHEN T3.AARCOD IS NOT NULL THEN 'N' ELSE 'S' END) as AAREXT, 
			// 				( SELECT (CASE WHEN T4.ASLSAF IS NOT NULL THEN T4.ASLSAF ELSE 0 END - CASE WHEN T5.ASLCTR IS NOT NULL THEN T5.ASLCTR ELSE 0 END ) AS CANDIS                                                              
			// 				 FROM IV40FP T4 LEFT JOIN IV41FP T5 ON (T4.ACICOD=T5.ACICOD AND T4.AALCOD=T5.AALCOD AND T4.AARCOD=T5.AARCOD) WHERE T4.ACICOD=T1.ACICOD and T4.AALCOD='0001' and T4.AARCOD=T1.AARCOD ORDER BY T4.ASLFEF desc FETCH FIRST 1 ROWS ONLY ) AS CANDIS,
			// 				 (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD='".$Compania."' AND T6.AARCOD= T1.AARCOD AND T6.AALCOD='".$auncod."' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS CANSER 
			// 		FROM is05FP T1 
			// 			left join IV05FP T2 on (T1.AARCOD=T2.AARCOD) 
			// 			left join IS18FP T3 on(T1.ACICOD=T3.ACICOD and T1.ARQNRO=T3.ARQNRO and T1.AARCOD=T3.AARCOD) 
			// 		WHERE T1.ACICOD='".$Compania."' and T1.ACICOD=T2.ACICOD and T1.ARQNRO=0".$arqnro."
			// 		ORDER BY T3.AARCOD, T2.AARDES";

			/*$sqla="SELECT T1.ArdCAN, T1.AARUMb, T2.AARDES, T1.AARCOD, T1.ARDCDS, T1.ARDCRC, T1.ARDOBS, T1.ARDCR2, 
							(CASE WHEN T3.AARCOD IS NOT NULL THEN 'N' ELSE 'S' END) as AAREXT, 
							( SELECT (CASE WHEN T4.ASLSAF IS NOT NULL THEN T4.ASLSAF ELSE 0 END - CASE WHEN T5.ASLCTR IS NOT NULL THEN T5.ASLCTR ELSE 0 END ) AS CANDIS                                                              
							 FROM IV40FP T4 LEFT JOIN IV41FP T5 ON (T4.ACICOD=T5.ACICOD AND T4.AALCOD=T5.AALCOD AND T4.AARCOD=T5.AARCOD) WHERE T4.ACICOD=T1.ACICOD and T4.AALCOD='0001' and T4.AARCOD=T1.AARCOD ORDER BY T4.ASLFEF desc FETCH FIRST 1 ROWS ONLY ) AS CANDIS,
							 (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD='".$Compania."' AND T6.AARCOD= T1.AARCOD AND T6.AALCOD='".$almacen."' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS CANSER 
					FROM is05FP T1 
						left join IV05FP T2 on (T1.AARCOD=T2.AARCOD) 
						left join IS18FP T3 on(T1.ACICOD=T3.ACICOD and T1.ARQNRO=T3.ARQNRO and T1.AARCOD=T3.AARCOD) 
					WHERE T1.ACICOD='".$Compania."' and T1.ACICOD=T2.ACICOD and T1.ARQNRO=0".$arqnro."
					ORDER BY T3.AARCOD, T2.AARDES";
			$resulta=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));*/


			$sqla="SELECT T1.ArdCAN, T1.AARUMb, T2.AARDES, T1.AARCOD, T1.ARDCDS, T1.ARDCRC, T1.ARDOBS, T1.ARDCR2, 
							(CASE WHEN T3.AARCOD IS NOT NULL THEN 'N' ELSE 'S' END) as AAREXT, 
							( SELECT (CASE WHEN T4.ASLSAF IS NOT NULL THEN T4.ASLSAF ELSE 0 END - CASE WHEN T5.ASLCTR IS NOT NULL THEN T5.ASLCTR ELSE 0 END ) AS CANDIS                                                              
							 FROM IV40FP T4 LEFT JOIN IV41FP T5 ON (T4.ACICOD=T5.ACICOD AND T4.AALCOD=T5.AALCOD AND T4.AARCOD=T5.AARCOD) WHERE T4.ACICOD=T1.ACICOD and T4.AALCOD='0001' and T4.AARCOD=T1.AARCOD ORDER BY T4.ASLFEF desc FETCH FIRST 1 ROWS ONLY ) AS CANDIS,
							 (SELECT T6.ASLSAF FROM IV40FP T6 WHERE T6.ACICOD='".$Compania."' AND T6.AARCOD= T1.AARCOD AND T6.AALCOD='".$almacen."' ORDER BY T6.ASLFEF DESC FETCH FIRST 1 ROWS ONLY) AS CANSER 
					FROM is05FP T1 
						left join IV05FP T2 on (T1.AARCOD=T2.AARCOD) 
						left join IS18FP T3 on(T1.ACICOD=T3.ACICOD and T1.ARQNRO=T3.ARQNRO and T1.AARCOD=T3.AARCOD) 
					WHERE T1.ACICOD='".$Compania."' and T1.ACICOD=T2.ACICOD and T1.ARQNRO=0".$arqnro."
					ORDER BY T1.ARDSEC";
			$resulta=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));
								
			//movimientos de status		
			$sqla="SELECT ARQSTS, ASTFEC, ASTHOR, AUSCOD FROM is10fp WHERE ACICOD ='$Compania' and ARQNRO =$arqnro";
			$results=odbc_exec($cid,$sqla)or die(exit("Error en odbc_exec 11111"));
			
						
			//buscar el supervisor del tecnico
			$sqlb="SELECT T2.AUNSUP, T3.AUSAP1, T3.AUSAP2, T3.AUSNO1,  T3.AUSNO2, T4.AEMMAI 
			FROM is01fp t2, mb20fp t3 left join mb21fp t4 on (t3.auscod=t4.auscod and t4.aemprd='S') 
			WHERE T2.ACICOD = '".$Compania."' AND T2.AUNSUP= T3.AUSCOD and T2.AUNCOD = '$auncod'";
			$resultb=odbc_exec($cid,$sqlb)or die(exit("Error en odbc_exec 11111"));
			$aunsup=trim(odbc_result($resultb,1));
			$ausno1=trim(odbc_result($resultb,4));
			$ausap1=trim(odbc_result($resultb,2));
			$correosup=trim(odbc_result($resultb,6));
  		
			
$html.='<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
			<thead class="top">
				<tr>
					<td colspan="3">Tipo de Pedido: '.trim(utf8_encode($atqdes)).'&nbsp;&nbsp;&nbsp;N&uacute;mero:<strong>'.$arqnro.'</strong>
					</td>
					<td >
						Fecha: <strong>'.date('d.m.y').'</strong>
					</td>
				</tr>
				<tr >
					<!--<td align="left" valign="middle"><strong>Fecha / Hora:</strong></td>-->
					<!--<td align="left" valign="middle" colspan="2"><strong>Unidad Solicitante:</strong></td>-->
					<td align="left" valign="middle"><strong>Tel&eacute;fono</strong></td>
					<td align="left" valign="middle"><strong>Usuario:</strong></td>
					<td align="left" valign="middle"><strong>Supervisor:</strong></td>
					<td align="left" valign="middle"><strong>Status:</strong></td>
				</tr>
				<tr >
					<!--<td height="30" align="left" valign="middle">'.$afesol.' / '.$ahrsol.'</td>-->
					<!--<td align="left" valign="middle" colspan="2">'.trim(utf8_encode($aundes)).'</td>-->
					<td align="left" valign="middle">'.$auntlf.'</td>
					<td align="left" valign="middle"><div>'.trim(utf8_encode(usuario($auscod))).'</div></td>
					<td align="left" valign="middle"><div>'.trim(utf8_encode(usuario($aunsup))).'</div></td>
					<td align="left" valign="middle">'.utf8_encode(status('ARQSTS',$arqsts)).'</td>
				</tr>
				<tr>
					<td align="left" valign="middle"><strong>Unidad Solicitante:</strong></td>
					<td align="left" valign="middle">'.trim(utf8_encode($aundes)).'</td>
					
					<td align="left" valign="middle"><strong>Unidad Solicitante que Entrega:</strong></td>
					<td align="left" valign="middle">'.trim(utf8_encode($aundes2)).'</td>
					
				</tr>
				<tr>
					<td align="left" valign="middle"><strong>Direcci&oacute;n:</strong></td>
					<td align="left" valign="middle">'.trim(utf8_encode($aunubi)).'</td>
					
					<td align="left" valign="middle"><strong>Direcci&oacute;n:</strong></td>
					<td align="left" valign="middle">'.trim(utf8_encode($aunubi2)).'</td>
				</tr>
				<tr >
					<td align="left" valign="middle" colspan="4"><strong>Fecha Emisici&oacute;n Packing List: '.date('d/m/Y h:i:s ').'</strong></td>
				</tr>
				<tr>
					<td colspan="4"><hr /></td>
				</tr>
			</thead>
	
			<tbody class="detalle">
			<tr><td colspan="4">
				<table width="100%" cellpadding="0" cellspacing="3" border="0"> 
					<tr class="thead">';
				$html.='<td width="30%"><strong>Descripci&oacute;n</strong></td>';
				$html.='<td width="10%" align="right"><strong>Cant. Pedida</strong></td>';
				$html.='<td width="10%"><strong>Unidad de<br />Medida</strong></td>';
				$html.='<td width="10%"><strong>Exist. Alm.<br />Principal</strong></td>';
				
				$html.='<td width="10%"><strong>Exist.<br />Servicio</strong></td>';

				$html.='<td width="12%"><strong>Ubicaci&oacute;n</strong></td>';
				$html.='<td width="15%" align="center"><strong>Despachada</strong></td>';
			$html.='</tr>';
					$uno='N';
                    while(odbc_fetch_row($resulta))
					{
						$aarcod=trim(odbc_result($resulta,4));	
						if ($uno!='S') 	{	$campoinit='ardcds['.$aarcod.']';
											$uno='S';
										}
						$descripcion=odbc_result($resulta,3);
						$cantidad=number_format(odbc_result($resulta,1),2,",",".");
						$cantidaddes=number_format(odbc_result($resulta,5),2,",",".");
						$cantidadexistencia=number_format(odbc_result($resulta,'CANDIS'),2,",",".");
						$unidad=odbc_result($resulta,2);
						$canservicio=number_format(odbc_result($resulta,'CANSER'),2,",",".");
						$html.='<tr>';
						$html.='<td valign="top" >'.trim(utf8_encode($descripcion)).'<strong>('.trim($aarcod).')</strong></td>';
						$html.='<td valign="top" align="right">'.$cantidad.'&nbsp;&nbsp;</td>';
						$html.='<td valign="top" >'.trim(utf8_encode(unidad_medidad($unidad,$Compania, $cantidaddes))).'</td>';
						$html.='<td valign="top" align="right">'.$cantidadexistencia.'</td>';
						$html.='<td valign="top" align="right">'.$canservicio.'</td>';
						$html.='<td valign="top" align="right">'.list_ubiart($cid,$Compania,'0001',trim($aarcod),'2').'</td>';
						
						$html.='<td valign="bottom" align="center">________</td>';
						$html.='</tr>';
					}
$html.='	</table></td></tr>	
			<tr>
				<td colspan="4"><hr /></td>
			</tr>
			</tbody>
		<tfoot class="obj">
			<tr>
				<td align="right" valign="top"><strong>Observaciones :</strong></td>
				<td colspan="3">'.trim(utf8_encode($arqobs)).'<br /></td>
			</tr>
			<tr>
				<td colspan="4"><hr /></td>
			</tr>
			<tr>
				<td colspan="4">Este Documento no puede ser utilizado para Despacho</td>
			</tr>
		</tfoot>
    </table>';


	//echo $html;
$tbl = <<<EOD
	$html
EOD;

	$pdf->writeHTML($tbl, true, false, false, false, '');
	$pdf->Output('packing_list_'.$arqnro.'.pdf', 'I');//muestra
	//$pdf->Output('packing_list_'.$arqnro.'.pdf', 'FI');;//PRUEBA DE GUARDADO (muestra y guarda en raiza de pedisos)..
?>