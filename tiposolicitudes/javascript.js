/*
jDavila
04/10/12
*/
function agregar()
{
	document.form.atlcod.value="";
	document.form.atldes.value="";
	$("#aftsav").hide(1);
}

/*
jDavila
04/10/12
*/
function agregarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	
	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "tiposolucitudagregarvalidarphp.php",
        data: $("#form").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#aediv").hide(1000);
		$("#aftsav").show(1000);
	}
}

/*
jDavila
21/03/12
*/
function editar(tipo){

	var param = [];
	param['atlcod']=tipo;
	ejecutasqlp("tiposolucitudinformacionphp.php",param);

	for(i in gdata)
	{
		document.form.hatlcod.value=gdata[i].ATLCOD;
		document.getElementById("wsatlcod").innerHTML=gdata[i].ATLCOD;
		document.form.atldes.value=gdata[i].ATLDES;
		/*falta cargar los parametros*/
		
	};
	$("#aftsav").hide(1);
}

/*
jDavila
04/10/12
*/
function editarvalidar(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";
	$.ajax({
			async: false,
			type: "POST",
			dataType: "JSON",
			url: "tiposolucitudeditarvalidarphp.php",
			data: $("#form").serialize(),
			success: function(datos){
						vdata = parseJSONx(datos);}							
	});			
	var sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#form").hide(1000);
		$("#aftsav").show(1000);	
	}

}

/*
jDavila
20/03/12
*/
function eliminar(tipo) {
	if (confirm('Seguro que desea borrar la solicitud ' + tipo + '?'))
	{
		var param = [];
		param['atlcod']=tipo;
		ejecutasqld("tiposolucitudeliminar.php",param);
		location.reload();
	}
}

/**/
function buscarparametrosa(tipo){
		var x;
 		x=$("#pa");
  		x.empty();
		ejecutasql("buscarparametrosaphp.php",tipo);
		for(i in gdata)
		{
			var ul = $("#pa");
			var check= (gdata[i].APDTIP!="*") ? " checked": " ";
			ul.append("<li id='pr"+gdata[i].APDCOD+"'><input type='checkbox' name='parama["+i+"]' value='"+gdata[i].APDCOD+"'"+check+"/>"+gdata[i].APDDES+"<ul id='ul"+gdata[i].APDCOD+"'></ul></li>");
		}
}

function agregarusu()
{
	document.agregarform.auscod.value="";

	$("#agregaraftsav").hide(1);
}


function agregarvalidarsus(){	
	var contenido= "";
	var elemento;
	document.body.style.cursor="wait";

	$.ajax({
	   	async: false,
        type: "POST",
		dataType: "JSON",
        url: "transaccionusuarioagregarvalidar.php",
        data: $("#agregarform").serialize(),
        success: function(datos){
			vdata = parseJSONx(datos);}							
	});			
	sierror='N';
	for(i in vdata)	{
		if(vdata[i].campo) 	{
			document.getElementById(vdata[i].campo).innerHTML=vdata[i].msg;
			sierror='S';
		}
	}
	document.body.style.cursor="default";
	if(sierror=='N') 		
	{
		$("#agregardiv").hide(1000);
		$("#agregaraftsav").show(1000);
	}
}


function eliminarsus(id, atrcod, atrdes) {
	if (confirm('Seguro que desea borrar el usuario ' + id + '?'))
	{
		var param = [];
		param['auscod']=id;
		param['atrcod']=atrcod;
		param['atrdes']=atrdes;
		
		ejecutasqld("transaccionusuarioeliminar.php",param);
		location.reload();
	}
}


